CREATE TABLE `student_receivables` (
                                                      `id` VARCHAR(45) NOT NULL,
                                                      `nim` VARCHAR(45) NULL,
                                                      `name` VARCHAR(45) NULL,
                                                      `level_student` VARCHAR(45) NULL,
                                                      `study_program` VARCHAR(45) NULL,
                                                      `semester` VARCHAR(45) NULL,
                                                      `program` VARCHAR(45) NULL,
                                                      `bill_type` VARCHAR(45) NULL,
                                                      `amount_of_debt` VARCHAR(45) NULL,
                                                      `remaining_debt` VARCHAR(45) NULL,
                                                      `status` VARCHAR(45) NULL,
                                                      `user` VARCHAR(45) NULL,
                                                      `created` DATETIME NULL,
                                                      PRIMARY KEY (`id`));

CREATE TABLE `bill_type` (
                                            `id` VARCHAR(45) NOT NULL,
                                            `name` VARCHAR(20) NULL,
                                            `status` VARCHAR(45) NULL,
                                            PRIMARY KEY (`id`));
