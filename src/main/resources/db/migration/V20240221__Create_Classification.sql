create table classification(
    id varchar(36),
    name varchar(45),
    status varchar(20),
    primary key (id)
);

create table subclassification(
    id varchar(36),
    id_classification varchar(36),
    code varchar(20),
    name varchar(45),
    status varchar(20),
    primary key (id)
);

ALTER TABLE account ADD id_subclassification varchar(36);