ALTER TABLE `account`
    ADD COLUMN `account_type` VARCHAR(45) NULL AFTER `id_subclassification`,
ADD COLUMN `nominal_balance` VARCHAR(45) NULL AFTER `account_type`,
ADD COLUMN `modified` DATETIME NULL AFTER `nominal_balance`,
ADD COLUMN `modified_by` VARCHAR(45) NULL AFTER `modified`,
ADD COLUMN `deleted` DATETIME NULL AFTER `modified_by`,
ADD COLUMN `deleted_by` VARCHAR(45) NULL AFTER `deleted`;
