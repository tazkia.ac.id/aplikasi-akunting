ALTER TABLE     `account_balance`
    CHANGE COLUMN `account_balance` `account_balance` DECIMAL(25,2) NULL DEFAULT NULL ;

ALTER TABLE `journal_detail`
    CHANGE COLUMN `amount` `amount` DECIMAL(25,2) NULL DEFAULT NULL ;

