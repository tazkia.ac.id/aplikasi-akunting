CREATE TABLE `report_setting` (
                                  `id` varchar(36) NOT NULL,
                                  `name` varchar(45) DEFAULT NULL,
                                  `status` varchar(45) DEFAULT NULL,
                                  `created` datetime DEFAULT NULL,
                                  `created_by` varchar(45) DEFAULT NULL,
                                  `deleted` datetime DEFAULT NULL,
                                  `deleted_by` varchar(45) DEFAULT NULL,
                                  `modified` datetime DEFAULT NULL,
                                  `modified_by` varchar(45) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
);


CREATE TABLE `report_setting_detail` (
                                                         `id` VARCHAR(36) NOT NULL,
                                                         `id_report_setting` VARCHAR(36) NULL,
                                                         `id_account` VARCHAR(45) NULL,
                                                         `sequence` INT NULL,
                                                         `minus` VARCHAR(3) NULL,
                                                         `brackets` VARCHAR(3) NULL,
                                                         `redsign` VARCHAR(3) NULL,
                                                         `status` varchar(45) DEFAULT NULL,
                                                         `created` datetime DEFAULT NULL,
                                                         `created_by` varchar(45) DEFAULT NULL,
                                                         `deleted` datetime DEFAULT NULL,
                                                         `deleted_by` varchar(45) DEFAULT NULL,
                                                         `modified` datetime DEFAULT NULL,
                                                         `modified_by` varchar(45) DEFAULT NULL,
                                                         PRIMARY KEY (`id`));
