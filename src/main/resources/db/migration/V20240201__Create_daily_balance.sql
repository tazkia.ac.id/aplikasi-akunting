create table daily_balance(
    id varchar(36) not null,
    id_account varchar(36) not null,
    date date,
    start_amount decimal(25),
    primary key (id)
);