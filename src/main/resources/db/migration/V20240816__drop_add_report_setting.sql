ALTER TABLE `report_setting_detail`
DROP COLUMN `redsign`,
DROP COLUMN `brackets`,
DROP COLUMN `minus`;

ALTER TABLE `report_setting`
    ADD COLUMN `redsign` VARCHAR(3) NULL AFTER `modified_by`,
ADD COLUMN `brackets` VARCHAR(3) NULL AFTER `redsign`,
ADD COLUMN `minus` VARCHAR(3) NULL AFTER `brackets`;




