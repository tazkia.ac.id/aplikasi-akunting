CREATE TABLE `account_balance` (
    `id` VARCHAR(36) NOT NULL,
    `id_account` VARCHAR(45) NULL,
    `account_balance` DECIMAL(25,0) NULL,
    `year` integer(4) NULL,
    `start_date` DATE,
    `status` VARCHAR(45) NULL,
    `created` DATETIME NULL,
    `created_by` VARCHAR(45) NULL,
    `deleted` DATETIME NULL,
    `deleted_by` VARCHAR(45) NULL,
    `modified` DATETIME NULL,
    `modified_by` VARCHAR(45) NULL,
    PRIMARY KEY (`id`)
);
