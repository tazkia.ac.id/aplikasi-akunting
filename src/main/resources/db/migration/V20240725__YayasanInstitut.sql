CREATE TABLE `yayasan` (
                                          `id` VARCHAR(45) NOT NULL,
                                          `nama_yayasan` VARCHAR(45) NULL,
                                          `deskripsi` VARCHAR(45) NULL,
                                          `status` VARCHAR(45) NULL,
                                          PRIMARY KEY (`id`));

CREATE TABLE `institut` (
                                           `id` VARCHAR(45) NOT NULL,
                                            `id_yayasan` VARCHAR(45) NULL,
                                           `nama_institut` VARCHAR(45) NULL,
                                           `deskripsi` VARCHAR(45) NULL,
                                           `status` VARCHAR(45) NULL,
                                           PRIMARY KEY (`id`));