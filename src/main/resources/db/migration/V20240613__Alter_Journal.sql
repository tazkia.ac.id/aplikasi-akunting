ALTER TABLE `journal`
    ADD COLUMN `status` VARCHAR(45) NULL AFTER `created_by`;

ALTER TABLE `journal_detail`
    ADD COLUMN `status` VARCHAR(45) NULL AFTER `sequence`;
