create table year(
    id integer (4),
    year integer(4),
    status varchar(15),
    primary key (id)
);

create table month(
    id integer(4),
    number integer(4),
    name varchar(20),
    status varchar(15),
    primary key (id)
);


INSERT INTO year VALUES (2019, 2019, 'ACTIVE');
INSERT INTO year VALUES (2020, 2020, 'ACTIVE');
INSERT INTO year VALUES (2021, 2021, 'ACTIVE');
INSERT INTO year VALUES (2022, 2022, 'ACTIVE');
INSERT INTO year VALUES (2023, 2023, 'ACTIVE');
INSERT INTO year VALUES (2024, 2024, 'ACTIVE');
INSERT INTO year VALUES (2025, 2025, 'ACTIVE');
INSERT INTO year VALUES (2026, 2026, 'ACTIVE');

INSERT INTO month VALUES (1, 1, 'January', 'ACTIVE');
INSERT INTO month VALUES (2, 2, 'February', 'ACTIVE');
INSERT INTO month VALUES (3, 3, 'March', 'ACTIVE');
INSERT INTO month VALUES (4, 4, 'April', 'ACTIVE');
INSERT INTO month VALUES (5, 5, 'May', 'ACTIVE');
INSERT INTO month VALUES (6, 6, 'June', 'ACTIVE');
INSERT INTO month VALUES (7, 7, 'July', 'ACTIVE');
INSERT INTO month VALUES (8, 8, 'August', 'ACTIVE');
INSERT INTO month VALUES (9, 9, 'September', 'ACTIVE');
INSERT INTO month VALUES (10, 10, 'October', 'ACTIVE');
INSERT INTO month VALUES (11, 11, 'November', 'ACTIVE');
INSERT INTO month VALUES (12, 12, 'December', 'ACTIVE');