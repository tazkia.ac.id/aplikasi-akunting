create table account(
    id varchar(36) not null,
    code varchar(36),
    name varchar(255),
    amount decimal(25),
    status varchar(30),
    primary key (id)
);

create table journal(
    id varchar(36) not null ,
    transaction_time DATETIME not null,
    description varchar(255),
    primary key (id)
);

create table journal_detail(
    id varchar(36) not null ,
    id_journal varchar(36),
    id_account varchar(36),
    balance_type varchar(36) not null,
    amount decimal(25),
    primary key (id)
);

create table s_role(
    id varchar(36) not null ,
    name varchar(36) not null ,
    description varchar(255),
    primary key (id)
);

create table s_role_permission(
       id_role varchar(36) not null,
       id_permission varchar(36) not null
);

create table s_permission(
       id varchar(36) not null,
       permission_label varchar(36),
       permission_value varchar(255),
    primary key (id)
);

create table s_user(
    id varchar(36) not null ,
    username varchar(100) not null,
    active boolean,
    id_role varchar(36) not null ,
    user varchar(100),
    primary key (id)
);

create table journal_template(
    id varchar(36) not null ,
    code varchar(36) not null ,
    name varchar(255) not null ,
    id_journal_category_template varchar(36) not null ,
    status varchar(36),
    primary key (id)
);

create table journal_category_template(
    id varchar(36) not null ,
    category varchar(36) not null ,
    status varchar(36),
    primary key (id)
);

create table journal_template_detail(
    id varchar(36) not null,
    id_journal_template varchar(36) not null,
    id_account varchar(36) not null,
    sequence int(12),
    status varchar(36),
    primary key(id)
);