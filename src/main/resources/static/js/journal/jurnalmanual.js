var rowIndex = 0;
document.getElementById("btnSubmit").disabled = true;


function addRow() {
    var tableBody = document.getElementById("tableBody");
    var newRow = tableBody.insertRow();

    var cellIndex = newRow.insertCell(0);
    cellIndex.textContent = rowIndex + 1;

    var cellName = newRow.insertCell(1);
    var cellDebet = newRow.insertCell(2);
    var cellCredit = newRow.insertCell(3);
    var cellAction = newRow.insertCell(4);

    var accountSelect = document.createElement("select");
    accountSelect.id = "account" + rowIndex;
    accountSelect.name = "account";
    accountSelect.classList.add("form-control", "select2");
    cellName.appendChild(accountSelect);
    var accountSelect2 = "#account" + rowIndex;

    fetch('/public/api/account/list')
        .then(response => response.json())
        .then(data => {
            data.data.forEach(account => {
                var option = document.createElement("option");
                option.value = account.id;
                option.text = account.code + "  " + account.name;
                accountSelect.appendChild(option);
            });

            $(accountSelect2).select2({
                theme: 'bootstrap-5',
                placeholder: '- Select Account -'
            });
        })
        .catch(error => console.error('Error fetching accounts:', error));

    var debetInput = document.createElement("input");
    debetInput.type = "text";  // Change type to "text" to enable custom formatting
    debetInput.name = "debet";
    debetInput.id = "debet" + rowIndex;
    debetInput.classList.add("form-control", "debet-input");
    cellDebet.appendChild(debetInput);

    var creditInput = document.createElement("input");
    creditInput.type = "text";  // Change type to "text" to enable custom formatting
    creditInput.id = "credit" + rowIndex;
    creditInput.name = "credit";
    creditInput.classList.add("form-control", "credit-input");
    cellCredit.appendChild(creditInput);

    debetInput.addEventListener("keyup", function() {
        if (debetInput.value != "") {
            creditInput.readOnly = true;
        } else {
            creditInput.readOnly = false;
        }
        formatCurrency(debetInput);
        updateTotal();
    });

    creditInput.addEventListener("keyup", function() {
        if (creditInput.value != "") {
            debetInput.readOnly = true;
        } else {
            debetInput.readOnly = false;
        }
        formatCurrency(creditInput);
        updateTotal();
    });

    cellAction.innerHTML = "<button type='button' class='hapusButton' style='background-color: #dc3545; color: white; border: none; border-radius: 5px; padding: 5px 10px 5px 10px;'><i class='fa fa-trash'></i></button>";

    var hapusButton = newRow.querySelector('.hapusButton');
    hapusButton.addEventListener("click", function() {
        deleteThisRow(this);
    });

    rowIndex++;
    updateTotal();
}

function formatCurrency(input) {
    let value = input.value;

    // Remove non-numeric characters except for comma
    value = value.replace(/[^\d,]/g, '');

    // Split the value on comma to separate decimal part if exists
    const parts = value.split(',');
    value = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.'); // Add thousand separators
    if (parts.length > 1) {
        value += ',' + parts[1];
    }

    // Update the input value
    input.value = value;
}

function updateTotal() {
    var tableBody = document.getElementById("tableBody");
    var totalDebetInput = document.getElementsByName("totaldebet")[0];
    var totalCreditInput = document.getElementsByName("totalcredit")[0];

    var totalDebet = 0;
    var totalCredit = 0;
    var hasData = false;

    for (var i = 0; i < tableBody.rows.length; i++) {
        var row = tableBody.rows[i];

        var debetInput = row.querySelector(".debet-input");
        var creditInput = row.querySelector(".credit-input");

        if (debetInput.value !== "" || creditInput.value !== "") {
            hasData = true;
        }

        totalDebet += parseFloat(debetInput.value.replace(/\./g, '').replace(',', '.')) || 0;
        totalCredit += parseFloat(creditInput.value.replace(/\./g, '').replace(',', '.')) || 0;
    }

    if (hasData) {
        totalDebetInput.value = totalDebet.toLocaleString('id-ID', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        totalCreditInput.value = totalCredit.toLocaleString('id-ID', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
    } else {
        totalDebetInput.value = "";
        totalCreditInput.value = "";
    }

    var statusElement = document.getElementById("status");
    if (totalDebet === totalCredit && hasData) {
        statusElement.textContent = "Balance";
        statusElement.style.color = "green";
        document.getElementById("btnSubmit").disabled = false;
    } else {
        statusElement.textContent = "Not Balance";
        statusElement.style.color = "red";
        document.getElementById("btnSubmit").disabled = true;
    }
}

document.addEventListener('DOMContentLoaded', function () {
    // Ensure all existing inputs are properly formatted on load
    var inputs = document.querySelectorAll('[id^="debet"], [id^="credit"]');
    inputs.forEach(function(input) {
        input.addEventListener('input', function() {
            formatCurrency(input);
        });
    });
});



function deleteRow() {
    var tableBody = document.getElementById("tableBody");
    var rowCount = tableBody.rows.length;
    if (rowCount > 0) {
        // Ambil baris terakhir dan hapus
        var lastRow = tableBody.rows[rowCount - 1];
        tableBody.removeChild(lastRow);
        rowIndex--;
    }
}

function deleteThisRow(button) {
    if (confirm("Apakah Anda yakin ingin menghapus baris ini?")) {
        var row = button.closest('tr'); // Temukan elemen <tr> terdekat (baris)

        if (row) {
            row.parentNode.removeChild(row);
            rowIndex--;
        } else {
            console.error("Error: Baris tidak ditemukan");
        }
    }
}