package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.AccountDao;
import id.ac.tazkia.accounting.dao.DailyBalanceDao;
import id.ac.tazkia.accounting.dao.JournalDetailDao;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.DailyBalance;
import id.ac.tazkia.accounting.entity.StatusRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

@Service
@EnableScheduling @Slf4j
public class DailyBalanceService {

    @Autowired
    private JournalDetailDao journalDetailDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private DailyBalanceDao dailyBalanceDao;

    @Scheduled(cron = "30 36     8 * * *", zone = "Asia/Jakarta")
    public void scheduledTask() {
        try {
            LocalDate today = LocalDate.now();
            YearMonth selectedYearMonth = YearMonth.from(today);
            LocalDate firstDayOfMonth = selectedYearMonth.atDay(1);
            LocalDate yesterdayDate = today.minusDays(1);
            List<Account> accountList = accountDao.findByStatusOrderByCode(StatusRecord.ACTIVE);

            for (Account account : accountList){
                BigDecimal credit = journalDetailDao.getAmountByAccountTypeCredit(firstDayOfMonth, yesterdayDate, account.getId());
                BigDecimal debet = journalDetailDao.getAmountByAccountTypeDebet(firstDayOfMonth, yesterdayDate, account.getId());

                BigDecimal resultDailyBalance = BigDecimal.valueOf(0);
                if (debet != null && credit != null) {
                    if (debet.compareTo(credit) > 0) {
                        resultDailyBalance = debet.subtract(credit);
                        saveDailyBalance(resultDailyBalance, account);
                    } else if (debet.compareTo(credit) < 0) {
                        resultDailyBalance = debet.subtract(credit);
                        saveDailyBalance(resultDailyBalance, account);
                    }
                } else if (debet != null) {
                    resultDailyBalance = debet;
                    saveDailyBalance(resultDailyBalance, account);
                } else if (credit != null) {
                    debet = BigDecimal.ZERO;
                    resultDailyBalance = debet.subtract(credit);
                    saveDailyBalance(resultDailyBalance, account);
                } else {
                    saveDailyBalance(resultDailyBalance, account);
                }
            }
        }catch (Exception e){
            log.error("[Error] [Save Daily Balance] : " + e.getMessage());
        }
    }

    private DailyBalance saveDailyBalance(BigDecimal result, Account account){
        DailyBalance dailyBalanceSave = new DailyBalance();
        dailyBalanceSave.setDate(LocalDate.now());
        dailyBalanceSave.setAccount(account);
        dailyBalanceSave.setStartAmount(result);
        dailyBalanceDao.save(dailyBalanceSave);
        log.info("[Log Info] - [Save Daily Balance]: " + dailyBalanceSave);
        return dailyBalanceSave;
    }
}
