package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.UserDao;
import id.ac.tazkia.accounting.entity.config.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserService {
    @Autowired
    private UserDao userDao;

    public User currentUser(Authentication currentUser){
        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) currentUser;

        String username = (String) token.getPrincipal().getAttributes().get("email");
        User u = userDao.findByUsername(username);
        return u;

    }
}
