package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.AccountDao;
import id.ac.tazkia.accounting.dao.ReportSettingDao;
import id.ac.tazkia.accounting.dao.ReportSettingDetailDao;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.ReportSetting;
import id.ac.tazkia.accounting.entity.ReportSettingDetail;
import id.ac.tazkia.accounting.entity.StatusRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class ReportSettingService {
    @Autowired
    private ReportSettingDao reportSettingDao;

    @Autowired
    private ReportSettingDetailDao reportSettingDetailDao;

    @Autowired
    private AccountDao accountDao;

    public List<ReportSetting> getAllReportSettings() {
        return reportSettingDao.findByStatus(StatusRecord.ACTIVE);
    }

    public List<ReportSettingDetail> getReportSettingDetailById(ReportSetting reportSetting) {
        return reportSettingDetailDao.findByReportSettingAndStatusOrderByAccountCodeAsc(reportSetting, StatusRecord.ACTIVE);
    }

    public int getCountReportSettingDetail(ReportSetting reportSetting) {
        return reportSettingDetailDao.countByReportSettingAndStatus(reportSetting, StatusRecord.ACTIVE);
    }

    public void saveReportSetting(ReportSetting reportSetting) {
        reportSettingDao.save(reportSetting);
    }

    public void deleteReportSetting(ReportSetting reportSetting) {
        reportSettingDao.delete(reportSetting);
    }

    public void deleteReportSettingDetail(ReportSettingDetail reportSettingDetail) {
        reportSettingDetailDao.delete(reportSettingDetail);
    }

    public String saveReportSettingDetail(String report, String[] account) {
        ReportSetting reportSetting = reportSettingDao.findById(report).get();
        int sequence = getCountReportSettingDetail(reportSetting) + 1; // Mulai dari sequence terakhir + 1

        for (int i = 0; i < account.length; i++) {
            String idaccount = account[i];

            Account getAccount = accountDao.findById(idaccount).get();

            ReportSettingDetail validation = reportSettingDetailDao.findByAccountAndReportSettingAndStatus(getAccount, reportSetting,StatusRecord.ACTIVE);
            if (validation == null) {
                ReportSettingDetail reportSettingDetail = new ReportSettingDetail();
                reportSettingDetail.setReportSetting(reportSetting);
                reportSettingDetail.setAccount(getAccount);
                reportSettingDetail.setSequence(sequence + i); // Tambahkan i ke sequence awal
                reportSettingDetailDao.save(reportSettingDetail);
            }
        }

        return reportSetting.getId();
    }

}
