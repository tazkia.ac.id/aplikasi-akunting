package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.*;
import id.ac.tazkia.accounting.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service @Slf4j
public class AccountService {
    @Autowired
    private AccountDao accountDao;

    @Autowired
    private JournalTemplateDetailDao journalTemplateDetailDao;

    @Autowired
    private JournalTemplateDao journalTemplateDao;

    @Autowired
    private DailyBalanceDao dailyBalanceDao;

    @Autowired
    private JournalDetailDao journalDetailDao;

    @Autowired
    private SubclassificationDao subclassificationDao;

    public Page<Account> getAccountPage(Pageable pageable){
        Page<Account> accountPage = accountDao.findByStatusOrderBySubclassificationCodeAscCodeAsc(StatusRecord.ACTIVE, pageable);
        return accountPage;
    }

    public Account getAccoountId(String id){
        return accountDao.findById(id).get();
    }

    public List<Account> getAccountSearch(String search){
        List<Account> accountList = accountDao.findByAndStatusAndNameContainingIgnoreCaseOrCodeContainingIgnoreCase(StatusRecord.ACTIVE,search, search);
        return accountList;
    }

    public void saveAccount(Account account){
        accountDao.save(account);
    }

    public Account checkCodeAccount(String code){
        Account account = accountDao.findByCodeAndStatus(code, StatusRecord.ACTIVE);
        return account;
    }

    public JournalTemplateDetail checkCodeTemplate(String id, String idAccount){
        Account account = accountDao.findById(idAccount).get();
        JournalTemplate journalTemplate = journalTemplateDao.findById(id).get();
        JournalTemplateDetail journalTemplateDetailResults = journalTemplateDetailDao
                .findByJournalTemplateAndAccountAndStatus(journalTemplate,account, StatusRecord.ACTIVE);
        return journalTemplateDetailResults;
    }

    public List<Account> getAllAccount(){
        List<Account> accountLists = accountDao.findByStatusOrderByCode(StatusRecord.ACTIVE);
        return accountLists;
    }

    public List<Account> getAccountBySubclassification(String subclassification){
        Subclassification getSubclassification = subclassificationDao.findById(subclassification).get();
        return accountDao.findByStatusAndSubclassificationOrderByCodeAsc(StatusRecord.ACTIVE, getSubclassification);
    }

    public List<Account> getAccountByType(AccountType type){
        return accountDao.findByStatusAndAccountTypeOrderByCodeAsc(StatusRecord.ACTIVE, type);
    }

    public Boolean deleteAccount(String id){
        Account account = accountDao.findById(id).get();
        List<JournalTemplateDetail> journalTemplateDetail = journalTemplateDetailDao.findByAccount(account);
        List<JournalDetail> journalDetails = journalDetailDao.findByAccount(account);

        if (!journalTemplateDetail.isEmpty() || !journalDetails.isEmpty()){
            log.info("[Delete Account] - [Failed] : Data - {}", account);
            return false;
        } else {
            account.setStatus(StatusRecord.DELETED);
            accountDao.save(account);
            log.info("[Delete Account] - [Success] : Data - {}", account);
            return true;
        }
    }

    public DailyBalance getCurrentBalanceAccount(Account account){
        LocalDate date = LocalDate.now();
        DailyBalance dailyBalance = dailyBalanceDao.findByAccountAndDate(account, date);
        return dailyBalance;
    }

    public String getCodeAccount(String subclassification){
        Subclassification getSubclassification = subclassificationDao.findById(subclassification).get();
        String resultCode = getSubclassification.getCode() + ".";
        return resultCode;
    }



}
