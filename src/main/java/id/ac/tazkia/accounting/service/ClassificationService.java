package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.ClassificationDao;
import id.ac.tazkia.accounting.dao.SubclassificationDao;
import id.ac.tazkia.accounting.entity.Classification;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.Subclassification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service @Slf4j
public class ClassificationService {

    @Autowired
    private ClassificationDao classificationDao;

    @Autowired
    private SubclassificationDao subclassificationDao;

    public void saveClassiification(Classification classification){
        classificationDao.save(classification);
    }

    public List<Classification> getClassification(){
        List<Classification> classificationList = classificationDao.findByStatus(StatusRecord.ACTIVE);
        return  classificationList;
    }

    public void saveSubClassification(Subclassification subclassification){
        subclassificationDao.save(subclassification);
    }

    public void deleteClassification(String id){
        Classification classification = classificationDao.findById(id).get();
        classification.setStatus(StatusRecord.DELETED);
        classificationDao.save(classification);
    }

    public Page<Subclassification> getSubclassification(Pageable pageable){
        Page<Subclassification> subclassificationPage = subclassificationDao.findByStatusOrderByCodeAsc(StatusRecord.ACTIVE
                ,pageable);
        return subclassificationPage;
    }

    public List<Subclassification> getSubclassificationForSelect(){
        List<Subclassification> subclassificationList = subclassificationDao.findByStatus(StatusRecord.ACTIVE);
        return subclassificationList;
    }

    public void deleteSubClassification(Subclassification subclassification){
        subclassification.setStatus(StatusRecord.DELETED);
        subclassificationDao.save(subclassification);
    }
}
