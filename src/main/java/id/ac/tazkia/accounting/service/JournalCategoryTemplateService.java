package id.ac.tazkia.accounting.service;


import id.ac.tazkia.accounting.dao.JournalCategoryTemplateDao;
import id.ac.tazkia.accounting.entity.JournalCategoryTemplate;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JournalCategoryTemplateService {
    @Autowired
    private JournalCategoryTemplateDao journalCategoryTemplateDao;

    public Page<JournalCategoryTemplate> getListPageJournalCategoryTemplate(Pageable pageable){
        Page<JournalCategoryTemplate> journalCategoryTemplates = journalCategoryTemplateDao.findByStatus(StatusRecord.ACTIVE, pageable);
        return journalCategoryTemplates;
    }

    public List<JournalCategoryTemplate> getListAllCategoryJournal(){
        List<JournalCategoryTemplate> journalCategoryTemplate = journalCategoryTemplateDao.findByStatusOrderByCategoryAsc(StatusRecord.ACTIVE);
        return journalCategoryTemplate;
    }

    public void saveJournalCategoryTemplate(JournalCategoryTemplate journalCategoryTemplate){
        journalCategoryTemplateDao.save(journalCategoryTemplate);
    }

    public void deleteJournalCategoryTemplate(String id){
        JournalCategoryTemplate journalCategoryTemplate = journalCategoryTemplateDao.findById(id).get();
        journalCategoryTemplate.setStatus(StatusRecord.DELETED);
        journalCategoryTemplateDao.save(journalCategoryTemplate);
    }

}
