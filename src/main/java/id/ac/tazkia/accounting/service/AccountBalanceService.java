package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.AccountBalanceDao;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountBalance;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.config.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

@Service
public class AccountBalanceService {
    @Autowired
    private AccountBalanceDao accountBalanceDao;

    public Page<AccountBalance> findAll(Pageable pageable) {
        Page<AccountBalance> getAll = accountBalanceDao.findByStatusOrderByAccountCodeAsc(StatusRecord.ACTIVE, pageable);
        return getAll;
    }

    public Page<AccountBalance> findByYear(Integer year, Pageable pageable){
        Page<AccountBalance> getYear = accountBalanceDao.findByYearAndStatus(year, StatusRecord.ACTIVE, pageable);
        return getYear;
    }

    public Page<AccountBalance> findByYearAndInstitut(Integer year, Institut institut, Pageable pageable){
        Page<AccountBalance> getInsitut = accountBalanceDao.findByYearAndInstitutAndStatusOrderByAccountCodeAsc(
                year, institut, StatusRecord.ACTIVE, pageable
        );

        System.out.println("Test : " + getInsitut);
        return getInsitut;
    }

        public Boolean saveAccountBalance(AccountBalance accountBalance, User user) {
            int year = accountBalance.getYear();
            LocalDate startDate = LocalDate.of(year, 1, 1);
            System.out.println("Account balance : " + accountBalance);
            AccountBalance validation = accountBalanceDao.
                    findByAccountAndInstitutAndYearAndStatus(
                            accountBalance.getAccount(),
                            accountBalance.getInstitut(),
                            accountBalance.getYear(), StatusRecord.ACTIVE);
            if (accountBalance.getId() == null) {
                System.out.println("post");
                if (validation == null) {
                    accountBalance.setInstitut(accountBalance.getInstitut());
                    accountBalance.setCreatedBy(user.getUsername());
                    accountBalance.setStartDate(startDate);
                    accountBalance.setCreated(LocalDateTime.now());
                    accountBalanceDao.save(accountBalance);
                    return true;
                }
                return false;
            }else {
                AccountBalance updatedAccountBalance = accountBalanceDao.findById(accountBalance.getId()).get();
                updatedAccountBalance.setAccountBalance(accountBalance.getAccountBalance());
                updatedAccountBalance.setInstitut(accountBalance.getInstitut());
                updatedAccountBalance.setModifiedBy(user.getUsername());
                updatedAccountBalance.setModified(LocalDateTime.now());
                accountBalanceDao.save(updatedAccountBalance);
                return true;
            }
        }

    public void deleteAccountBalance(AccountBalance accountBalance, User user) {
        accountBalance.setDeleted(LocalDateTime.now());
        accountBalance.setDeletedBy(user.getUsername());
        accountBalance.setStatus(StatusRecord.NONACTIVVE);
        accountBalanceDao.save(accountBalance);
    }
}
