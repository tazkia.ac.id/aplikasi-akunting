package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.*;
import id.ac.tazkia.accounting.dto.CreateJournalDto;
import id.ac.tazkia.accounting.dto.GetJournalTemplate;
import id.ac.tazkia.accounting.dto.GetJournalTemplateById;
import id.ac.tazkia.accounting.dto.GetJournalTemplateDetail;
import id.ac.tazkia.accounting.entity.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class JournalTemplateService {
    @Autowired
    private JournalTemplateDao journalTemplateDao;

    @Autowired
    private JournalTemplateDetailDao journalTemplateDetailDao;

    @Autowired
    private JournalDao journalDao;

    @Autowired
    private JournalDetailDao journalDetailDao;

    public Page<JournalTemplate> journalTemplatePage(Pageable pageable){
        Page<JournalTemplate> journalTemplates = journalTemplateDao.findByStatusOrderByCodeAsc(StatusRecord.ACTIVE, pageable);
        return journalTemplates;
    }
    public void saveJournalTemplate(JournalTemplate journalTemplate){
        journalTemplateDao.save(journalTemplate);
    }

    public void editJournalTemplate(JournalTemplate journalTemplate){
        JournalTemplate journalTemplateEdit = journalTemplateDao.findById(journalTemplate.getId()).orElse(null);
        journalTemplateEdit.setName(journalTemplate.getName());
        journalTemplateEdit.setJournalCategoryTemplate(journalTemplate.getJournalCategoryTemplate());
        journalTemplateEdit.setTypeApp(journalTemplate.getTypeApp());
        journalTemplateDao.save(journalTemplate);
    }

    public void deleteJournalTemplate(String id){
        JournalTemplate journalTemplate = journalTemplateDao.findById(id).get();
        journalTemplate.setStatus(StatusRecord.DELETED);
        journalTemplateDao.save(journalTemplate);
    }

    public Page<JournalTemplate> journalTemplatesBySearch(String search, Pageable pageable){
        Page<JournalTemplate> journalTemplates = journalTemplateDao.findByStatusAndNameContainingIgnoreCaseOrCodeContainingIgnoreCaseAndStatusOrderByCodeAsc(
                StatusRecord.ACTIVE, search, search, StatusRecord.ACTIVE, pageable
        );

        return journalTemplates;
    }

    public List<JournalTemplate> getJournalByCategory(JournalCategoryTemplate category){
        List<JournalTemplate> journalTemplateList = journalTemplateDao.findByStatusAndJournalCategoryTemplateOrderByCodeAsc(StatusRecord.ACTIVE, category);
        return journalTemplateList;
    }

    public Page<JournalTemplate> getJournalByType(TypeApp typeApp, Pageable pageable){
        return journalTemplateDao.findByTypeAppAndStatus(typeApp, StatusRecord.ACTIVE, pageable);
    }

    public Page<JournalTemplate> getJournalByTypeAndSearchNameOrCode(TypeApp typeApp, String seacrh, Pageable pageable){
        return journalTemplateDao
                .findByTypeAppAndStatusAndNameContainingIgnoreCaseOrCodeContainingIgnoreCaseOrderByCodeAsc(
                        typeApp, StatusRecord.ACTIVE, seacrh, seacrh, pageable
                );
    }

    public JournalTemplate checkCodeJournalTemplate(String code){
        JournalTemplate journalTemplate = journalTemplateDao.findByCodeAndStatus(code, StatusRecord.ACTIVE);
        return journalTemplate;
    }

    public List<GetJournalTemplate> getAllListJournalTemplate(){
        List<JournalTemplate> journalTemplateList = journalTemplateDao.findByStatus(StatusRecord.ACTIVE);


        List<GetJournalTemplate> result = new ArrayList<>();

        for (JournalTemplate journalTemplate : journalTemplateList) {
            GetJournalTemplate getJournalTemplate = new GetJournalTemplate();
            List<JournalTemplateDetail> journalTemplateDetailList = journalTemplateDetailDao.findByJournalTemplate(journalTemplate);
            getJournalTemplate.setId(journalTemplate.getId());
            getJournalTemplate.setCode(journalTemplate.getCode());
            getJournalTemplate.setName(journalTemplate.getName());
            getJournalTemplate.setCategory(journalTemplate.getJournalCategoryTemplate().getCategory());
            result.add(getJournalTemplate);
        }

        return result;
    }

    public List<GetJournalTemplate> getListJournalTemplateByTypeApp(TypeApp typeApp){
        List<JournalTemplate> journalTemplateList = journalTemplateDao.findByStatusAndTypeApp(StatusRecord.ACTIVE, typeApp);


        List<GetJournalTemplate> result = new ArrayList<>();

        for (JournalTemplate journalTemplate : journalTemplateList) {
            GetJournalTemplate getJournalTemplate = new GetJournalTemplate();
            List<JournalTemplateDetail> journalTemplateDetailList = journalTemplateDetailDao.findByJournalTemplate(journalTemplate);
            getJournalTemplate.setId(journalTemplate.getId());
            getJournalTemplate.setCode(journalTemplate.getCode());
            getJournalTemplate.setName(journalTemplate.getName());
            getJournalTemplate.setCategory(journalTemplate.getJournalCategoryTemplate().getCategory());

            result.add(getJournalTemplate);
        }

        return result;
    }

    public List<GetJournalTemplateById> getJournalTemplateDetail(String id){
        JournalTemplate journalTemplate = journalTemplateDao.findById(id).get();
        List<JournalTemplateDetail> journalTemplateDetailList = journalTemplateDetailDao.findByJournalTemplateOrderBySequenceAsc(journalTemplate);

        List<GetJournalTemplateById> getJournalTemplateDetails = new ArrayList<>();
        for (JournalTemplateDetail journalTemplateDetail : journalTemplateDetailList){
            GetJournalTemplateById getJournalTemplateDetail = new GetJournalTemplateById();
            getJournalTemplateDetail.setAccount(journalTemplateDetail.getAccount().getName());
            getJournalTemplateDetail.setIdAccount(journalTemplateDetail.getAccount().getId());
            getJournalTemplateDetail.setBalanceType(journalTemplateDetail.getBalanceType().toString());
            getJournalTemplateDetail.setSequence(journalTemplateDetail.getSequence());

            getJournalTemplateDetails.add(getJournalTemplateDetail);
        }
        return getJournalTemplateDetails;
    }

    public GetJournalTemplate getJournalTemplatesById(String id){
        JournalTemplate journalTemplate = journalTemplateDao.findById(id).get();
        GetJournalTemplate getJournalTemplate = new GetJournalTemplate();
        getJournalTemplate.setId(journalTemplate.getId());
        getJournalTemplate.setCode(journalTemplate.getCode());
        getJournalTemplate.setName(journalTemplate.getName());
        getJournalTemplate.setCategory(journalTemplate.getJournalCategoryTemplate().getCategory());
        return getJournalTemplate;
    }




}
