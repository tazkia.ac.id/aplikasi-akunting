package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.*;
import id.ac.tazkia.accounting.dto.*;
import id.ac.tazkia.accounting.entity.*;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service @Slf4j @Transactional
public class JournalDetailService {
    @Autowired
    private JournalDetailDao journalDetailDao;

    @Autowired
    private JournalTemplateDetailDao journalTemplateDetailDao;

    @Autowired
    private JournalDao journalDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private DailyBalanceDao dailyBalanceDao;

    @Autowired
    private JournalTemplateDao journalTemplateDao;

    @Autowired
    private InstitutDao institutDao;

    public List<JournalDetail> getJournalDetailByJournal(Journal journal, StatusRecord statusRecord){
        List<JournalDetail> journalDetails = journalDetailDao.findByStatusAndJournalOrderBySequence(statusRecord, journal);
        return journalDetails;
    }

    public List<JuournalDetailDto> getPageJournalDetail(LocalDate date1, LocalDate date2){
        List<JuournalDetailDto> journalDetails = journalDetailDao.listJournaDetail(date1, date2);
        System.out.println(journalDetails);
        return journalDetails;
    }

    public Page<LocalDate> getDateJournal(LocalDate date1, LocalDate date2, Pageable pageable){
        Page<LocalDate> tanggalJurnal = journalDetailDao.cariJurnal(date1,date2, pageable);
        return tanggalJurnal;
    }

    public BigDecimal getTotalAmountDebet(String idJournal){
        BigDecimal totalDebet = journalDetailDao.getTotalAmountDebet(idJournal);
        return totalDebet;
    }
    public BigDecimal getTotalAmountCredit(String idJournal){
        BigDecimal totalCredit = journalDetailDao.getTotalAmountCredit(idJournal);
        return totalCredit;
    }

    public ArrayList<JournalDetail> getJournalDetailByPageJournal(Page<Journal> page, StatusRecord statusRecord){
        ArrayList<JournalDetail> journalDetails = new ArrayList<>();
        for (Journal journal : page) {
            List<JournalDetail> journalDetail = journalDetailDao.findByStatusAndJournalOrderBySequence(statusRecord, journal);
            for (JournalDetail data: journalDetail) {
                journalDetails.add(data);
            }
        }
        return journalDetails;
    }

    public ArrayList<JournalDetail> getJournalDetailByDateJournal(List<Journal> journals, StatusRecord statusRecord){
        ArrayList<JournalDetail> journalDetails = new ArrayList<>();
        for (Journal journal : journals) {
            List<JournalDetail> journalDetail = journalDetailDao.findByStatusAndJournalOrderBySequence(statusRecord, journal);
            for (JournalDetail data: journalDetail) {
                journalDetails.add(data);
            }
        }
        return journalDetails;
    }



    public List<JournalDetailByAccountDto> getJournalDetailByAccount(LocalDate date1, LocalDate date2, String account){
        List<JournalDetailByAccountDto> getJournalByAccount = journalDetailDao.listJournalDetailByAccount(date1, date2, account);
        return getJournalByAccount;
    }

    public CreateJournalDto createJournalUseUserInterface(CreateJournalDto createJournalDto, String[] amount){
        // search journal template
        List<JournalTemplateDetail> journalTemplateDetail = journalTemplateDetailDao
                .findByStatusAndJournalTemplateOrderBySequenceAsc(StatusRecord.ACTIVE,
                        createJournalDto.getJournalTemplate());
        Journal journal = new Journal();
        journal.setTransactionTime(LocalDateTime.now());
        journal.setTransactionDate(createJournalDto.getDateTransaction());
        journal.setDescription(createJournalDto.getDescription());
        journal.setCreatedBy(createJournalDto.getCreatedBy());
        journal.setInstitut(createJournalDto.getInstitut());
        journal.setJournalType(JournalType.NORMAL);
        createJournalDto.setJournal(journal);
        BigDecimal debet = BigDecimal.valueOf(0);
        BigDecimal credit = BigDecimal.valueOf(0);

        ArrayList<JournalDetailDto> journalDetailDtoList = new ArrayList<>();
        for (int j = 0; j < amount.length && j < journalTemplateDetail.size(); j++) {
            JournalTemplateDetail datas = journalTemplateDetail.get(j);

            String amountString = amount[j];

            amountString = amountString.replaceAll("\\.", "");

            amountString = amountString.replace(",", ".");

            BigDecimal currentAmount = new BigDecimal(amountString);

            JournalDetailDto journalDetailDto = new JournalDetailDto();
            journalDetailDto.setJournal(journal);
            journalDetailDto.setAccount(datas.getAccount());
            journalDetailDto.setBalanceType(datas.getBalanceType());
            journalDetailDto.setAmount(currentAmount);
            journalDetailDto.setStatus(StatusRecord.ACTIVE);
            journalDetailDto.setSequence(datas.getSequence());

            if (journalDetailDto.getBalanceType() == BalanceType.DEBET){
                debet = debet.add(journalDetailDto.getAmount());
            } else if (journalDetailDto.getBalanceType() == BalanceType.CREDIT) {
                credit = credit.add(journalDetailDto.getAmount());
            }
            journalDetailDtoList.add(journalDetailDto);
        }

        if (debet.compareTo(credit) == 0){
            journalDao.save(journal);
            for (JournalDetailDto journalDetail : journalDetailDtoList){
                JournalDetail journalDetailSave = new JournalDetail();
                BeanUtils.copyProperties(journalDetail, journalDetailSave);
                journalDetailDao.save(journalDetailSave);
            }
            createJournalDto.setStatusJournal(Boolean.TRUE);
        }else {
            createJournalDto.setStatusJournal(Boolean.FALSE);
            createJournalDto.setJournalDetailDtoList(journalDetailDtoList);
            createJournalDto.setDebetSatu(debet);
            createJournalDto.setCreditSatu(credit);
            createJournalDto.setMessageJournal("Not Balance");
        }
        return createJournalDto;
    }

    public CreateJournalDto createJournalManual(CreateJournalDto createJournalDto, String[] account,
                                                String[] debet, String[] credit) {
        Journal journal = new Journal();
        journal.setTransactionTime(LocalDateTime.now());
        journal.setTransactionDate(createJournalDto.getDateTransaction());
        journal.setDescription(createJournalDto.getDescription());
        journal.setCreatedBy(createJournalDto.getCreatedBy());
        journal.setInstitut(createJournalDto.getInstitut());
        journal.setJournalType(createJournalDto.getJournalType());
        createJournalDto.setJournal(journal);

        BigDecimal validationDebet = BigDecimal.valueOf(0);
        BigDecimal validationCredit = BigDecimal.valueOf(0);
        ArrayList<JournalDetailDto> journalDetailDtoList = new ArrayList<>();
        int sequence = 1;

        // Set untuk mendeteksi akun duplikat
        Set<String> accountSet = new HashSet<>();

        for (int j = 0; j < account.length && j < debet.length && j < credit.length; j++) {
            String idAccount = account[j];

            // Cek jika akun sudah ada di Set, berarti duplikat
            if (accountSet.contains(idAccount)) {
                createJournalDto.setStatusJournal(Boolean.FALSE);
                createJournalDto.setMessageJournal("Akun yang sama tidak boleh dimasukkan lebih dari sekali.");
                return createJournalDto;  // Langsung return jika duplikat ditemukan
            } else {
                accountSet.add(idAccount);  // Tambahkan akun ke Set jika belum ada
            }

            String debetString = debet[j];
            String creditString = credit[j];

            try {
                BigDecimal currentAmountDebet = new BigDecimal(debetString);
                BigDecimal currentAmountCredit = new BigDecimal(creditString);

                System.out.println("Test : " + currentAmountDebet);
                System.out.println("Test : " + currentAmountCredit);
                System.out.flush();  // Force flush to see the output immediately

                Account accountById = accountDao.findById(idAccount).get();
                JournalDetailDto journalDetailDto = new JournalDetailDto();
                journalDetailDto.setJournal(journal);
                journalDetailDto.setAccount(accountById);
                journalDetailDto.setStatus(StatusRecord.ACTIVE);
                journalDetailDto.setSequence(sequence++);

                if (currentAmountDebet.compareTo(BigDecimal.ZERO) > 0) {
                    journalDetailDto.setAmount(currentAmountDebet);
                    journalDetailDto.setBalanceType(BalanceType.DEBET);
                    validationDebet = validationDebet.add(currentAmountDebet);
                }
                if (currentAmountCredit.compareTo(BigDecimal.ZERO) > 0) {
                    journalDetailDto.setAmount(currentAmountCredit);
                    journalDetailDto.setBalanceType(BalanceType.CREDIT);
                    validationCredit = validationCredit.add(currentAmountCredit);
                }
                journalDetailDtoList.add(journalDetailDto);
            } catch (NumberFormatException e) {
                System.err.println("Invalid number format: " + debetString + " or " + creditString);
                e.printStackTrace();
            }
        }

        // Cek apakah debet dan kredit seimbang
        if (validationDebet.compareTo(validationCredit) == 0) {
            journalDao.save(journal);
            for (JournalDetailDto journalDetail : journalDetailDtoList) {
                JournalDetail journalDetailSave = new JournalDetail();
                BeanUtils.copyProperties(journalDetail, journalDetailSave);
                log.info("[Info] [Save Success] : " + journalDetailSave);
                journalDetailDao.save(journalDetailSave);
            }
            createJournalDto.setStatusJournal(Boolean.TRUE);
            createJournalDto.setMessageJournal("Proses pencatatan jurnal berhasil dilakukan.");
        } else {
            createJournalDto.setStatusJournal(Boolean.FALSE);
            createJournalDto.setJournalDetailDtoList(journalDetailDtoList);
            createJournalDto.setDebetSatu(validationDebet);
            createJournalDto.setCreditSatu(validationCredit);
            createJournalDto.setMessageJournal("Pembuatan jurnal gagal karena jumlahnya tidak seimbang Mohon periksa dan ubah nilai amount yang sesuai.");
        }

        return createJournalDto;
    }


    public String cleanAndFormatValue(String value) {
        if (value == null || value.trim().isEmpty()) {
            return "0";
        }
        // Remove thousand separators ('.') but keep decimal point (',')
        value = value.replaceAll("\\.(?=.*\\d)", "");
        // Replace decimal point (',') with '.'
        value = value.replace(",", ".");
        return value;
    }


    public JournalDetailEditDto journalDetailEdit(JournalDetailEditDto journalDetailEditDto){
        Journal journal = journalDetailEditDto.getJournal();
        journal.setTransactionDate(journalDetailEditDto.getTransactionDate());
        journal.setDescription(journalDetailEditDto.getDecription());
        journal.setInstitut(journalDetailEditDto.getInstitut());
        journalDao.save(journal);
        BigDecimal currentDebet = BigDecimal.ZERO;
        BigDecimal currentCredit = BigDecimal.ZERO;
        ArrayList<JournalDetailDto> journalDetailDtoList = new ArrayList<>();

        for (int i = 0;i < journalDetailEditDto.getJournalDetails().length; i++ ){
            JournalDetail journalDetail = journalDetailEditDto.getJournalDetails()[i];
            Account account = journalDetailEditDto.getAccount()[i];
            String amount = cleanAndFormatValue(journalDetailEditDto.getAmount()[i]);
            BalanceType balanceType = journalDetailEditDto.getBalanceType()[i];
            BigDecimal fixAmount = new BigDecimal(amount);

            if (BalanceType.DEBET == balanceType){
                currentDebet = currentDebet.add(fixAmount);
            }
            if (BalanceType.CREDIT == balanceType){
                currentCredit = currentCredit.add(fixAmount);
            }


            JournalDetailDto journalDetailDto = new JournalDetailDto();


            if (journal.getStatus() == StatusRecord.REVIEW){
                journalDetailDto.setStatus(StatusRecord.REVIEW);
            } else if (journal.getStatus() == StatusRecord.ACTIVE) {
                journalDetailDto.setStatus(StatusRecord.ACTIVE);
            }

            if (journalDetailEditDto.getJournal() == null){
                journalDetailDto.setJournal(journal);
                journalDetailDto.setAccount(account);
                journalDetailDto.setAmount(fixAmount);
                journalDetailDto.setBalanceType(balanceType);
                journalDetailDto.setSequence(i);
                System.out.println("journalDetailNew : " + journalDetailDto);
            }else {
                if (journalDetail == null){
                    journalDetailDto.setJournal(journal);
                    journalDetailDto.setAccount(account);
                    journalDetailDto.setAmount(fixAmount);
                    journalDetailDto.setBalanceType(balanceType);
                    journalDetailDto.setSequence(i);
                    System.out.println("journalDetailNew : " + journalDetailDto);
                } else {
                    journalDetailDto.setId(journalDetail.getId());
                    journalDetailDto.setAccount(account);
                    journalDetailDto.setAmount(fixAmount);
                    journalDetailDto.setBalanceType(balanceType);
                    journalDetailDto.setSequence(journalDetail.getSequence());
                    journalDetailDto.setJournal(journalDetailEditDto.getJournal());
                    System.out.println("Edit : " + journalDetailDto);
                }

                journalDetailDtoList.add(journalDetailDto);
            }
        }

        if (currentDebet.compareTo(currentCredit) == 0){
            for (JournalDetailDto journalDetail : journalDetailDtoList){
                JournalDetail journalDetailSave = new JournalDetail();
                BeanUtils.copyProperties(journalDetail, journalDetailSave);
                journalDetailDao.save(journalDetailSave);
            }
            journalDetailEditDto.setValidation(Boolean.TRUE);
            journalDetailEditDto.setMessage("Data Berhasil Dirubah.");
        }else {
            journalDetailEditDto.setValidation(Boolean.FALSE);
            journalDetailEditDto.setMessage("Data Gagal Dirubah, " +
                    "dikarenakan tidak seimbang antara debet dan credit.");
            journalDetailEditDto.setJournalDetailDtoList(journalDetailDtoList);
            journalDetailEditDto.setTotalDebet(currentDebet);
            journalDetailEditDto.setTotalCredit(currentCredit);
        }

        return journalDetailEditDto;
    }



    public CreateJournalByApiDto saveJournalByApi(CreateJournalByApiDto createJournalByApiDto){
        Institut institut = institutDao.findById(createJournalByApiDto.getInstitut()).get();

        Journal journal = new Journal();
        journal.setTransactionDate(createJournalByApiDto.getDateTransaction());
        journal.setDescription(createJournalByApiDto.getDescription());
        journal.setTransactionTime(LocalDateTime.now());
        journal.setInstitut(institut);
        journal.setJournalType(JournalType.NORMAL);

        JournalTemplate journalTemplate = journalTemplateDao.findByCodeAndStatus(createJournalByApiDto.getCodeTemplate(), StatusRecord.ACTIVE);
        if (journalTemplate != null){
            List<JournalTemplateDetail> journalTemplateDetails = journalTemplateDetailDao.findByJournalTemplateAndStatus(journalTemplate, StatusRecord.ACTIVE);

            if (TypeApp.SPMB.equals(journalTemplate.getTypeApp())){
                journal.setTags("SPMB");
            }else {
                journal.setTags(createJournalByApiDto.getTags());
            }
            BigDecimal debet = BigDecimal.valueOf(0);
            BigDecimal credit = BigDecimal.valueOf(0);

            ArrayList<JournalDetailDto> journalDetailDtoList = new ArrayList<>();
            for (int j = 0; j < journalTemplateDetails.size(); j++) {
                JournalTemplateDetail datas = journalTemplateDetails.get(j);
                // Tukar BalanceType jika reverse
                BalanceType originalBalanceType = datas.getBalanceType();
                BalanceType newBalanceType;
                if (originalBalanceType == BalanceType.DEBET) {
                    newBalanceType = BalanceType.CREDIT;
                } else {
                    newBalanceType = BalanceType.DEBET;
                }

                JournalDetailDto journalDetailDto = new JournalDetailDto();
                journalDetailDto.setJournal(journal);
                journalDetailDto.setAccount(datas.getAccount());
                journalDetailDto.setBalanceType(datas.getBalanceType());
                journalDetailDto.setAmount(createJournalByApiDto.getAmounts());
                journalDetailDto.setSequence(datas.getSequence());

                if (createJournalByApiDto.getType() == CreateJournalByApiDto.TypeJournal.reverse) {
                    journalDetailDto.setBalanceType(newBalanceType); // Set balance type ke newBalanceType saat reverse
                    if (newBalanceType == BalanceType.DEBET) {
                        debet = debet.add(journalDetailDto.getAmount());
                    } else if (newBalanceType == BalanceType.CREDIT) {
                        credit = credit.add(journalDetailDto.getAmount());
                    }
                    System.out.println("Reversed: Sequence " + datas.getSequence() + ", Account " + datas.getAccount().getName() + ", New BalanceType: " + newBalanceType + ", Amout : " + createJournalByApiDto.getAmounts());
                } else {
                    journalDetailDto.setBalanceType(originalBalanceType); // Jika tidak reverse, gunakan balance type asli
                    if (originalBalanceType == BalanceType.DEBET) {
                        debet = debet.add(journalDetailDto.getAmount());
                    } else if (originalBalanceType == BalanceType.CREDIT) {
                        credit = credit.add(journalDetailDto.getAmount());

                    }
                    System.out.println("Normal: Sequence " + datas.getSequence() + ", Account " + datas.getAccount().getName() + ", BalanceType: " + originalBalanceType + ", Tags : " + createJournalByApiDto.getTags());
                }
                journalDetailDtoList.add(journalDetailDto);
            }

            if (debet.compareTo(credit) == 0){
                journal.setStatus(StatusRecord.REVIEW);
                journalDao.save(journal);
                for (JournalDetailDto journalDetail : journalDetailDtoList){
                    JournalDetail journalDetailSave = new JournalDetail();
                    BeanUtils.copyProperties(journalDetail, journalDetailSave);
                    journalDetailSave.setStatus(StatusRecord.REVIEW);
                    journalDetailDao.save(journalDetailSave);

                    log.info("[Save Success] : " + journalDetailSave);
                }
                createJournalByApiDto.setStatusJournal(Boolean.TRUE);
                createJournalByApiDto.setDebet(debet);
                createJournalByApiDto.setCredit(credit);
                createJournalByApiDto.setMessageJournal("Balance");
            }else {
                createJournalByApiDto.setStatusJournal(Boolean.FALSE);
                createJournalByApiDto.setDebet(debet);
                createJournalByApiDto.setCredit(credit);
                createJournalByApiDto.setMessageJournal("Not Balance");
                log.info("[Save Failed] : " + createJournalByApiDto);
            }
        }else {
            createJournalByApiDto.setMessageJournal("Template tidak ditemukan");
        }


        return createJournalByApiDto;
    }



    public CreateJournalByApiArrayDto createJournalByApiArrayDto(CreateJournalByApiArrayDto createJournalByApiArrayDto){
        Institut institut = institutDao.findById(createJournalByApiArrayDto.getInstitut()).get();

        Journal journal = new Journal();
        journal.setTransactionDate(createJournalByApiArrayDto.getDateTransaction());
        journal.setDescription(createJournalByApiArrayDto.getDescription());
        journal.setTransactionTime(LocalDateTime.now());
        journal.setInstitut(institut);
        journal.setJournalType(JournalType.NORMAL);
        JournalTemplate journalTemplate = journalTemplateDao.findByCodeAndStatus(createJournalByApiArrayDto.getCodeTemplate(), StatusRecord.ACTIVE);
        List<JournalTemplateDetail> journalTemplateDetails = journalTemplateDetailDao.findByJournalTemplateAndStatusOrderBySequenceAsc(journalTemplate, StatusRecord.ACTIVE);

        BigDecimal debet = BigDecimal.valueOf(0);
        BigDecimal credit = BigDecimal.valueOf(0);

        ArrayList<JournalDetailDto> journalDetailDtoList = new ArrayList<>();
        for (int j = 0; j < createJournalByApiArrayDto.getAmounts().size() && j < journalTemplateDetails.size(); j++) {
            JournalTemplateDetail datas = journalTemplateDetails.get(j);
            BigDecimal amount = createJournalByApiArrayDto.getAmounts().get(j);

            if (amount.compareTo(BigDecimal.ZERO) != 0) {
                JournalDetailDto journalDetailDto = new JournalDetailDto();
                journalDetailDto.setJournal(journal);
                journalDetailDto.setAccount(datas.getAccount());
                journalDetailDto.setBalanceType(datas.getBalanceType());
                journalDetailDto.setAmount(amount);
                journalDetailDto.setSequence(datas.getSequence());

                System.out.println("Test : " + journalDetailDto);

                if (journalDetailDto.getBalanceType() == BalanceType.DEBET){
                    debet = debet.add(journalDetailDto.getAmount());
                } else if (journalDetailDto.getBalanceType() == BalanceType.CREDIT) {
                    credit = credit.add(journalDetailDto.getAmount());
                }

                journalDetailDtoList.add(journalDetailDto);
            }
        }

        BigDecimal total = debet.add(credit);
        createJournalByApiArrayDto.setTotal(total);
        if (debet.compareTo(credit) == 0){
            if (journalTemplate.getTypeApp() == TypeApp.FINANCE){
                journal.setStatus(StatusRecord.ACTIVE);
            }else {
                journal.setStatus(StatusRecord.REVIEW);
            }
            journalDao.save(journal); // Save Journal

            for (JournalDetailDto journalDetail : journalDetailDtoList){
                JournalDetail journalDetailSave = new JournalDetail();
                BeanUtils.copyProperties(journalDetail, journalDetailSave);
                if (journalTemplate.getTypeApp() == TypeApp.FINANCE){
                    journalDetailSave.setStatus(StatusRecord.ACTIVE);
                }else {
                    journalDetailSave.setStatus(StatusRecord.REVIEW);
                }
                journalDetailDao.save(journalDetailSave); // Save JournalDetail

                log.info("[Save Success] : " + journalDetailSave);
            }

            createJournalByApiArrayDto.setDebet(debet);
            createJournalByApiArrayDto.setCredit(credit);
            createJournalByApiArrayDto.setMessageJournal("Pencatatan journal berhasil");
            createJournalByApiArrayDto.setStatusJournal(Boolean.TRUE);
            createJournalByApiArrayDto.setResponseCode("200");
        } else {
            createJournalByApiArrayDto.setDebet(debet);
            createJournalByApiArrayDto.setCredit(credit);
            createJournalByApiArrayDto.setMessageJournal("Pencatatan journal gagal dikarenakan tidak balance");
            createJournalByApiArrayDto.setStatusJournal(Boolean.FALSE);
            createJournalByApiArrayDto.setResponseCode("400");
            log.info("[Save Failed] : " + createJournalByApiArrayDto);
        }

        return createJournalByApiArrayDto;
    }

    public BigDecimal getSumAmountByAccountDebet(LocalDate startDate, LocalDate endDate,
                                                 String id){
        BigDecimal debet = journalDetailDao.getAmountByAccountTypeDebet(startDate, endDate, id);
        return debet;
    }

    public BigDecimal getSumAmountByAccountCredit(LocalDate startDate, LocalDate endDate,
                                                 String id){
        BigDecimal credit = journalDetailDao.getAmountByAccountTypeCredit(startDate, endDate, id);
        return credit;
    }

    public List<DailyBalance> getBalanceSheet(){
        LocalDate today = LocalDate.now();
        return dailyBalanceDao.findByDateOrderByStartAmountDesc(today);
    }

    public void journalDetailUpdate(JournalDetail journalDetail){
        journalDetailDao.save(journalDetail);
    }

}
