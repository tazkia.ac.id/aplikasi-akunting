package id.ac.tazkia.accounting.service;


import id.ac.tazkia.accounting.dao.JournalTemplateDetailDao;
import id.ac.tazkia.accounting.dto.ValidationTemplateDto;
import id.ac.tazkia.accounting.entity.JournalDetail;
import id.ac.tazkia.accounting.entity.JournalTemplate;
import id.ac.tazkia.accounting.entity.JournalTemplateDetail;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JournalTemplateDetailService {

    @Autowired
    private JournalTemplateDetailDao jounalTemplateDetailDao;

    public Integer getSequenceByJounalTemplate(JournalTemplate journalTemplate){
        Integer countDetailJournal = jounalTemplateDetailDao.getCountByJournalTemplate(journalTemplate.getId());
        Integer sequence = countDetailJournal + 1;
        return sequence;
    }

    public List<Object[]> getValidationJournalTemplate(){
        List<Object[]> validationJournalTemplateDtos = jounalTemplateDetailDao.getValidationJurnalTemplate();
        return validationJournalTemplateDtos;
    }

    public ValidationTemplateDto getValidationJournalTemplateById(String journalTemplate){
        ValidationTemplateDto validationJournalTemplateById = jounalTemplateDetailDao.getValidationJournalTemplateById(journalTemplate);
        return validationJournalTemplateById;
    }
    public void saveJournalTemplateDetail(JournalTemplateDetail journalTemplateDetail){
        jounalTemplateDetailDao.save(journalTemplateDetail);
    }

    public List<JournalTemplateDetail> journalTemplateDetailList(){
        List<JournalTemplateDetail> journalTemplateDetails = jounalTemplateDetailDao.findByStatusOrderBySequenceAsc(StatusRecord.ACTIVE);
        return journalTemplateDetails;
    }

    public List<JournalTemplateDetail> journalTemplateDetailByJournalTemplateList(JournalTemplate journalTemplate){
        List<JournalTemplateDetail> journalTemplateDetails = jounalTemplateDetailDao
                .findByJournalTemplateAndStatusOrderBySequenceAsc(journalTemplate, StatusRecord.ACTIVE);
        return journalTemplateDetails;
    }

    public void deleteJournalTemplateDetail(String id){
        JournalTemplateDetail journalTemplateDetail = jounalTemplateDetailDao.findById(id).get();
        journalTemplateDetail.setStatus(StatusRecord.DELETED);
        jounalTemplateDetailDao.save(journalTemplateDetail);
    }

    public List<JournalTemplateDetail> journalTemplateDetailByJournalTemplate(JournalTemplate journalTemplate){
        List<JournalTemplateDetail> journalTemplateDetailList = jounalTemplateDetailDao.findByStatusAndJournalTemplateOrderBySequenceAsc(
                StatusRecord.ACTIVE, journalTemplate
        );

        return journalTemplateDetailList;
    }
}
