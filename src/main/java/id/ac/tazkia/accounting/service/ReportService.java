package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.*;
import id.ac.tazkia.accounting.dto.*;
import id.ac.tazkia.accounting.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;

@Service @Slf4j
public class ReportService {
    @Autowired
    private JournalDao journalDao;

    @Autowired
    private AccountDao accountDao;

    public List<ReportInterfaceTerbaru> laporanNeracaSaldo(LocalDate startDate, LocalDate endDate, String[] institut) {
        int year = startDate.getYear();
        List<ReportInterfaceTerbaru> reportNeracaSaldoDtoList = journalDao.getNewSqlReport(year, startDate, endDate, institut);
        return reportNeracaSaldoDtoList;
    }

    public ReportInterfaceTotalDto totalNeracaSaldo(LocalDate startDate, LocalDate endDate, String[] institut) {
        int year = startDate.getYear();
        ReportInterfaceTotalDto reportInterfaceTotalDto = journalDao.getTotalReport(year, startDate, endDate, institut);
        return reportInterfaceTotalDto;
    }

    public List<WoorkSheetAllDto> getWoorksheetAll(String tahun) {
        List<WoorkSheetAllDto> woorkSheetAll = journalDao.getWoorksheetAll(tahun);
        return woorkSheetAll;
    }


    public List<ReportInterfaceTerbaru> laporanNeraca(LocalDate startDate, LocalDate endDate, String[] institut) {
        int year = startDate.getYear();
        List<ReportInterfaceTerbaru> reportNeracaSaldoDtoList = journalDao.getNewSqlReport(year, startDate, endDate, institut);

        // Filter hanya yang memiliki accountType AKTIVA, PASIVA, atau EQUITAS
        return reportNeracaSaldoDtoList.stream()
                .filter(report -> {
                    String type = report.getAccountType();
                    return "AKTIVA".equals(type) || "PASIVA".equals(type) || "EQUITY".equals(type);
                })
                .collect(Collectors.toList());
    }

    public List<ReportInterfaceTerbaru> laporanLabaRugi(LocalDate startDate, LocalDate endDate, String[] institut) {
        int year = startDate.getYear();
        List<ReportInterfaceTerbaru> reportNeracaSaldoDtoList = journalDao.getNewSqlReport(year, startDate, endDate, institut);

        // Filter hanya yang memiliki accountType AKTIVA, PASIVA, atau EQUITAS
        return reportNeracaSaldoDtoList.stream()
                .filter(report -> {
                    String type = report.getAccountType();
                    return "REVENUE".equals(type) || "EXPENSE".equals(type);
                })
                .collect(Collectors.toList());
    }


}
