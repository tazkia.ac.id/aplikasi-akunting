package id.ac.tazkia.accounting.service;

import id.ac.tazkia.accounting.dao.JournalDao;
import id.ac.tazkia.accounting.dao.JournalDetailDao;
import id.ac.tazkia.accounting.dao.MonthDao;
import id.ac.tazkia.accounting.dao.YearDao;
import id.ac.tazkia.accounting.dto.ParamsJournalDto;
import id.ac.tazkia.accounting.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class JournalService {


    @Autowired
    private MonthDao monthDao;

    @Autowired
    private YearDao yearDao;

    @Autowired
    private JournalDao journalDao;

    @Autowired
    private JournalDetailDao journalDetailDao;


    public List<Month> getMonth(){
        List<Month> monthList = monthDao.findByStatusOrderByNumberAsc(StatusRecord.ACTIVE);
        return monthList;
    }

    public List<Year> getYear(){
        List<Year> yearList = yearDao.findByStatusOrderByYearAsc(StatusRecord.ACTIVE);
        return yearList;
    }

    public Page<Journal> getAllJournal(Pageable pageable){
        Page<Journal> journals = journalDao.findByStatusOrderByTransactionDateDesc(StatusRecord.ACTIVE, pageable);
        return journals;
    }

    public List<Journal> getDateJournal(StatusRecord statusRecord, LocalDate startDate, LocalDate endDate){
        List<Journal> journalList = journalDao.findByTransactionDateBetweenAndStatusOrderByTransactionDate(startDate, endDate, statusRecord);
        return journalList;
    }

    public void deleteJournal(Journal journal){
        journal.setStatus(StatusRecord.DELETED);
        journalDao.save(journal);

        List<JournalDetail> journalDetails = journalDetailDao.findByStatusAndJournalOrderBySequence(StatusRecord.ACTIVE, journal);
        for (JournalDetail journalDetail : journalDetails) {
            journalDetail.setStatus(StatusRecord.DELETED);
            journalDetailDao.save(journalDetail);
        }
    }

    public Page<Journal> getJournalReview(Pageable pageable){
        Page<Journal> journalPage = journalDao.findByStatusOrderByTransactionDateDescTransactionTimeDescIdAsc(StatusRecord.REVIEW, pageable);
        return journalPage;
    }

    public List<JournalDetail> getJournalDetail(StatusRecord statusRecord){
        List<JournalDetail> journalDetails = journalDetailDao.findByStatusOrderBySequenceAsc(statusRecord);
        return journalDetails;
    }

    public Journal getJournalFindById(String id){
        return journalDao.findById(id).get();
    }

    public Page<Journal> getJournal(ParamsJournalDto paramsJournalDto, StatusRecord statusRecord,Pageable pageable, String urutanTanggal){
        Page<Journal> journalPage;

        if (urutanTanggal.equals("REVIEW")){
            if (paramsJournalDto.getInstitut() != null && paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null) {
                journalPage = journalDao.findByInstitutAndStatusAndTransactionDateBetweenOrderByTransactionDateAsc(
                        paramsJournalDto.getInstitut(),
                        statusRecord,
                        paramsJournalDto.getStartDate(),
                        paramsJournalDto.getEndDate(),
                        pageable
                );
            }else if(paramsJournalDto.getApp() != null && paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null) {
                journalPage = journalDao.findByStatusAndTransactionDateBetweenAndTagsContainingIgnoreCaseOrderByTransactionDateAsc(
                        statusRecord, paramsJournalDto.getStartDate(), paramsJournalDto.getEndDate(), paramsJournalDto.getApp(), pageable
                );
            } else if (paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null && paramsJournalDto.getSearch() != null) {
                journalPage = journalDao.findByStatusAndTransactionDateBetweenAndDescriptionContainingIgnoreCaseOrderByTransactionDateAsc(
                        statusRecord, paramsJournalDto.getStartDate(), paramsJournalDto.getEndDate(), paramsJournalDto.getSearch(), pageable
                );
            } else if (paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null) {
                journalPage = journalDao.findByStatusAndTransactionDateBetweenOrderByTransactionDateAsc(
                        statusRecord, paramsJournalDto.getStartDate(), paramsJournalDto.getEndDate(), pageable
                );
            } else if (paramsJournalDto.getSearch() != null && paramsJournalDto.getInstitut() != null) {
                journalPage = journalDao.findByStatusAndInstitutAndDescriptionContainingIgnoreCaseOrderByTransactionDateAsc(
                        statusRecord, paramsJournalDto.getInstitut(), paramsJournalDto.getSearch(), pageable
                );
            } else if (paramsJournalDto.getInstitut() != null) {
                journalPage = journalDao.findByStatusAndInstitutOrderByTransactionDateDesc(statusRecord, paramsJournalDto.getInstitut(), pageable);
            } else if (paramsJournalDto.getSearch() != null && !paramsJournalDto.getSearch().isEmpty()) {
                journalPage = journalDao.findByStatusAndDescriptionContainingIgnoreCaseOrderByTransactionDateAsc(
                        statusRecord, paramsJournalDto.getSearch(), pageable
                );
            } else if (paramsJournalDto.getApp() != null) {
                journalPage = journalDao.findByStatusAndTagsContainingIgnoreCaseOrderByTransactionDateAsc(statusRecord, paramsJournalDto.getApp(), pageable);
            } else {
                journalPage = journalDao.findByStatusOrderByTransactionDateAsc(statusRecord, pageable);
            }

            return journalPage;
        }else {
            if (paramsJournalDto.getInstitut() != null && paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null) {
                journalPage = journalDao.findByInstitutAndStatusAndTransactionDateBetweenOrderByTransactionDateDesc(
                        paramsJournalDto.getInstitut(),
                        statusRecord,
                        paramsJournalDto.getStartDate(),
                        paramsJournalDto.getEndDate(),
                        pageable
                );
            }else if(paramsJournalDto.getApp() != null && paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null) {
                journalPage = journalDao.findByStatusAndTransactionDateBetweenAndTagsContainingIgnoreCaseOrderByTransactionDateDesc(
                        statusRecord, paramsJournalDto.getStartDate(), paramsJournalDto.getEndDate(), paramsJournalDto.getApp(), pageable
                );
            } else if (paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null && paramsJournalDto.getSearch() != null) {
                journalPage = journalDao.findByStatusAndTransactionDateBetweenAndDescriptionContainingIgnoreCaseOrderByTransactionDateDesc(
                        statusRecord, paramsJournalDto.getStartDate(), paramsJournalDto.getEndDate(), paramsJournalDto.getSearch(), pageable
                );
            } else if (paramsJournalDto.getStartDate() != null && paramsJournalDto.getEndDate() != null) {
                journalPage = journalDao.findByStatusAndTransactionDateBetweenOrderByTransactionDateDesc(
                        statusRecord, paramsJournalDto.getStartDate(), paramsJournalDto.getEndDate(), pageable
                );
            } else if (paramsJournalDto.getSearch() != null && paramsJournalDto.getInstitut() != null) {
                journalPage = journalDao.findByStatusAndInstitutAndDescriptionContainingIgnoreCaseOrderByTransactionDateDesc(
                        statusRecord, paramsJournalDto.getInstitut(), paramsJournalDto.getSearch(), pageable
                );
            } else if (paramsJournalDto.getInstitut() != null) {
                journalPage = journalDao.findByStatusAndInstitutOrderByTransactionDateDesc(statusRecord, paramsJournalDto.getInstitut(), pageable);
            } else if (paramsJournalDto.getSearch() != null && !paramsJournalDto.getSearch().isEmpty()) {
                journalPage = journalDao.findByStatusAndDescriptionContainingIgnoreCaseOrderByTransactionDateDesc(
                        statusRecord, paramsJournalDto.getSearch(), pageable
                );
            } else if (paramsJournalDto.getApp() != null) {
                journalPage = journalDao.findByStatusAndTagsContainingIgnoreCaseOrderByTransactionDateDesc(statusRecord, paramsJournalDto.getApp(), pageable);
            } else {
                journalPage = journalDao.findByStatusOrderByTransactionDateDesc(statusRecord, pageable);
            }

            return journalPage;
        }

    }

    public void updateJournal(Journal journal){
        journalDao.save(journal);
    }




}
