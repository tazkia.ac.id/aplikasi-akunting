package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.InstitutDao;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.service.AccountService;
import id.ac.tazkia.accounting.service.ClassificationService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.internal.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.expression.Strings;

import java.util.List;

@Controller @Slf4j
public class AccountController {

    @Autowired
    public AccountService accountService;


    @Autowired
    public ClassificationService classificationService;

    @Autowired
    public InstitutDao institutDao;

    @ModelAttribute("institut")
    public List<Institut> institut() {
        List<Institut> institutList = institutDao.findByStatus(StatusRecord.ACTIVE);
        return institutList;
    }

    @GetMapping("/account")
    public String getListAccount(Model model,@RequestParam(required = false) String search,
                                 @PageableDefault Pageable pageable){
        model.addAttribute("setting", "active");
        model.addAttribute("accountactive", "active");
        if (StringUtils.hasText(search)){
            model.addAttribute("list", accountService.getAccountSearch(search));
        }else {
            model.addAttribute("list", accountService.getAccountPage(pageable));
        }

        model.addAttribute("classification", classificationService.getSubclassificationForSelect());
        return "account/list";
    }

    @PostMapping("/account/save")
    public String saveAccount(@Valid Account account, RedirectAttributes attributes){
        try{
            accountService.saveAccount(account);
            attributes.addFlashAttribute("success", "Saved successfully");
            return "redirect:/account";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error(e.getMessage());
            return "redirect:/account";
        }
    }

    @GetMapping("/account/delete")
    public String deleteAccount(@RequestParam String id, RedirectAttributes attributes){
        try {
            Boolean validation = accountService.deleteAccount(id);
            if (validation == true){
                attributes.addFlashAttribute("success", "Deleted successfully");
            }else {
                attributes.addFlashAttribute("error", "Deleted Failed : " +
                        "Tidak dapat dihapus karena data tersebut telah tersimpan di dalam jurnal detail.");
            }
            return "redirect:/account";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error(e.getMessage());
            return "redirect:/account";
        }
    }

    @GetMapping("/account/edit")
    public String editAccount(Model model,@RequestParam Account account){
        try {
            model.addAttribute("setting", "active");
            model.addAttribute("accountactive", "active");
            model.addAttribute("account", account);
            model.addAttribute("classification", classificationService.getSubclassificationForSelect());
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "account/edit";
    }
}
