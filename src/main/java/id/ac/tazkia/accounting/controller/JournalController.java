package id.ac.tazkia.accounting.controller;


import id.ac.tazkia.accounting.dao.AccountDao;
import id.ac.tazkia.accounting.dao.InstitutDao;
import id.ac.tazkia.accounting.dao.MonthDao;
import id.ac.tazkia.accounting.dao.YearDao;
import id.ac.tazkia.accounting.dto.*;
import id.ac.tazkia.accounting.entity.*;
import id.ac.tazkia.accounting.entity.config.User;
import id.ac.tazkia.accounting.service.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller @Slf4j
public class JournalController {

    @Autowired
    private JournalTemplateDetailService journalTemplateDetailService;

    @Autowired
    private JournalDetailService journalDetailService;

    @Autowired
    private JournalService journalService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private InstitutDao institutDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private ReportService reportService;

    @Autowired
    private MonthDao monthDao;

    @Autowired
    private YearDao yearDao;

    @ModelAttribute("month")
    public List<Month> monthList(){
        return monthDao.findByStatusOrderByNumberAsc(StatusRecord.ACTIVE);
    }

    @ModelAttribute("year")
    public List<Year> yearList(){
        return yearDao.findByStatusOrderByYearAsc(StatusRecord.ACTIVE);
    }

    @ModelAttribute("institut")
    public List<Institut> institut() {
        List<Institut> institutList = institutDao.findByStatus(StatusRecord.ACTIVE);
        return institutList;
    }

    @ModelAttribute("journalmenu")
    public String journal() {
        return "active";
    }


    @GetMapping("/journal")
    public String getListJournal(Model model, @RequestParam(required = false)JournalCategoryTemplate category,
                                 @RequestParam(required = false)JournalTemplate template){
        try{
            model.addAttribute("journalactive", "active");
            model.addAttribute("category", category);
            model.addAttribute("template", template);
            model.addAttribute("templateDetail", journalTemplateDetailService.journalTemplateDetailByJournalTemplate(template));
            if (template != null){
                model.addAttribute("templateDetailValidation",
                        journalTemplateDetailService.getValidationJournalTemplateById(template.getId())
                );
            }
        }catch (Exception e){
            log.error("Error : " + e.getMessage());
        }

        return "journal/form";
    }

    @PostMapping("/journal/create")
    public String journalCreate(@Valid CreateJournalDto createJournalDto, @RequestParam String[] amount,
                                RedirectAttributes attributes, Authentication authentication){
        try {
            User user = currentUserService.currentUser(authentication);
            createJournalDto.setCreatedBy(user);
            CreateJournalDto resultJournal = journalDetailService.createJournalUseUserInterface(createJournalDto, amount);
            if (createJournalDto.getStatusJournal() == Boolean.TRUE){
                attributes.addFlashAttribute("success", "Proses pencatatan jurnal berhasil dilakukan.");
            }else {
                attributes.addFlashAttribute("failed", "Pembuatan jurnal gagal karena jumlahnya tidak seimbang. " +
                        "Mohon periksa dan ubah nilai amount yang sesuai.");
            }
            attributes.addFlashAttribute("message", resultJournal.getJournalDetailDtoList());
            attributes.addFlashAttribute("totalDebet", resultJournal.getDebetSatu());
            attributes.addFlashAttribute("totalCredit", resultJournal.getCreditSatu());
            attributes.addFlashAttribute("messageJournal", resultJournal.getMessageJournal());
            return "redirect:/journal?category=" + resultJournal.getJournalTemplate().getJournalCategoryTemplate().getId() +
                    "&template=" + resultJournal.getJournalTemplate().getId();
        }catch (Exception e) {
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            return "redirect:/journal";
        }
    }

    @GetMapping("/journal/results/{journal}")
    public String resultsGenerateJournal(@PathVariable Journal journal, Model model){
        model.addAttribute("journalactive", "active");
        model.addAttribute("journal", journal);
        model.addAttribute("journalDetailList", journalDetailService.getJournalDetailByJournal(journal, StatusRecord.ACTIVE));
        model.addAttribute("totalDebet", journalDetailService.getTotalAmountDebet(journal.getId()));
        model.addAttribute("totalCredit", journalDetailService.getTotalAmountCredit(journal.getId()));
        return "journal/results";
    }

    @GetMapping("/journal/data")
    public String listJournalData(Model model,
                                  @PageableDefault(size = 30) Pageable pageable,
                                  ParamsJournalDto paramsJournalDto){
        try{
            Page<Journal> journalPage = journalService.getJournal(paramsJournalDto,StatusRecord.ACTIVE, pageable, "DATA");
            model.addAttribute("journallist", "active");
            model.addAttribute("search", paramsJournalDto.getSearch());
            model.addAttribute("institutParams", paramsJournalDto.getInstitut());
            model.addAttribute("startDate", paramsJournalDto.getStartDate());
            model.addAttribute("endDate", paramsJournalDto.getEndDate());
            model.addAttribute("journal", journalPage);
            model.addAttribute("journalDetail", journalDetailService
                    .getJournalDetailByPageJournal(journalPage, StatusRecord.ACTIVE));
        }catch (Exception e){
            log.error("Error : " + e.getMessage());
        }
        return "journal/list";
    }

    @GetMapping("/journal/data/detail")
    public String journalDetail(Model model, @RequestParam Journal journal){
        model.addAttribute("journallist", "active");
        model.addAttribute("journal", journal);

        model.addAttribute("journalDetail", journalDetailService.getJournalDetailByJournal(journal, StatusRecord.ACTIVE));
        model.addAttribute("accountList", accountService.getAllAccount());
        return "journal/detail";
    }

    @GetMapping("/journal/woorksheet")
    public String getJournalWoorksheet(Model model, @RequestParam(required = false) Account account,
                                       @RequestParam(required = false) LocalDate startDate,
                                       @RequestParam(required = false)  LocalDate endDate){
        LocalDate today = LocalDate.now();
        LocalDate yesterdayDate = today.minusDays(1);
        model.addAttribute("woorksheetactive", "active");
        model.addAttribute("accountList", accountService.getAllAccount());
        model.addAttribute("account", account);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        if (account != null){
            model.addAttribute("journalDetailList", journalDetailService.getJournalDetailByAccount(
                   startDate, endDate, account.getId()));
            model.addAttribute("dateYesterday", yesterdayDate);
            model.addAttribute("currentBalance", accountService.getCurrentBalanceAccount(account));
            model.addAttribute("debet", journalDetailService.getSumAmountByAccountDebet(
                    startDate, endDate, account.getId()
            ));
            model.addAttribute("credit", journalDetailService.getSumAmountByAccountCredit(
               startDate, endDate, account.getId()
            ));
        }
        return "journal/woorksheet";
    }

    @GetMapping("/journal/woorksheetall")
    public String getWoorkSheetAll(Model model,
                                   @RequestParam(required = false) String tahun,
                                   @RequestParam(required = false) String bulan,
                                   @ModelAttribute("month") List<Month> monthList,
                                   @ModelAttribute("year") List<Year> yearList) {
        model.addAttribute("woorksheetactiveall", "active");

        // Ambil tahun saat ini
        String currentYear = String.valueOf(LocalDate.now().getYear());

        // Jika tahun kosong, gunakan tahun saat ini
        if (tahun == null || tahun.isEmpty()) {
            tahun = currentYear;
        }

        // Jika bulan kosong, ambil bulan pertama dari daftar bulan
        if (bulan == null || bulan.isEmpty()) {
            bulan = monthList.isEmpty() ? "1" : String.valueOf(monthList.get(0).getId());
        }

        model.addAttribute("tahun", tahun);
        model.addAttribute("bulan", bulan);

        // Ambil data worksheet berdasarkan tahun dan bulan
        List<WoorkSheetAllDto> woorkSheetAll = reportService.getWoorksheetAll(tahun);
        List<Account> accountList = accountDao.findByStatusOrderByCode(StatusRecord.ACTIVE);

        // Mengelompokkan Account berdasarkan kode
        Map<String, WorksheetAllDataDto> groupedData = new LinkedHashMap<>();

        for (Account account : accountList) {
            groupedData.put(account.getCode(), new WorksheetAllDataDto(account.getName(), new ArrayList<>()));
        }

        // Mengelompokkan data transaksi berdasarkan kode akun
        for (WoorkSheetAllDto item : woorkSheetAll) {
            groupedData.getOrDefault(item.getCode(), new WorksheetAllDataDto("", new ArrayList<>()))
                    .getWoorkSheetList().add(item);
        }

        model.addAttribute("groupedData", groupedData);

        return "journal/woorksheetall";
    }



    @GetMapping("/journal/manual")
    public String postJournalManual(Model model){
        model.addAttribute("journalmanualactive", "active");
        return "journal/manual";
    }

    @GetMapping("/journal/penyesuaian")
    public String postJournalPenyesua(Model model){
        model.addAttribute("journalpenyesuaianactive", "active");
        return "journal/penyesuaian";
    }


    @PostMapping("/journal/manual/save")
    public String postJournalManualSave(@Valid CreateJournalDto createJournalDto, @RequestParam String[] account,
                                        @RequestParam String[] debet,
                                        @RequestParam String[] credit,
                                        RedirectAttributes attributes, Authentication authentication){
        try {
            User user = currentUserService.currentUser(authentication);
            createJournalDto.setCreatedBy(user);
            for (int i = 0; i < debet.length; i++) {
                debet[i] = journalDetailService.cleanAndFormatValue(debet[i]);
            }

            for (int i = 0; i < credit.length; i++) {
                credit[i] = journalDetailService.cleanAndFormatValue(credit[i]);
            }
            CreateJournalDto resultJournal = journalDetailService.createJournalManual(createJournalDto, account, debet, credit);
            if (createJournalDto.getStatusJournal() == Boolean.TRUE){
                attributes.addFlashAttribute("success", resultJournal.getMessageJournal());
            }else {
                attributes.addFlashAttribute("message", resultJournal.getJournalDetailDtoList());
                attributes.addFlashAttribute("totalDebet", resultJournal.getDebetSatu());
                attributes.addFlashAttribute("totalCredit", resultJournal.getCreditSatu());
                attributes.addFlashAttribute("messageJournal", resultJournal.getMessageJournal());
                attributes.addFlashAttribute("failed", resultJournal.getMessageJournal()        );
            }

            if (createJournalDto.getJournalType() == JournalType.NORMAL){
                return "redirect:/journal/manual";
            }else {
                return "redirect:/journal/penyesuaian";
            }



        }catch (Exception e) {
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            return "redirect:/journal/manual";
        }

    }

    @PostMapping("/journalDetail/edit")
    public String editJournalDetail(@Valid JournalDetailEditDto journalDetailEditDto,
                                    RedirectAttributes attributes)throws Exception{
        String url = "";
        try{
            JournalDetailEditDto prosesMengubahData =
                    journalDetailService.journalDetailEdit(journalDetailEditDto);
            if (prosesMengubahData.getValidation() == Boolean.TRUE){
                attributes.addFlashAttribute("success",
                        "Proses perubahan jurnal detail berhasil dilakukan.");
                if (prosesMengubahData.getJournal().getStatus() == StatusRecord.ACTIVE){
                    url = "redirect:/journal/data/detail?journal=" + journalDetailEditDto.getJournal().getId();
                } else if (prosesMengubahData.getJournal().getStatus() == StatusRecord.REVIEW) {
                    url = "redirect:/journal/edit-review/" + journalDetailEditDto.getJournal().getId();
                }
            }else {
                attributes.addFlashAttribute("message", prosesMengubahData.getJournalDetails());
                attributes.addFlashAttribute("totalDebet", prosesMengubahData.getTotalDebet());
                attributes.addFlashAttribute("totalCredit", prosesMengubahData.getTotalCredit());
                attributes.addFlashAttribute("messageJournal", prosesMengubahData.getMessage());

                if (prosesMengubahData.getJournal().getStatus() == StatusRecord.ACTIVE){
                    url = "redirect:/journal/data/detail?journal=" + journalDetailEditDto.getJournal().getId();
                } else if (prosesMengubahData.getJournal().getStatus() == StatusRecord.REVIEW) {
                    url = "redirect:/journal/edit-review/" + journalDetailEditDto.getJournal().getId();
                }

            }

        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            url = "redirect:/journal/data/detail?journal=" + journalDetailEditDto.getJournal().getId();
        }

        return url;
    }

    @GetMapping("/journal/delete")
    public String journalDelete(@RequestParam Journal journal, RedirectAttributes attributes){
        try {
            journalService.deleteJournal(journal);
            attributes.addFlashAttribute("success",
                    "Data Berhasil di hapus.");
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error("Error {} : " + e.getMessage());
        }
        return "redirect:/journal/data";
    }

    @GetMapping("/journal/review")
    public String reviewJournal(Model model, @PageableDefault Pageable pageable,
                                ParamsJournalDto paramsJournalDto){
        model.addAttribute("review", "active");
        model.addAttribute("search", paramsJournalDto.getSearch());
        model.addAttribute("startDate", paramsJournalDto.getStartDate());
        model.addAttribute("endDate", paramsJournalDto.getEndDate());
        model.addAttribute("indexRow", true);
        model.addAttribute("typeApp", TypeApp.values());
        Page<Journal> journalPage = journalService.getJournal(paramsJournalDto, StatusRecord.REVIEW, pageable, "REVIEW");
        model.addAttribute("journal", journalPage);
        model.addAttribute("journalDetail", journalDetailService
                .getJournalDetailByPageJournal(journalPage, StatusRecord.REVIEW));
        return "journal/review";
    }

    @GetMapping("/journal/update")
    public String updateJournal(@RequestParam Journal journal, RedirectAttributes attributes){
        journal.setStatus(StatusRecord.ACTIVE);
        List<JournalDetail> journalDetails = journalDetailService.getJournalDetailByJournal(journal, StatusRecord.REVIEW);
        for (JournalDetail journalDetail : journalDetails) {
            journalDetail.setStatus(StatusRecord.ACTIVE);
            journalDetailService.journalDetailUpdate(journalDetail);
        }
        journalService.updateJournal(journal);
        attributes.addFlashAttribute("success", "Berhasil di catat ke jurnal.");
        return "redirect:/journal/review";
    }

    @GetMapping("/journal/edit-review/{journal}")
    public String editReview(Model model, @PathVariable Journal journal){
        model.addAttribute("review", "active");
        model.addAttribute("journal", journal);
        model.addAttribute("journalDetail", journalDetailService.getJournalDetailByJournal(journal, StatusRecord.REVIEW));
        model.addAttribute("accountList", accountService.getAllAccount());

        return "journal/reviewEdit";
    }

    @PostMapping("/journal/review/updatestatus")
    public String reviewUpdateStatus(@RequestParam String[] data, RedirectAttributes attributes){
        for (String idjournal : data) {
            Journal journal = journalService.getJournalFindById(idjournal);
            List<JournalDetail> journalDetails = journalDetailService.getJournalDetailByJournal(journal, StatusRecord.REVIEW);
            for (JournalDetail journalDetail : journalDetails) {
                journalDetail.setStatus(StatusRecord.ACTIVE);
                journalDetailService.journalDetailUpdate(journalDetail);
            }
            journal.setStatus(StatusRecord.ACTIVE);
            journalService.updateJournal(journal);
        }
        attributes.addFlashAttribute("success", "Berhasil di catat ke jurnal.");
        return "redirect:/journal/review";
    }

}
