package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.InstitutDao;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.service.DailyBalanceService;
import id.ac.tazkia.accounting.service.JournalDetailService;
import id.ac.tazkia.accounting.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@Controller
public class ReportController {
    @Autowired
    private JournalDetailService journalDetailService;

    @Autowired
    private ReportService reportService;

    @Autowired
    public InstitutDao institutDao;

    @ModelAttribute("institut")
    public List<Institut> institut() {
        List<Institut> institutList = institutDao.findByStatus(StatusRecord.ACTIVE);
        return institutList;
    }

    @GetMapping("/report/neraca-saldo")
    public String getReportNeracaSaldo(Model model, @RequestParam(required = false) LocalDate startDate,
                                       @RequestParam(required = false) LocalDate endDate,
                                       @RequestParam(required = false) String[] institut) {
        int currentYear = LocalDate.now().getYear();
        LocalDate firstJanuary = LocalDate.of(currentYear, 1, 1);
        LocalDate dateNow = LocalDate.now();
        model.addAttribute("laporan", "active");
        if (startDate == null || endDate == null) {
            model.addAttribute("startDate", firstJanuary);
            model.addAttribute("endDate", dateNow);
            model.addAttribute("institutParams", institut);
            model.addAttribute("listData", reportService.laporanNeracaSaldo(firstJanuary, dateNow, institut));
            model.addAttribute("total", reportService.totalNeracaSaldo(firstJanuary, dateNow, institut));
            System.out.println("Data - "+ firstJanuary + " : " + dateNow);
        }else {
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            model.addAttribute("institutParams", institut);
            model.addAttribute("listData", reportService.laporanNeracaSaldo(startDate, endDate, institut));
            model.addAttribute("total", reportService.totalNeracaSaldo(startDate, endDate, institut));
            System.out.println("Data - "+ startDate + " : " + endDate);
        }


        return "report/neracaSaldo";
    }

    @GetMapping("/report/neraca")
    public String getReportNeraca(Model model, @RequestParam(required = false) LocalDate startDate,
                                       @RequestParam(required = false) LocalDate endDate,
                                       @RequestParam(required = false) String[] institut) {
        int currentYear = LocalDate.now().getYear();
        LocalDate firstJanuary = LocalDate.of(currentYear, 1, 1);
        LocalDate dateNow = LocalDate.now();
        model.addAttribute("neraca", "active");
        if (startDate == null || endDate == null) {
            model.addAttribute("startDate", firstJanuary);
            model.addAttribute("endDate", dateNow);
            model.addAttribute("institutParams", institut);
            model.addAttribute("listData", reportService.laporanNeraca(firstJanuary, dateNow, institut));
            System.out.println("Data - "+ firstJanuary + " : " + dateNow);
        }else {
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            model.addAttribute("institutParams", institut);
            model.addAttribute("listData", reportService.laporanNeraca(startDate, endDate, institut));
            System.out.println("Data - "+ startDate + " : " + endDate);
        }


        return "journal/neraca";
    }

    @GetMapping("/report/labarugi")
    public String getLabaRugi(Model model, @RequestParam(required = false) LocalDate startDate,
                              @RequestParam(required = false) LocalDate endDate,
                              @RequestParam(required = false) String[] institut){
        int currentYear = LocalDate.now().getYear();
        LocalDate firstJanuary = LocalDate.of(currentYear, 1, 1);
        LocalDate dateNow = LocalDate.now();
        model.addAttribute("labarugi", "active");
        if (startDate == null || endDate == null) {
            model.addAttribute("startDate", firstJanuary);
            model.addAttribute("endDate", dateNow);
            model.addAttribute("institutParams", institut);
            model.addAttribute("listData", reportService.laporanLabaRugi(firstJanuary, dateNow, institut));
            System.out.println("Data - "+ firstJanuary + " : " + dateNow);
        }else {
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            model.addAttribute("institutParams", institut);
            model.addAttribute("listData", reportService.laporanLabaRugi(startDate, endDate, institut));
            System.out.println("Data - "+ startDate + " : " + endDate);
        }


        return "journal/labarugi";
    }



}
