package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.entity.JournalDetail;
import id.ac.tazkia.accounting.entity.JournalTemplate;
import id.ac.tazkia.accounting.entity.JournalTemplateDetail;
import id.ac.tazkia.accounting.entity.TypeApp;
import id.ac.tazkia.accounting.service.AccountService;
import id.ac.tazkia.accounting.service.JournalCategoryTemplateService;
import id.ac.tazkia.accounting.service.JournalTemplateDetailService;
import id.ac.tazkia.accounting.service.JournalTemplateService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.internal.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller @Slf4j
public class JournalTemplateController {
    @Autowired
    private JournalTemplateService journalTemplateService;

    @Autowired
    private JournalCategoryTemplateService journalCategoryTemplateService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JournalTemplateDetailService journalTemplateDetailService;

    @GetMapping("/journal-template")
    public String listJournalTemplate(Model model, @PageableDefault Pageable pageable, @RequestParam(required = false) TypeApp typeApp,
                                      @RequestParam(required = false) String search){
        model.addAttribute("setting", "active");
        model.addAttribute("journalTemplate", "active");
        model.addAttribute("typeApp", TypeApp.values());
        model.addAttribute("listCategory", journalCategoryTemplateService.getListAllCategoryJournal());

        model.addAttribute("search", search);
        if (StringUtils.hasText(search)) {
            model.addAttribute("listJournalTemplate", journalTemplateService.journalTemplatesBySearch(search, pageable));
        } else if (typeApp != null && StringUtils.hasText(search)){
            model.addAttribute("listJournalTemplate", journalTemplateService.getJournalByTypeAndSearchNameOrCode(typeApp, search, pageable));
        } else if (typeApp != null){
            model.addAttribute("listJournalTemplate", journalTemplateService.getJournalByType(typeApp, pageable));
        }else{
            model.addAttribute("listJournalTemplate", journalTemplateService.journalTemplatePage(pageable));
        }
        model.addAttribute("listDetailJournalTemplate", journalTemplateDetailService.journalTemplateDetailList());


        model.addAttribute("journalValidation", journalTemplateDetailService.getValidationJournalTemplate());
        return "journalTemplate/list";
    }

    @PostMapping("/journal-template/save")
    public String saveJournalTemplate(@Valid JournalTemplate journalTemplate, RedirectAttributes attributes){
        try{
            journalTemplateService.saveJournalTemplate(journalTemplate);
            attributes.addFlashAttribute("success", "Saved successfully");
            return "redirect:/journal-template";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            return "redirect:/journal-template";
        }
    }

    @GetMapping("/journal-template/delete")
    public String deleteJournalTemplate(@RequestParam String id,
                                                RedirectAttributes attributes){
        try {
            journalTemplateService.deleteJournalTemplate(id);
            attributes.addFlashAttribute("success", "Deleted successfully");
            return "redirect:/journal-template";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error(e.getMessage());
            return "redirect:/journal-template";
        }
    }

    @GetMapping("/journal-template/detail")
    public String formAddJournalTemplateDetail(Model model, @RequestParam JournalTemplate journalTemplate){
        model.addAttribute("setting", "active");
        model.addAttribute("journalTemplate", "active");
        model.addAttribute("journalTemplate", journalTemplate);
        model.addAttribute("sequence", journalTemplateDetailService.getSequenceByJounalTemplate(journalTemplate));
        model.addAttribute("listAccount", accountService.getAllAccount());
        model.addAttribute("listDetail", journalTemplateDetailService.journalTemplateDetailByJournalTemplateList(journalTemplate));
        return "journalTemplate/form";
    }

    @PostMapping("/journal-template/detail/save")
    public String saveJournalTemplateDetail(@Valid JournalTemplateDetail journalTemplateDetail,
                                            RedirectAttributes attributes){
        try{
            journalTemplateDetailService.saveJournalTemplateDetail(journalTemplateDetail);
            attributes.addFlashAttribute("success", "Saved Journal Detail successfully");
            return "redirect:/journal-template";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            return "redirect:/journal-template";
        }
    }

    @GetMapping("/journal-template/detail/delete")
    public String deleteJournalTemplateDetail(@RequestParam String id, RedirectAttributes attributes){
        try {
            journalTemplateDetailService.deleteJournalTemplateDetail(id);
            attributes.addFlashAttribute("success", "Deleted successfully");
            return "redirect:/journal-template";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error(e.getMessage());
            return "redirect:/journal-template";
        }
    }

    @GetMapping("/journal-template/edit")
    public String editJournalTemplate(@RequestParam JournalTemplate journalTemplate, Model model){
        try {
            model.addAttribute("journalTemplate", journalTemplate);
            model.addAttribute("typeApp", TypeApp.values());
            model.addAttribute("listCategory", journalCategoryTemplateService.getListAllCategoryJournal());
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "journalTemplate/edit";
    }

    @PostMapping("/journal-template/edit")
    public String editJournalTemplate(@Valid JournalTemplate journalTemplate, RedirectAttributes attributes){
        try {
            journalTemplateService.editJournalTemplate(journalTemplate);
            attributes.addFlashAttribute("success", "Edit successfully");
            return "redirect:/journal-template";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            return "redirect:/journal-template";
        }
    }
}
