package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.YayasanDao;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.Yayasan;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class YayasanController {
    @Autowired
    private YayasanDao yayasanDao;
    @GetMapping("/yayasan")
    public String yayasan(Model model) {
        model.addAttribute("setting", "active");
        model.addAttribute("yayasan", "active");
        model.addAttribute("yayasan", yayasanDao.findByStatus(StatusRecord.ACTIVE));
        return "yayasan/list";
    }

    @PostMapping("/yayasan")
    public String saveYayasan(@Valid Yayasan yayasan) {
        yayasanDao.save(yayasan);
        return "redirect:/yayasan";
    }

    @GetMapping("/yayasan/hapus/{yayasan}")
    public String hapusYayasan(@PathVariable Yayasan yayasan) {
        yayasanDao.delete(yayasan);
        return "redirect:/yayasan";
    }
}
