package id.ac.tazkia.accounting.controller.api;

import id.ac.tazkia.accounting.dao.AccountInstitutDao;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountInstitut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiAccountInstitutController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountInstitutDao accountInstitutDao;
    @GetMapping("/api/accountInstitut/{id}")
    public List<AccountInstitut> getAccountInstitut(@PathVariable String id) {
        Account account = accountService.getAccoountId(id);
        return accountInstitutDao.findByAccountAndStatus(account, StatusRecord.ACTIVE);
    }
}
