package id.ac.tazkia.accounting.controller.api;

import id.ac.tazkia.accounting.dto.BaseResponseApiDto;
import id.ac.tazkia.accounting.dto.ResponseCodeAccountDto;
import id.ac.tazkia.accounting.entity.JournalCategoryTemplate;
import id.ac.tazkia.accounting.entity.JournalTemplate;
import id.ac.tazkia.accounting.entity.TypeApp;
import id.ac.tazkia.accounting.service.JournalCategoryTemplateService;
import id.ac.tazkia.accounting.service.JournalTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController @Slf4j
public class ApiJournalTemplateController {
    @Autowired
    private JournalTemplateService journalTemplateService;

    @Autowired
    private JournalCategoryTemplateService journalCategoryTemplateService;

    @GetMapping("/api/category")
    public ResponseEntity<BaseResponseApiDto> getCategory(){
        try{
            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .data(journalCategoryTemplateService.getListAllCategoryJournal())
                            .build()
            );
        }catch (Exception e){
            log.error("[GET Journal Category] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }
    @GetMapping("/api/category/journal-template")
    public ResponseEntity<BaseResponseApiDto> getJournalByCategory(@RequestParam JournalCategoryTemplate category){
        try{

            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .data(journalTemplateService.getJournalByCategory(category))
                            .build()
            );

        }catch (Exception e){
            log.error("[GET Journal Template] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/api/check/journal-template")
    public ResponseCodeAccountDto checkCodeJournalTemplate(@RequestParam String code){
        JournalTemplate checkCode = journalTemplateService.checkCodeJournalTemplate(code);
        if (checkCode != null){
            ResponseCodeAccountDto responseCodeAccountDto = ResponseCodeAccountDto.builder()
                    .code(HttpStatus.FORBIDDEN).message("Telah Digunakan").build();
            return responseCodeAccountDto;
        }else {
            ResponseCodeAccountDto responseCodeAccountDto = ResponseCodeAccountDto.builder()
                    .code(HttpStatus.NOT_FOUND).message("Belum Digunakan").build();
            return responseCodeAccountDto;
        }
    }

    @GetMapping("/public/api/journal-template")
    public ResponseEntity<BaseResponseApiDto> listJournalTemplate(@RequestParam(required = false)TypeApp type,
                                                                  @RequestParam(required = false)String detail,
                                                                  @RequestParam(required = false) String id){
        try{
            if (type != null){
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                        BaseResponseApiDto.builder()
                                .responseCode("200")
                                .responseMessage("Success")
                                .data(journalTemplateService.getListJournalTemplateByTypeApp(type))
                                .build()
                );
            } else if (detail != null) {
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                        BaseResponseApiDto.builder()
                                .responseCode("200")
                                .responseMessage("Success")
                                .data(journalTemplateService.getJournalTemplateDetail(detail))
                                .build()
                );
            } else if (id != null) {
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                        BaseResponseApiDto.builder()
                                .responseCode("200")
                                .responseMessage("Success")
                                .data(journalTemplateService.getJournalTemplatesById(id))
                                .build()
                );
            } else {
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(
                        BaseResponseApiDto.builder()
                                .responseCode("200")
                                .responseMessage("Success")
                                .data(journalTemplateService.getAllListJournalTemplate())
                                .build()
                );
            }
        }catch (Exception e){
            log.error("[GET Journal Template] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }
}
