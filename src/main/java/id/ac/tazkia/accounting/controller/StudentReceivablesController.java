package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.BillTypeDao;
import id.ac.tazkia.accounting.dao.StudentReceivablesDao;
import id.ac.tazkia.accounting.dto.MahasiswaDto;
import id.ac.tazkia.accounting.dto.StudentReceivablesDto;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.StudentReceivables;
import id.ac.tazkia.accounting.entity.config.User;
import id.ac.tazkia.accounting.service.CurrentUserService;
import jakarta.annotation.PostConstruct;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.internal.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@Slf4j
public class StudentReceivablesController {
    @Autowired
    private StudentReceivablesDao studentReceivablesDao;

    @Autowired
    private BillTypeDao billTypeDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    @Value("${smile.url}")
    private String smileUrl;


    WebClient webClient = WebClient.builder()
            .baseUrl(smileUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    public MahasiswaDto getMahasiswa(String nim) {
        return webClient.get()
                .uri(smileUrl + nim)
                .retrieve()
                .bodyToMono(MahasiswaDto.class)
                .block();
    }

    @GetMapping("/studentReceivables/list")
    public String studentReceivables(Model model, @RequestParam(required = false) String nim, @PageableDefault(size = 10) Pageable pageable) {

        model.addAttribute("piutang", "active");
        model.addAttribute("nim", nim);
        model.addAttribute("billType", billTypeDao.findByStatus(StatusRecord.ACTIVE));
        model.addAttribute("mahasiswa", getMahasiswa(nim));
        if (StringUtils.hasText(nim)){
            model.addAttribute("listPiutang", studentReceivablesDao.findByNimAndStatus(nim, StatusRecord.ACTIVE));
        }else {
            model.addAttribute("listPiutang", studentReceivablesDao.findByStatusOrderByCreatedDesc(StatusRecord.ACTIVE, pageable));
        }

        return "studentReceivables/list";
    }

    @PostMapping("/studentReceivables/save")
    public String savaStudentReceivables(@Valid StudentReceivablesDto studentReceivablesDto, Authentication authentication, RedirectAttributes attributes) {
        User user = currentUserService.currentUser(authentication);
        StudentReceivables studentReceivables = new StudentReceivables();
        BigDecimal amountOfDebt = formatValue(studentReceivablesDto.getAmountOfDebt());
        BigDecimal remainingDebt = formatValue(studentReceivablesDto.getRemainingDebt());
        BeanUtils.copyProperties(studentReceivablesDto, studentReceivables);
        studentReceivables.setAmountOfDebt(amountOfDebt);
        studentReceivables.setRemainingDebt(remainingDebt);
        studentReceivables.setUser(user.getUsername());
        studentReceivables.setCreated(LocalDateTime.now());
        studentReceivablesDao.save(studentReceivables);
        attributes.addFlashAttribute("success", "Berhasil Disimpan");
        return "redirect:/studentReceivables/list?nim=" + studentReceivables.getNim();
    }

    @GetMapping("/studentReceivables/delete")
    public String deleteStudentReceivables(@RequestParam StudentReceivables id, RedirectAttributes attributes) {
        id.setStatus(StatusRecord.DELETED);
        studentReceivablesDao.save(id);
        attributes.addFlashAttribute("success", "Berhasil Dihapus");
        return "redirect:/studentReceivables/list?nim=" + id.getNim();
    }

    public BigDecimal formatValue(String value) {
        String formattedBalance = value;

        formattedBalance = formattedBalance.replaceAll("\\.", "");

        formattedBalance = formattedBalance.replace(",", ".");

        BigDecimal balance = new BigDecimal(formattedBalance);
        return balance;
    }
}
