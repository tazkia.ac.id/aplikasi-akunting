package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.InstitutDao;
import id.ac.tazkia.accounting.dao.YayasanDao;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.Yayasan;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class InstitutController {
    @Autowired
    private InstitutDao institutDao;

    @Autowired
    private YayasanDao yayasanDao;

    @ModelAttribute("yayasan")
    public List<Yayasan> getYayasan(){
        List<Yayasan> yayasans = yayasanDao.findByStatus(StatusRecord.ACTIVE);
        return yayasans;
    }

    @GetMapping("/institut")
    public String institut(Model model) {
        model.addAttribute("setting", "active");
        model.addAttribute("institut", "active");
        model.addAttribute("listInstitut", institutDao.findByStatus(StatusRecord.ACTIVE));
        return "institut/list";
    }

    @PostMapping("/institut")
    public String saveInstitut(@Valid Institut institut){
        institutDao.save(institut);
        return "redirect:/institut";
    }

    @GetMapping("/institut/hapus/{institut}")
    public String deleteInstitut(@PathVariable Institut institut){
        institutDao.delete(institut);
        return "redirect:/institut";
    }
}
