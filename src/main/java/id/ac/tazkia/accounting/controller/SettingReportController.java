package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.entity.ReportSetting;
import id.ac.tazkia.accounting.entity.ReportSettingDetail;
import id.ac.tazkia.accounting.service.AccountService;
import id.ac.tazkia.accounting.service.ClassificationService;
import id.ac.tazkia.accounting.service.ReportService;
import id.ac.tazkia.accounting.service.ReportSettingService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller@Slf4j
public class SettingReportController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private ReportSettingService reportSettingService;

    @Autowired
    private ClassificationService classificationService;

    @GetMapping("/settingreport")
    public String settingReport(Model model) {
        model.addAttribute("reportnav", "active");
        model.addAttribute("setting", "active");
        model.addAttribute("listdata", reportSettingService.getAllReportSettings());
        return "settingreport/list";
    }

    @GetMapping("/settingreport/detail/{report}")
    public String settingReportDetail(Model model, @PathVariable ReportSetting report) {
        model.addAttribute("reportnav", "active");
        model.addAttribute("setting", "active");

        model.addAttribute("account", accountService.getAllAccount());
        model.addAttribute("report", report);
        model.addAttribute("count", reportSettingService.getCountReportSettingDetail(report));
        model.addAttribute("listdata", reportSettingService.getReportSettingDetailById(report));
        model.addAttribute("subclassification", classificationService.getSubclassificationForSelect());
        return "settingreport/detail";
    }

    @PostMapping("/settingreport")
    public String saveSettingReport(@Valid ReportSetting reportSetting) {
        try{
            reportSettingService.saveReportSetting(reportSetting);
        }catch (Exception e){
            log.error("Error : " + e.getMessage());
        }
        return "redirect:/settingreport";
    }

    @PostMapping("/settingreportdetail")
    public String saveSettingReportDetail(String report,String[] account, RedirectAttributes attributes) {
        try {
            String id = reportSettingService.saveReportSettingDetail(report, account);
            attributes.addFlashAttribute("message", "Report Setting Detail Saved");
        } catch (Exception e) {
            log.error("Error : " + e.getMessage());
        }
        return "redirect:/settingreport/detail/" + report;
    }

    @GetMapping("/settingreportdetail/delete")
    public String deleteSettingReportDetail(@RequestParam ReportSettingDetail id) {
        reportSettingService.deleteReportSettingDetail(id);
        System.out.println("test : " + id);
        return "redirect:/settingreport/detail/" + id.getReportSetting().getId();
    }

    @GetMapping("/settingreport/delete")
    public String deleteSetting(@RequestParam ReportSetting id){
        reportSettingService.deleteReportSetting(id);
        return "redirect:/settingreport";
    }

}
