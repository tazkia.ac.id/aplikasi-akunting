package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dto.LedgerDto;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.BalanceType;
import id.ac.tazkia.accounting.service.AccountService;
import id.ac.tazkia.accounting.service.JournalDetailService;
import id.ac.tazkia.accounting.service.JournalService;
import id.ac.tazkia.accounting.service.ReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class BalanceSheetController {
    @Autowired
    private JournalService journalService;

    @Autowired
    private JournalDetailService journalDetailService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ReportService reportService;

    @GetMapping("/journal/balanceSheet")
    public String getJournalBalanceSheet(Model model, @RequestParam(required = false) LocalDate startDate,
                                         @RequestParam(required = false) LocalDate endDate){
        model.addAttribute("balanceSheetSidebar", "active");
        model.addAttribute("monthList", journalService.getMonth());
        model.addAttribute("yearList", journalService.getYear());

        List<Account> accountList = accountService.getAllAccount();

        int currentYear = LocalDate.now().getYear();

        // Membuat objek LocalDate untuk 1 Januari tahun ini
        LocalDate firstJanuary = LocalDate.of(currentYear, 1, 1);
        LocalDate dateNow = LocalDate.now();
        ArrayList<LedgerDto> ledgerDtoArrayList = new ArrayList<>();
        if (startDate == null && endDate == null) {
            model.addAttribute("startDate", firstJanuary);
            model.addAttribute("endDate", dateNow);
            ledgerDtoArrayList = getLegder(accountList, firstJanuary, dateNow);
        }else {
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            ledgerDtoArrayList = getLegder(accountList, startDate, endDate);
        }

        model.addAttribute("ledgerDtoArrayList", ledgerDtoArrayList);

        return "journal/balanceSheet";
    }

    private ArrayList<LedgerDto> getLegder(List<Account> accountList, LocalDate startDate, LocalDate endDate){
        ArrayList<LedgerDto> ledgerDtoArrayList = new ArrayList<>();
        for (Account account : accountList) {
            BigDecimal debet = journalDetailService.getSumAmountByAccountDebet(startDate, endDate, account.getId());
            BigDecimal credit = journalDetailService.getSumAmountByAccountCredit(startDate, endDate, account.getId());
            BigDecimal totalResult = BigDecimal.ZERO;
            LedgerDto ledgerDto = new LedgerDto();

            if (debet == null) {
                debet = BigDecimal.ZERO;
            }
            if (credit == null) {
                credit = BigDecimal.ZERO;
            }

            totalResult = debet.subtract(credit);

            ledgerDto.setAccount(account.getCode() + " - " + account.getName());
            ledgerDto.setDebet(debet);
            ledgerDto.setCredit(credit);

            if (totalResult.compareTo(BigDecimal.ZERO) > 0) {
//                jika jumlahnya positif maka akan masuk ke debet
                ledgerDto.setBalanceType(BalanceType.DEBET);
                ledgerDto.setTotal(totalResult);
            } else if (totalResult.compareTo(BigDecimal.ZERO) < 0) {
//                jika hasilnya negatif maka akan masuk ke credit
                ledgerDto.setBalanceType(BalanceType.CREDIT);
                ledgerDto.setTotal(totalResult.abs());
            }else {
                ledgerDto.setBalanceType(BalanceType.DEBET);
            }

//            System.out.println(ledgerDto);
            ledgerDtoArrayList.add(ledgerDto);
        }
        return ledgerDtoArrayList;
    }

}
