package id.ac.tazkia.accounting.controller.api;


import id.ac.tazkia.accounting.dto.BaseResponseApiDto;
import id.ac.tazkia.accounting.dto.ResponseCodeAccountDto;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountType;
import id.ac.tazkia.accounting.entity.JournalTemplateDetail;
import id.ac.tazkia.accounting.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController @Slf4j
public class AccountApiController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/api/account/check")
    @ResponseBody
    public ResponseCodeAccountDto checkCode(@RequestParam String code){
        Account checkCode = accountService.checkCodeAccount(code);
        if (checkCode != null){
            ResponseCodeAccountDto responseCodeAccountDto = ResponseCodeAccountDto.builder()
                    .code(HttpStatus.FORBIDDEN).message("Telah Digunakan").build();
            return responseCodeAccountDto;
        }else {
            ResponseCodeAccountDto responseCodeAccountDto = ResponseCodeAccountDto.builder()
                    .code(HttpStatus.NOT_FOUND).message("Belum Digunakan").build();
            return responseCodeAccountDto;
        }
    }

    @GetMapping("/api/account/check-template")
    @ResponseBody
    public ResponseCodeAccountDto checkCodeTemplate(@RequestParam String id, @RequestParam String code){
        JournalTemplateDetail checked = accountService.checkCodeTemplate(id, code);
        if (checked != null){
            ResponseCodeAccountDto responseCodeAccountDto = ResponseCodeAccountDto.builder()
                    .code(HttpStatus.FORBIDDEN).message("Telah Digunakan").build();
            return responseCodeAccountDto;
        }else {
            ResponseCodeAccountDto responseCodeAccountDto = ResponseCodeAccountDto.builder()
                    .code(HttpStatus.NOT_FOUND).message("Belum Digunakan").build();
            return responseCodeAccountDto;
        }
    }

    @GetMapping("/public/api/account/list")
    @ResponseBody
    public ResponseEntity<BaseResponseApiDto> getAccountList(@RequestParam(required = false) AccountType sub){
        try{
            if (sub == null){
                return ResponseEntity.ok().body(
                        BaseResponseApiDto.builder()
                                .data(accountService.getAllAccount())
                                .build()
                );
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseApiDto.builder()
                                .data(accountService.getAccountByType(sub))
                                .build()
                );
            }



        }catch (Exception e){
            log.error("[GET Journal Template] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/api/code-account")
    @ResponseBody
    public ResponseEntity<BaseResponseApiDto> getCodeAccount(@RequestParam String id){
        try{
            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .data(accountService.getCodeAccount(id))
                            .build()
            );
        }catch (Exception e){
            log.error("[GET Journal Template] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }
}
