package id.ac.tazkia.accounting.controller;

import ch.qos.logback.core.model.Model;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.service.AccountService;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class ReportAccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/download/account")
    public void downloadAccount(HttpServletResponse response) throws IOException {
        String[] column = {"No", "ID Account", "Code Account", "Name Account", "Subclassification", "Classification", "Type Account", "Balance Type"};
        List<Account> account = accountService.getAllAccount();
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data-Account");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < column.length; i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(column[i]);
            cell.setCellStyle(headerCellStyle);
        }


        int rowNum = 1;
        int baris = 1;

        for (Account data: account){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(data.getId());
            row.createCell(2).setCellValue(data.getCode());
            row.createCell(3).setCellValue(data.getName());
            row.createCell(4).setCellValue(data.getSubclassification().getName());
            row.createCell(5).setCellValue(data.getSubclassification().getClassification().getName());
            row.createCell(6).setCellValue(String.valueOf(data.getAccountType()));
            row.createCell(7).setCellValue(String.valueOf(data.getNominalBalance()));
        }

        for (int i = 0; i < column.length; i++){
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=Data-Account-"+ LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MMMM-YYYY"))+".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }
}
