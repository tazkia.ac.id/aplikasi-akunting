package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.AccountDao;
import id.ac.tazkia.accounting.dao.JournalDao;
import id.ac.tazkia.accounting.dao.JournalDetailDao;
import id.ac.tazkia.accounting.dto.*;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.Journal;
import id.ac.tazkia.accounting.entity.JournalDetail;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.service.JournalDetailService;
import id.ac.tazkia.accounting.service.JournalService;
import id.ac.tazkia.accounting.service.ReportService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ExcelJournalController {
    @Autowired
    private JournalDao journalDao;

    @Autowired
    private JournalDetailDao journalDetailDao;

    @Autowired
    private JournalDetailService journalDetailService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private AccountDao accountDao;

    @GetMapping("/download/laporan")
    public void downloadLaporan(HttpServletResponse response,
                                @RequestParam LocalDate startDate,
                                @RequestParam LocalDate endDate,
                                @RequestParam String[] institut) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Laporan Neraca Saldo");

        // Membuat Style untuk Header
        CellStyle headerStyle = workbook.createCellStyle();
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);
        headerStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // Membuat Header
        Row headerRow = sheet.createRow(0);
        String[] headers = {"Code", "Name", "Account Type", "Nominal Balance",
                            "Saldo Awal Debet", "Saldo Awal Kredit",
                            "Mutasi Debet", "Mutasi Kredit",
                            "Ns Debet", "Ns Kredit",
                            "Penyesuaian Debet", "Penyesuaian Kredit",
                            "Sp Debet", "Sp Kredit",
                            "Lr Debet", "Lr Kredit",
                            "Neraca Debet", "Neraca Kredit"};

        for (int i = 0; i < headers.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers[i]);
            cell.setCellStyle(headerStyle);
        }

        // Mengambil data dari service
        List<ReportInterfaceTerbaru> reportList = reportService.laporanNeracaSaldo(startDate, endDate, institut);

        // Mengisi Data ke Excel
        int rowNum = 1;
        for (ReportInterfaceTerbaru data : reportList) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(data.getCode());
            row.createCell(1).setCellValue(data.getName());
            row.createCell(2).setCellValue(data.getAccountType());
            row.createCell(3).setCellValue(data.getNominalBalance());
            row.createCell(4).setCellValue(data.getSaldoAwalDebet() != null ? data.getSaldoAwalDebet().doubleValue() : 0);
            row.createCell(5).setCellValue(data.getSaldoAwalKredit() != null ? data.getSaldoAwalKredit().doubleValue() : 0);
            row.createCell(6).setCellValue(data.getMutasiDebet() != null ? data.getMutasiDebet().doubleValue() : 0);
            row.createCell(7).setCellValue(data.getMutasiKredit() != null ? data.getMutasiKredit().doubleValue() : 0);
            row.createCell(8).setCellValue(data.getNeracaSaldoDebet() != null ? data.getNeracaSaldoDebet().doubleValue() : 0);
            row.createCell(9).setCellValue(data.getNeracaSaldoKredit() != null ? data.getNeracaSaldoKredit().doubleValue() : 0);
            row.createCell(10).setCellValue(data.getPenyesuaianDebet() != null ? data.getPenyesuaianDebet().doubleValue() : 0);
            row.createCell(11).setCellValue(data.getPenyesuaianKredit() != null ? data.getPenyesuaianKredit().doubleValue() : 0);
            row.createCell(12).setCellValue(data.getSetelahPenyesuaianDebet() != null ? data.getSetelahPenyesuaianDebet().doubleValue() : 0);
            row.createCell(13).setCellValue(data.getSetelahPenyesuaianKredir() != null ? data.getSetelahPenyesuaianKredir().doubleValue() : 0);
            row.createCell(14).setCellValue(data.getLabaRugidebet() != null ? data.getLabaRugidebet().doubleValue() : 0);
            row.createCell(15).setCellValue(data.getLabaRugikredit() != null ? data.getLabaRugikredit().doubleValue() : 0);
            row.createCell(16).setCellValue(data.getNeracaDebet() != null ? data.getNeracaDebet().doubleValue() : 0);
            row.createCell(17).setCellValue(data.getNeracaKredit() != null ? data.getNeracaKredit().doubleValue() : 0);
        }

        // Auto-size kolom agar lebih rapi
        for (int i = 0; i < headers.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Konfigurasi response untuk download file
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=Laporan_Neraca_Saldo.xlsx");

        workbook.write(response.getOutputStream());
        workbook.close();
    }

    

    @GetMapping("/download/journal")
    public void downloadJournalExcel(HttpServletResponse response, @RequestParam LocalDate startDate, @RequestParam LocalDate endDate) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data-Journal");

        // Header Style
        CellStyle headerStyle = workbook.createCellStyle();
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);
        headerStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // Create Header Row
        Row headerRow = sheet.createRow(0);
        String[] headers = {"#", "Deskripsi", "Tanggal", "Account", "Debet", "Credit"};
        for (int i = 0; i < headers.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers[i]);
            cell.setCellStyle(headerStyle);
        }

        List<Journal> journals = journalDao.findByTransactionDateBetweenAndStatusOrderByTransactionDate(startDate, endDate, StatusRecord.ACTIVE);
        List<JournalDetail> journalDetails = journalDetailDao.findByStatusOrderBySequenceAsc(StatusRecord.ACTIVE);

        int rowNum = 1;
        for (Journal journal : journals) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(rowNum - 1);
            row.createCell(1).setCellValue(journal.getDescription());
            row.createCell(2).setCellValue(journal.getTransactionDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));

            for (JournalDetail detail : journalDetails) {
                if (detail.getJournal().getId().equals(journal.getId())) {
                    Row detailRow = sheet.createRow(rowNum++);
                    detailRow.createCell(3).setCellValue(detail.getAccount().getCode() + " - " + detail.getAccount().getName());
                    if ("DEBET".equals(detail.getBalanceType().toString())) {
                        detailRow.createCell(4).setCellValue(detail.getAmount().doubleValue());
                    }
                    if ("CREDIT".equals(detail.getBalanceType().toString())) {
                        detailRow.createCell(5).setCellValue(detail.getAmount().doubleValue());
                    }
                }
            }
        }

        // Auto size columns
        for (int i = 0; i < headers.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Set the response header
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=Data-Journal_start-date=" + startDate + "-" + "endDate=" + endDate + ".xlsx");

        // Write the workbook to the output stream
        try (OutputStream os = response.getOutputStream()) {
            workbook.write(os);
        }
        workbook.close();
    }

    @GetMapping("/download/woorksheet")
    public void downloadWoorksheet(HttpServletResponse response, @RequestParam Account account,
                                   @RequestParam LocalDate startDate, @RequestParam LocalDate endDate) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data-Woorksheet");

        // Membuat header style
        CellStyle headerStyle = workbook.createCellStyle();
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);
        headerStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);

        // Membuat header row
        Row headerRow = sheet.createRow(0);
        String[] headers = {"Date", "Description", "Account", "Debet", "Credit"};
        for (int i = 0; i < headers.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers[i]);
            cell.setCellStyle(headerStyle);
        }

        // Mengisi data dari journalDetailByAccountDtos
        List<JournalDetailByAccountDto> journalDetailByAccountDtos = journalDetailService.getJournalDetailByAccount(
                startDate, endDate, account.getId());

        BigDecimal debet = journalDetailService.getSumAmountByAccountDebet(
                startDate, endDate, account.getId());
        BigDecimal credit = journalDetailService.getSumAmountByAccountCredit(
                startDate, endDate, account.getId());

        int rowNum = 1;
        for (JournalDetailByAccountDto data : journalDetailByAccountDtos) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(data.getDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
            row.createCell(1).setCellValue(data.getDescription());
            row.createCell(2).setCellValue(data.getCode() + " - " + data.getName());

            if ("DEBET".equals(data.getBalance())) {
                row.createCell(3).setCellValue(data.getAmount().setScale(2, RoundingMode.HALF_UP).toString());
            }

            if ("CREDIT".equals(data.getBalance())) {
                row.createCell(4).setCellValue(data.getAmount().setScale(2, RoundingMode.HALF_UP).toString());
            }
        }

        // Mengisi total debet dan credit di bagian footer
        Row footerRow = sheet.createRow(rowNum);
        footerRow.createCell(0).setCellValue("Total");
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 2));

        // Menangani nilai null pada total debet
        footerRow.createCell(3).setCellValue(
                debet != null ? debet.setScale(2, RoundingMode.HALF_UP).toString() : "0.00"
        );

// Menangani nilai null pada total credit
        footerRow.createCell(4).setCellValue(
                credit != null ? credit.setScale(2, RoundingMode.HALF_UP).toString() : "0.00"
        );

        // Auto size column
        for (int i = 0; i < headers.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Set response header
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=Data-Woorksheet-" + account.getName() + "_" + LocalDate.now() + ".xlsx");

        // Menulis workbook ke output stream
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/download/reviewJournal")
    public void  downloadReviewJournal(HttpServletResponse response, @RequestParam LocalDate startDate, @RequestParam LocalDate endDate)throws IOException{
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data-Journal");

        // Header Style
        CellStyle headerStyle = workbook.createCellStyle();
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);
        headerStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // Create Header Row
        Row headerRow = sheet.createRow(0);
        String[] headers = {"#", "Deskripsi", "Tanggal", "Account", "Debet", "Credit"};
        for (int i = 0; i < headers.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers[i]);
            cell.setCellStyle(headerStyle);
        }
        List<Journal> journals = journalDao.findByTransactionDateBetweenAndStatusOrderByTransactionDate(startDate, endDate, StatusRecord.REVIEW);
        List<JournalDetail> journalDetails = journalDetailDao.findByStatusOrderBySequenceAsc(StatusRecord.REVIEW);

        int rowNum = 1;
        for (Journal journal : journals) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(rowNum - 1);
            row.createCell(1).setCellValue(journal.getDescription());
            row.createCell(2).setCellValue(journal.getTransactionDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));

            for (JournalDetail detail : journalDetails) {
                if (detail.getJournal().getId().equals(journal.getId())) {
                    Row detailRow = sheet.createRow(rowNum++);
                    detailRow.createCell(3).setCellValue(detail.getAccount().getCode() + " - " +
                            detail.getAccount().getName() + " - " + detail.getJournal().getDescription() + " - " +
                            detail.getJournal().getTransactionDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
                    if ("DEBET".equals(detail.getBalanceType().toString())) {
                        detailRow.createCell(4).setCellValue(detail.getAmount().doubleValue());
                    }
                    if ("CREDIT".equals(detail.getBalanceType().toString())) {
                        detailRow.createCell(5).setCellValue(detail.getAmount().doubleValue());
                    }
                }
            }
        }

        // Auto size columns
        for (int i = 0; i < headers.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Set the response header
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=Data-Review=" + startDate + ":" + endDate +  ".xlsx");

        // Write the workbook to the output stream
        try (OutputStream os = response.getOutputStream()) {
            workbook.write(os);
        }
        workbook.close();

    }

    @GetMapping("/download-worksheet")
    public void downloadWoorksheet(HttpServletResponse response, @RequestParam(required = false) String tahun) throws IOException {
        // Validasi: Tahun tidak boleh kosong
        if (tahun == null || tahun.trim().isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Tahun harus diisi.");
            return;
        }

        // Validasi: Tahun harus berupa angka 4 digit
        if (!tahun.matches("\\d{4}")) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Format tahun tidak valid.");
            return;
        }

        List<WoorkSheetAllDto> woorkSheetAll = reportService.getWoorksheetAll(tahun);

        // Validasi: Jika tidak ada data, beri respons error
        if (woorkSheetAll == null || woorkSheetAll.isEmpty()) {
            response.sendError(HttpServletResponse.SC_NO_CONTENT, "Tidak ada data untuk tahun " + tahun);
            return;
        }

        List<Account> accountList = accountDao.findByStatusOrderByCode(StatusRecord.ACTIVE);
        Map<String, WorksheetAllDataDto> groupedData = new LinkedHashMap<>();

        for (Account account : accountList) {
            groupedData.put(account.getCode(), new WorksheetAllDataDto(account.getName(), new ArrayList<>()));
        }

        // Mengelompokkan data transaksi berdasarkan kode akun
        for (WoorkSheetAllDto item : woorkSheetAll) {
            groupedData.getOrDefault(item.getCode(), new WorksheetAllDataDto("", new ArrayList<>()))
                    .getWoorkSheetList().add(item);
        }

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=worksheet_all.xlsx");

        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Worksheet All");
            int rowIdx = 0;

            for (Map.Entry<String, WorksheetAllDataDto> entry : groupedData.entrySet()) {
                String namaAkun = entry.getValue().getAccountName();
                List<WoorkSheetAllDto> transactions = entry.getValue().getWoorkSheetList();

                // Tulis Nama Akun sebagai Header
                Row akunRow = sheet.createRow(rowIdx++);
                Cell akunCell = akunRow.createCell(0);
                akunCell.setCellValue("Nama Akun: " + namaAkun);
                CellStyle boldStyle = workbook.createCellStyle();
                Font boldFont = workbook.createFont();
                boldFont.setBold(true);
                boldStyle.setFont(boldFont);
                akunCell.setCellStyle(boldStyle);

                // Tulis Header Transaksi
                Row headerRow = sheet.createRow(rowIdx++);
                String[] columns = {"Tanggal", "Keterangan", "Debet", "Kredit", "Saldo"};
                for (int i = 0; i < columns.length; i++) {
                    Cell cell = headerRow.createCell(i);
                    cell.setCellValue(columns[i]);
                    cell.setCellStyle(boldStyle);
                }

                // Tulis Data Transaksi
                for (WoorkSheetAllDto transaction : transactions) {
                    Row row = sheet.createRow(rowIdx++);
                    row.createCell(0).setCellValue(transaction.getTanggal() + " " + transaction.getBulan());
                    row.createCell(1).setCellValue(transaction.getDesk());
                    row.createCell(2).setCellValue(transaction.getDebet().toString());
                    row.createCell(3).setCellValue(transaction.getCredit().toString());
                    row.createCell(4).setCellValue(transaction.getSaldoAkhir().toString());
                }

                // Tambahkan baris kosong untuk pemisah antar akun
                rowIdx++;
            }

            // Auto-size kolom
            for (int i = 0; i < 5; i++) {
                sheet.autoSizeColumn(i);
            }

            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.close();
        }
    }


    @GetMapping("/neraca/export-excel")
    public void exportNeracaToExcel(@RequestParam LocalDate startDate,
                                    @RequestParam LocalDate endDate,
                                    @RequestParam(required = false) String[] institut,
                                    HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=neraca.xlsx");

        List<ReportInterfaceTerbaru> listData = reportService.laporanNeraca(startDate, endDate, institut);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Neraca");

        // Style untuk header (tebal)
        CellStyle headerStyle = workbook.createCellStyle();
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);

        // Style untuk akun utama (tebal)
        CellStyle boldStyle = workbook.createCellStyle();
        Font boldFont = workbook.createFont();
        boldFont.setBold(true);
        boldStyle.setFont(boldFont);

        // Header baris pertama
        Row headerRow = sheet.createRow(0);
        String[] columns = {"Kode", "Nama Akun", "Saldo"};
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerStyle);
        }

        // Isi data
        int rowNum = 1;
        for (ReportInterfaceTerbaru data : listData) {
            Row row = sheet.createRow(rowNum++);

            // Menghitung indentasi berdasarkan jumlah titik dalam kode akun
            int indent = data.getCode().length() - data.getCode().replace(".", "").length();

            // Kolom Kode & Nama
            Cell cellCode = row.createCell(0);
            cellCode.setCellValue(data.getCode());

            Cell cellName = row.createCell(1);
            cellName.setCellValue(data.getName());

            // Indentasi Nama Akun (Geser ke kanan)
            CellStyle indentStyle = workbook.createCellStyle();
            indentStyle.setIndention((short) indent);
            cellName.setCellStyle(indentStyle);

            // Jika akun utama (6 karakter) → Tebal
            if (data.getCode().length() == 6) {
                cellName.setCellStyle(boldStyle);
            }

            // Kolom Saldo
            Cell cellSaldo = row.createCell(2);
            double saldo = ("AKTIVA".equals(data.getAccountType())) ?
                    data.getNeracaDebet().doubleValue() :
                    data.getNeracaKredit().doubleValue();
            cellSaldo.setCellValue(saldo);

            // Format angka dengan koma sebagai pemisah ribuan
            CellStyle numberStyle = workbook.createCellStyle();
            DataFormat format = workbook.createDataFormat();
            numberStyle.setDataFormat(format.getFormat("#,##0.00"));
            cellSaldo.setCellStyle(numberStyle);
        }

        // Autosize kolom agar terlihat rapi
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Simpan file Excel ke response
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }


    @GetMapping("/export-labarugi")
    public void exportLabaRugiToExcel(@RequestParam LocalDate startDate,
                                      @RequestParam LocalDate endDate,
                                      @RequestParam(required = false) String[] institut,
                                      HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=labarugi.xlsx");

        List<ReportInterfaceTerbaru> listData = reportService.laporanLabaRugi(startDate, endDate, institut);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Laba Rugi");

        // Style Header
        CellStyle headerStyle = workbook.createCellStyle();
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);

        // Style Tebal untuk akun utama
        CellStyle boldStyle = workbook.createCellStyle();
        Font boldFont = workbook.createFont();
        boldFont.setBold(true);
        boldStyle.setFont(boldFont);

        // Header
        Row headerRow = sheet.createRow(0);
        String[] columns = {"Kode", "Nama Akun", "Nominal"};
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerStyle);
        }

        // Isi Data
        int rowNum = 1;
        for (ReportInterfaceTerbaru data : listData) {
            Row row = sheet.createRow(rowNum++);

            // Menghitung indentasi berdasarkan jumlah titik dalam kode akun
            int indent = data.getCode().length() - data.getCode().replace(".", "").length();

            // Kolom Kode & Nama
            Cell cellCode = row.createCell(0);
            cellCode.setCellValue(data.getCode());

            Cell cellName = row.createCell(1);
            cellName.setCellValue(data.getName());

            // Indentasi Nama Akun
            CellStyle indentStyle = workbook.createCellStyle();
            indentStyle.setIndention((short) indent);
            cellName.setCellStyle(indentStyle);

            // Jika akun utama (6 karakter) → Tebal
            if (data.getCode().length() == 6) {
                cellName.setCellStyle(boldStyle);
            }

            // Kolom Nominal
            Cell cellNominal = row.createCell(2);
            double nominal = ("REVENUE".equals(data.getAccountType())) ?
                    data.getLabaRugikredit().doubleValue() :
                    data.getLabaRugidebet().doubleValue();
            cellNominal.setCellValue(nominal);

            // Format angka dengan koma sebagai ribuan
            CellStyle numberStyle = workbook.createCellStyle();
            DataFormat format = workbook.createDataFormat();
            numberStyle.setDataFormat(format.getFormat("#,##0.00"));
            cellNominal.setCellStyle(numberStyle);
        }

        // Autosize kolom agar terlihat rapi
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Simpan file Excel ke response
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }

}
