package id.ac.tazkia.accounting.controller.api;

import id.ac.tazkia.accounting.dto.BaseResponseApiDto;
import id.ac.tazkia.accounting.dto.CreateJournalByApiArrayDto;
import id.ac.tazkia.accounting.dto.CreateJournalByApiDto;
import id.ac.tazkia.accounting.dto.CreateJournalDto;
import id.ac.tazkia.accounting.service.JournalDetailService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class ApiJournalController {
    @Autowired
    private JournalDetailService journalDetailService;

    @PostMapping(value = "/public/api/journal", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponseApiDto> saveJournarByApi(@Valid @RequestBody CreateJournalByApiDto createJournalByApiDto,
                                                               BindingResult bindingResult) {
        try{
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseApiDto("ERR01", "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }
            CreateJournalByApiDto saveJournalDetail = journalDetailService.saveJournalByApi(createJournalByApiDto);
            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .responseMessage(saveJournalDetail.getMessageJournal())
                            .data(saveJournalDetail)
                            .build());

        }catch (Exception e){
            log.error("[GET Journal Template] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }

    }

    @PostMapping(value = "/public/api/journal-array", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponseApiDto> savaJournalByApiArray(@Valid @RequestBody CreateJournalByApiArrayDto createJournalByApiArrayDto,
                                                                    BindingResult bindingResult){
        try{
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseApiDto("ERR01", "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }
            CreateJournalByApiArrayDto saveJournalDetail = journalDetailService.createJournalByApiArrayDto(createJournalByApiArrayDto);
            return ResponseEntity.ok().body(
                    BaseResponseApiDto.builder()
                            .responseCode(saveJournalDetail.getResponseCode())
                            .responseMessage(saveJournalDetail.getMessageJournal())
                            .data(saveJournalDetail)
                            .build());

        }catch (Exception e){
            log.error("[GET Journal Template] - [ERROR] : Unable to get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseApiDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }
}
