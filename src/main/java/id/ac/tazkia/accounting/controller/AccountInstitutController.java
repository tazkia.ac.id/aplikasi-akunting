package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.AccountDao;
import id.ac.tazkia.accounting.dao.AccountInstitutDao;
import id.ac.tazkia.accounting.dao.InstitutDao;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountInstitut;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AccountInstitutController {
    @Autowired
    private InstitutDao institutDao;

    @Autowired
    private AccountInstitutDao accountInstitutDao;

    @Autowired
    private AccountDao accountDao;

    @GetMapping("/accountInstitut")
    public String accountInstitut(Model model) {
        model.addAttribute("setting", "active");
        model.addAttribute("menuAccountInstitut", "active");
        model.addAttribute("institutList", institutDao.findByStatus(StatusRecord.ACTIVE));
        return "accountInstitut/list";
    }

    @GetMapping("/accountInstitut/detail")
    public String detail(Model model, @RequestParam Institut institut) {
        model.addAttribute("setting", "active");
        model.addAttribute("menuAccountInstitut", "active");
        model.addAttribute("institut", institut);
        model.addAttribute("accountInstitut", accountInstitutDao.findByInstitutAndStatusOrderByAccountCodeAsc(institut, StatusRecord.ACTIVE));
        return "accountInstitut/detail";
    }

    @PostMapping("/accountInstitut/save")
    public String saveAccountInstitut(String institut, String account[]) {
        Institut institutGet = institutDao.findById(institut).get();

        for (int i = 0; i < account.length; i++) {
            String idaccount = account[i];
            Account getAccount = accountDao.findById(idaccount).get();

            AccountInstitut validation = accountInstitutDao.findByAccountAndInstitutAndStatus(getAccount, institutGet, StatusRecord.ACTIVE);
            if (validation == null) {
                AccountInstitut accountInstitut = new AccountInstitut();
                accountInstitut.setAccount(getAccount);
                accountInstitut.setInstitut(institutGet);
                accountInstitutDao.save(accountInstitut);
            }
        }

        return "redirect:/accountInstitut/detail?institut=" + institut;
    }

    @GetMapping("/accountInstitut/delete")
    public String deleteAccountInstitut(@RequestParam AccountInstitut id) {
        accountInstitutDao.delete(id);
        return "redirect:/accountInstitut/detail?institut=" + id.getInstitut().getId();
    }
}
