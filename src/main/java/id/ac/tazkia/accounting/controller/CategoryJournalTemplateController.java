package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.entity.JournalCategoryTemplate;
import id.ac.tazkia.accounting.service.JournalCategoryTemplateService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller @Slf4j
public class CategoryJournalTemplateController {

    @Autowired
    private JournalCategoryTemplateService journalCategoryTemplateService;
    @GetMapping("/category-journal-template")
    public String listCategoryJournalTemplate(Model model, @PageableDefault Pageable pageable){
        model.addAttribute("setting", "active");
        model.addAttribute("journalcategory", "active");
        model.addAttribute("journalCategoryList", journalCategoryTemplateService
                .getListPageJournalCategoryTemplate(pageable));
        return "journalCategoryTemplate/list";
    }

    @PostMapping("/category-journal-template/save")
    public String saveCategoryJournalTemplate(@Valid JournalCategoryTemplate journalCategoryTemplate,
                                              RedirectAttributes attributes){
        try{
            journalCategoryTemplateService.saveJournalCategoryTemplate(journalCategoryTemplate);
            attributes.addFlashAttribute("success", "Saved successfully");
            return "redirect:/category-journal-template";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error("Error : " + e);
            return "redirect:/category-journal-template";
        }
    }

    @GetMapping("/category-journal-template/delete")
    public String deleteCategoryJournalTemplate(@RequestParam String id,
                                                RedirectAttributes attributes){
        try {
            journalCategoryTemplateService.deleteJournalCategoryTemplate(id);
            attributes.addFlashAttribute("success", "Deleted successfully");
            return "redirect:/category-journal-template";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error(e.getMessage());
            return "redirect:/category-journal-template";
        }
    }

}
