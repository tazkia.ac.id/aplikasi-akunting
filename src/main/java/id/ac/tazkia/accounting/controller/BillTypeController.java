package id.ac.tazkia.accounting.controller;

import id.ac.tazkia.accounting.dao.BillTypeDao;
import id.ac.tazkia.accounting.entity.BillType;
import id.ac.tazkia.accounting.entity.StatusRecord;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BillTypeController {

    @Autowired
    private BillTypeDao billTypeDao;

    @GetMapping("/billtype")
    public String billtype(Model model) {
        model.addAttribute("billtype", "active");
        model.addAttribute("setting", "active");

        model.addAttribute("list", billTypeDao.findByStatus(StatusRecord.ACTIVE));
        return "billType/list";
    }

    @PostMapping("/billtype/save")
    public String save(@Valid BillType billType) {
        billTypeDao.save(billType);
        return "redirect:/billtype";
    }

    @GetMapping("/billtype/delete")
    public String delete(@RequestParam BillType id) {
        id.setStatus(StatusRecord.DELETED);
        billTypeDao.save(id);
        return "redirect:/billtype";
    }
}
