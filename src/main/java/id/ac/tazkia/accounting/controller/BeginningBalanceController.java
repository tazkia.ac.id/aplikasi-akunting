package id.ac.tazkia.accounting.controller;


import id.ac.tazkia.accounting.dao.AccountInstitutDao;
import id.ac.tazkia.accounting.dao.InstitutDao;
import id.ac.tazkia.accounting.dto.AccountBalanceDto;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountBalance;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.config.User;
import id.ac.tazkia.accounting.service.AccountBalanceService;
import id.ac.tazkia.accounting.service.AccountService;
import id.ac.tazkia.accounting.service.CurrentUserService;
import id.ac.tazkia.accounting.service.JournalService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

@Controller@Slf4j
public class BeginningBalanceController {
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private AccountBalanceService accountBalanceService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JournalService journalService;

    @Autowired
    private InstitutDao institutDao;

    @Autowired
    private AccountInstitutDao accountInstitutDao;


    @ModelAttribute("institut")
    public List<Institut> institut() {
        return institutDao.findByStatus(StatusRecord.ACTIVE);
    }

    @GetMapping("/beginningBalance")
    public String listBeginningBalance(Model model,
                                       @RequestParam(required = false) Integer year,
                                       @RequestParam(required = false) Institut institut,
                                       @PageableDefault Pageable pageable) {
        model.addAttribute("setting", "active");
        model.addAttribute("beginningBalance", "active");
        model.addAttribute("listYear", journalService.getYear());
        model.addAttribute("year", year);

        // Periksa kombinasi parameter
        if (year != null && institut != null) {
            // Jika `year` dan `institut` tidak null
            model.addAttribute("list", accountBalanceService.findByYearAndInstitut(year, institut, pageable));
        } else if (year != null) {
            // Jika hanya `year` yang tidak null
            model.addAttribute("list", accountBalanceService.findByYear(year, pageable));
        } else {
            // Jika keduanya null
            model.addAttribute("list", accountBalanceService.findAll(pageable));
        }

        return "beginningBalance/list";
    }


    @GetMapping("/beginningBalance/form")
    public String formBeginningBalance(Model model){
        model.addAttribute("setting", "active");
        model.addAttribute("beginningBalance", "active");
        model.addAttribute("account", accountService.getAllAccount());
        model.addAttribute("year", journalService.getYear());
        model.addAttribute("accountBalance", new AccountBalance());
        return "beginningBalance/form";
    }

    @GetMapping("/beginningBalance/edit")
    public String formBeginningBalance(Model model, @RequestParam AccountBalance id){
        model.addAttribute("setting", "active");
        model.addAttribute("beginningBalance", "active");
        model.addAttribute("accountBalance", id);
        model.addAttribute("accountInstitut", accountInstitutDao.findByAccountAndStatus(id.getAccount(), StatusRecord.ACTIVE));
        return "beginningBalance/form";
    }

    @PostMapping("/beginningBalance/save")
    public String beginningBalanceSave(@Valid AccountBalanceDto accountBalanceDto,
                                       RedirectAttributes attributes, Authentication authentication){
        try {
            String formattedBalance = accountBalanceDto.getAccountBalance();

            formattedBalance = formattedBalance.replaceAll("\\.", "");

            formattedBalance = formattedBalance.replace(",", ".");


            BigDecimal balance = new BigDecimal(formattedBalance);

            AccountBalance accountBalance = new AccountBalance();
            BeanUtils.copyProperties(accountBalanceDto, accountBalance);
            accountBalance.setAccountBalance(balance);

            User user = currentUserService.currentUser(authentication);
            Boolean validation = accountBalanceService.saveAccountBalance(accountBalance, user);
            if (validation == true){
                attributes.addFlashAttribute("success", "Saved successfully");
            }else {
                attributes.addFlashAttribute("error", "Data untuk tahun tersebut sudah tersedia.");
            }

            return "redirect:/beginningBalance";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error(e.getMessage());
            return "redirect:/beginningBalance";
        }
    }

    @GetMapping("/beginningBalance/delete")
    public String beginningBalanceDelete(RedirectAttributes attributes, Authentication authentication,@RequestParam AccountBalance accountBalance){
        try {
            User user = currentUserService.currentUser(authentication);
            accountBalanceService.deleteAccountBalance(accountBalance, user);
            attributes.addFlashAttribute("success", "Deleted successfully");
            return "redirect:/beginningBalance";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error(e.getMessage());
            return "redirect:/account";
        }
    }

}
