package id.ac.tazkia.accounting.controller;


import id.ac.tazkia.accounting.entity.Classification;
import id.ac.tazkia.accounting.entity.Subclassification;
import id.ac.tazkia.accounting.service.ClassificationService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller @Slf4j
public class ClassificationController {

    @Autowired
    private ClassificationService classificationService;

    @GetMapping("/classification")
    public String getClassification(Model model, @PageableDefault Pageable pageable){
        model.addAttribute("setting", "active");
        model.addAttribute("classification", "active");
        model.addAttribute("listClassification", classificationService.getClassification());
        model.addAttribute("listSubClassification", classificationService.getSubclassification(pageable));
        return "classification/list";
    }

    @GetMapping("/classification/form")
    public String formClassification(Model model){
        model.addAttribute("setting", "active");
        model.addAttribute("classification", "active");
        model.addAttribute("list", classificationService.getClassification());
        return "classification/form";
    }

    @PostMapping("/classification/save")
    public String saveClassification(@Valid Classification classification, RedirectAttributes attributes){
        try{
            classificationService.saveClassiification(classification);
            attributes.addFlashAttribute("success", "Saved successfully");
            return "redirect:/classification/form";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error("[Error][Classification-Save] :" + e.getMessage());
            return "redirect:/classification/form";
        }
    }

    @GetMapping("/classification/delete")
    public String deleteClassification(@RequestParam String id, RedirectAttributes attributes){
        try{
            classificationService.deleteClassification(id);
            attributes.addFlashAttribute("success", "Delete successfully");
            return "redirect:/classification/form";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error("[Error][Classification-Save] :" + e.getMessage());
            return "redirect:/classification/form";
        }
    }

    @PostMapping("/subClassification/save")
    public String saveSubClassification(@Valid Subclassification subclassification, RedirectAttributes attributes){
        try{
            classificationService.saveSubClassification(subclassification);
            attributes.addFlashAttribute("success", "Saved successfully");
            return "redirect:/classification";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error("[Error][Classification-Save] :" + e.getMessage());
            return "redirect:/classification";
        }
    }

    @GetMapping("/subClassification/delete")
    public String deleteSubClassification(@RequestParam Subclassification id, RedirectAttributes attributes){
        try{
            classificationService.deleteSubClassification(id);
            attributes.addFlashAttribute("success", "Deleted successfully");
            return "redirect:/classification";
        }catch (Exception e){
            attributes.addFlashAttribute("error", "Error : " + e.getMessage());
            log.error("[Error][Classification-Delete] :" + e.getMessage());
            return "redirect:/classification";
        }
    }
}
