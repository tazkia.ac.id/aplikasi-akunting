package id.ac.tazkia.accounting.dto;

import java.math.BigDecimal;

public interface ReportSqlDto {

    String getCode();

    String getName();

    BigDecimal getNominalBalance();

    String getAccountType();

    BigDecimal getSaDebet();

    BigDecimal getSaKredit();

    BigDecimal getMutasiDebet();

    BigDecimal getMutasiKredit();

    BigDecimal getNsDebet();

    BigDecimal getNsKredit();

    BigDecimal getPenyesuaianDebet();

    BigDecimal getPenyesuaianKredit();

    BigDecimal getSpDebet();

    BigDecimal getSpKredit();

    BigDecimal getLrDebet();

    BigDecimal getLrKredit();

    BigDecimal getNeracaDebet();

    BigDecimal getNeracaKredit();
}
