package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

@Data
public class JournalDetailEditDto {
    private LocalDate transactionDate;
    private String decription;
    private JournalDetail[] journalDetails;

    private Account[] account;

    private Journal journal;

    private Institut institut;

    private String[] amount;

    private BalanceType[] balanceType;

    private Boolean validation;
    private String message;
    private ArrayList<JournalDetailDto> journalDetailDtoList;
    private BigDecimal totalDebet;
    private BigDecimal totalCredit;
}
