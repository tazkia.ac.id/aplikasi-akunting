package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.BalanceType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class JournalDetailByApiDto {
    private BigDecimal amount;
}
