package id.ac.tazkia.accounting.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WorksheetAllDataDto {
    private String accountName;
    private List<WoorkSheetAllDto> woorkSheetList;

    public WorksheetAllDataDto(String accountName, List<WoorkSheetAllDto> woorkSheetList) {
        this.accountName = accountName;
        this.woorkSheetList = woorkSheetList;
    }
}
