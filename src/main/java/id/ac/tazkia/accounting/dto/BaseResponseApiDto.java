package id.ac.tazkia.accounting.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponseApiDto {
    private String responseCode;
    private String responseMessage;
    private Object data;

    public BaseResponseApiDto(String responseCode, String responseMessage){
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;

    }
}
