package id.ac.tazkia.accounting.dto;


import id.ac.tazkia.accounting.entity.Institut;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class CreateJournalByApiDto {
    @NotNull
    private String codeTemplate;

    @NotNull
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateTransaction;

    @NotNull
    private String description;

    @NotNull
    private String institut;

    private String attachment;

    @NotNull
    private BigDecimal amounts;

    private Boolean statusJournal;

    private BigDecimal debet;
    private BigDecimal credit;
    private String messageJournal;


    private String tags;

    private TypeJournal type;


    public enum TypeJournal{
        reverse
    }

}
