package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.Institut;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountBalanceDto {
    private String id;
    private Account account;
    private String accountBalance;
    private Integer year;
    private Institut institut;

}
