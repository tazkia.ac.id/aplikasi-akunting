package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.BalanceType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class LedgerDto {
    private String account;
    private BigDecimal debet;
    private BigDecimal credit;
    private BigDecimal total;
    private BalanceType balanceType;

}
