package id.ac.tazkia.accounting.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ReportNeracaSaldoTotalDto {

    private List<ReportNeracaSaldoDto> reportNeracaSaldoDtoList;

    private BigDecimal totalSaDebet = BigDecimal.ZERO;
    private BigDecimal totalSaKredit = BigDecimal.ZERO;
    private BigDecimal totalMutasiDebet = BigDecimal.ZERO;
    private BigDecimal totalMutasiKredit = BigDecimal.ZERO;
    private BigDecimal totalNsDebet = BigDecimal.ZERO;
    private BigDecimal totalNsKredit = BigDecimal.ZERO;

    private BigDecimal totalPenyesuaianDebet = BigDecimal.ZERO;
    private BigDecimal totalPenyesuaianKredit = BigDecimal.ZERO;
    private BigDecimal totalSpDebet = BigDecimal.ZERO;
    private BigDecimal totalSpKredit = BigDecimal.ZERO;

    private BigDecimal totalLrDebet = BigDecimal.ZERO;
    private BigDecimal totalLrKredit = BigDecimal.ZERO;
    private BigDecimal totalNeracaDebet = BigDecimal.ZERO;
    private BigDecimal totalNeracaKredit = BigDecimal.ZERO;

}
