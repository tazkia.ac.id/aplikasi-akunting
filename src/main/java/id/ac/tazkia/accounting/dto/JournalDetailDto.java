package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.BalanceType;
import id.ac.tazkia.accounting.entity.Journal;
import id.ac.tazkia.accounting.entity.StatusRecord;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class JournalDetailDto {
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_journal", nullable = false)
    private Journal journal;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_account", nullable = false)
    private Account account;

    @NotNull
    @Enumerated(EnumType.STRING)
    private BalanceType balanceType;

    @NotNull @Min(0)
    private BigDecimal amount;

    @NotNull
    private Integer sequence;

    private StatusRecord status;
}
