package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.Journal;
import id.ac.tazkia.accounting.entity.JournalTemplate;
import id.ac.tazkia.accounting.entity.JournalType;
import id.ac.tazkia.accounting.entity.config.User;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

@Data
public class CreateJournalDto {
    private JournalTemplate journalTemplate;

        @NotNull
        @Column(columnDefinition = "DATE")
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private LocalDate dateTransaction;

    private String description;

    private Journal journal;

    private Institut institut;

    private Boolean statusJournal;

    private ArrayList<JournalDetailDto> journalDetailDtoList;

    private BigDecimal debetSatu;
    private BigDecimal creditSatu;
    private String messageJournal;

    private JournalType journalType;

    private ArrayList<String> amount;

    private User createdBy;
}
