package id.ac.tazkia.accounting.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface JournalDetailByAccountDto {
    String getId();
    String getIdAccount();

    LocalDate getDate();

    String getDescription();

    String getCode();

    String getName();

    BigDecimal getAmount();


    String getBalance();
}
