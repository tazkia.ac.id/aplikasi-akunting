package id.ac.tazkia.accounting.dto;

import lombok.Data;

import java.util.List;

@Data
public class GetJournalTemplateById {
    private Integer sequence;
    private String idAccount;
    private String account;
    private String balanceType;
}
