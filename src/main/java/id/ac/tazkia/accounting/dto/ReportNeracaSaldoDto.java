package id.ac.tazkia.accounting.dto;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReportNeracaSaldoDto {
    private String code;
    private String name;
    private String nominalBalance;
    private String accountType;

    private BigDecimal saDebet;           // Saldo Awal Debet
    private BigDecimal saKredit;          // Saldo Awal Kredit
    private BigDecimal mutasiDebet;       // Mutasi Debet
    private BigDecimal mutasiKredit;      // Mutasi Kredit
    private BigDecimal nsDebet;           // Nilai Setelah (NS) Debet
    private BigDecimal nsKredit;          // Nilai Setelah (NS) Kredit
    private BigDecimal penyesuaianDebet;  // Penyesuaian Debet
    private BigDecimal penyesuaianKredit; // Penyesuaian Kredit
    private BigDecimal spDebet;           // Saldo Penutup Debet
    private BigDecimal spKredit;          // Saldo Penutup Kredit

    private BigDecimal lrDebet;           // Laba Rugi Debet
    private BigDecimal lrKredit;          // Laba Rugi Kredit
    private BigDecimal neracaDebet;       // Neraca Debet
    private BigDecimal neracaKredit;      // Neraca Kredit
}
