package id.ac.tazkia.accounting.dto;


import id.ac.tazkia.accounting.entity.JournalTemplate;
import id.ac.tazkia.accounting.entity.JournalTemplateDetail;
import lombok.Data;

import java.util.List;

@Data
public class GetJournalTemplate {
    private String id;
    private String code;
    private String name;
    private String category;
}
