package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.Institut;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ParamsJournalDto {
    private LocalDate startDate;
    private LocalDate endDate;
    private Institut institut;
    private String search;
    private String app;
}
