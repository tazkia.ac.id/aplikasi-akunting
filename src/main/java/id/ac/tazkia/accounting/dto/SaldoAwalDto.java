package id.ac.tazkia.accounting.dto;

import java.math.BigDecimal;

public interface SaldoAwalDto {
    String getIdAccount();
    String getCode();
    String getName();
    String getAccountType();
    String getNominalBalance();
    BigDecimal getSaldoAwal();
}
