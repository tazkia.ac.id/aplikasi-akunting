package id.ac.tazkia.accounting.dto;

import lombok.Data;


public interface ValidationTemplateDto {
    String getId();
    String getCode();

    Integer  getDetail();

    Integer getDebet();

    Integer getCredit();
}
