package id.ac.tazkia.accounting.dto;

import java.math.BigDecimal;
import java.util.Date;

public interface WoorkSheetAllDto {
    String getCode();
    String getName();
    BigDecimal getSaldoAwal();
    String getDesk();
    String getBulan();

    String getTanggal();

    BigDecimal getDebet();

    BigDecimal getCredit();

    BigDecimal getSaldoAkhir();
}
