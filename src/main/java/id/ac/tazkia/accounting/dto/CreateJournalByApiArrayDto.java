package id.ac.tazkia.accounting.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

@Data
public class CreateJournalByApiArrayDto {
    @NotNull
    private String codeTemplate;

    @NotNull
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateTransaction;

    @NotNull
    private String description;

    @NotNull
    private String institut;

    private String attachment;

    @NotNull
    private ArrayList<BigDecimal> amounts;

    private BigDecimal total;

    private Boolean statusJournal;

    private BigDecimal debet;
    private BigDecimal credit;
    private String messageJournal;
    private String responseCode;
}
