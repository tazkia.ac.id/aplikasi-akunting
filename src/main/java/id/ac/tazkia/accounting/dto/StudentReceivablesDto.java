package id.ac.tazkia.accounting.dto;

import id.ac.tazkia.accounting.entity.BillType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class StudentReceivablesDto {
    private String nim;

    private String name;

    private String levelStudent;

    private String studyProgram;

    private String semester;

    private String program;

    private BillType billType;

    private String amountOfDebt;

    private String remainingDebt;
}
