package id.ac.tazkia.accounting.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface JuournalDetailDto {

    LocalDate getDate();
    String getDescription();
    String getName();
    BigDecimal getDebet();
    BigDecimal getCredit();
    Integer getSequence();
}
