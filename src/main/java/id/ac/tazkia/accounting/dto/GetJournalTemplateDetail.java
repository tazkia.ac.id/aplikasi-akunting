package id.ac.tazkia.accounting.dto;

import lombok.Data;

@Data
public class GetJournalTemplateDetail {
    private Integer sequence;
    private String idAccount;
    private String account;
    private String balanceType;
}
