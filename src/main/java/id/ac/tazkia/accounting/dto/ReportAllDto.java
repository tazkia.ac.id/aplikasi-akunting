package id.ac.tazkia.accounting.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReportAllDto {
    private String idAccount;
    private String code;
    private String name;
    private String accountType;
    private String nominalBalance;
    private BigDecimal saldoAwal;

    private BigDecimal saldoAwalDebet;
    private BigDecimal saldoAwalKredit;

    private BigDecimal mutasiDebet;
    private BigDecimal mutasiKredit;
    private BigDecimal neracaSaldo;

    private BigDecimal nsDebet;
    private BigDecimal nsKredit;

    private BigDecimal penyesuaianDebet;
    private BigDecimal penyesuaianKredit;
    private BigDecimal saldoPenyesuaian;
    private BigDecimal spDebet;
    private BigDecimal spKredit;

    private BigDecimal lrDebet;
    private BigDecimal lrKredit;
    private BigDecimal neracaDebet;
    private BigDecimal neracaKredit;


}
