package id.ac.tazkia.accounting.dto;

import lombok.Data;

@Data
public class MahasiswaDto {
    private String id;
    private String nim;
    private String nama;
    private String prodi;
    private String program;
    private String jenjang;
    private Integer semester;
    private Integer semesterSekarang;

}
