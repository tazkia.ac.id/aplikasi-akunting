package id.ac.tazkia.accounting.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TotalReportDto {
    BigDecimal totalSaDebet = BigDecimal.ZERO;
    BigDecimal totalSaKredit = BigDecimal.ZERO;

    BigDecimal totalMutasiDebet = BigDecimal.ZERO;
    BigDecimal totalMutasiKredit = BigDecimal.ZERO;

    BigDecimal totalNsDebet = BigDecimal.ZERO;
    BigDecimal totalNsKredit = BigDecimal.ZERO;

    BigDecimal totalPenyesuaianDebet = BigDecimal.ZERO;
    BigDecimal totalPenyesuaianKredit = BigDecimal.ZERO;

    BigDecimal totalSpDebet = BigDecimal.ZERO;
    BigDecimal totalSpKredit = BigDecimal.ZERO;

    BigDecimal totalLrDebet = BigDecimal.ZERO;
    BigDecimal totalLrKredit = BigDecimal.ZERO;

    BigDecimal totalNeracaDebet = BigDecimal.ZERO;
    BigDecimal totalNeracaKredit = BigDecimal.ZERO;
}
