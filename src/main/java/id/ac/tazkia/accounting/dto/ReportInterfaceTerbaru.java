package id.ac.tazkia.accounting.dto;

import java.math.BigDecimal;

public interface ReportInterfaceTerbaru {
    String getCode();
    String getName();
    String getNominalBalance();
    String getAccountType();
    BigDecimal getSaldoAwalDebet();
    BigDecimal getSaldoAwalKredit();
    BigDecimal getMutasiDebet();
    BigDecimal getMutasiKredit();
    BigDecimal getNeracaSaldoDebet();
    BigDecimal getNeracaSaldoKredit();
    BigDecimal getPenyesuaianDebet();
    BigDecimal getPenyesuaianKredit();
    BigDecimal getSetelahPenyesuaianDebet();
    BigDecimal getSetelahPenyesuaianKredir();
    BigDecimal getLabaRugidebet();
    BigDecimal getLabaRugikredit();
    BigDecimal getNeracaDebet();
    BigDecimal getNeracaKredit();
}
