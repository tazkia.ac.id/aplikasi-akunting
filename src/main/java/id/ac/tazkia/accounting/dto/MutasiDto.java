package id.ac.tazkia.accounting.dto;

import java.math.BigDecimal;

public interface MutasiDto {
    String getIdAccount();
    BigDecimal getMutasiDebet();
    BigDecimal getMutasiKredit();
}
