package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.Year;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface YearDao extends PagingAndSortingRepository<Year, String> {
    List<Year> findByStatusOrderByYearAsc(StatusRecord statusRecord);
}
