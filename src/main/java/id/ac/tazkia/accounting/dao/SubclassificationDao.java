package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.Subclassification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SubclassificationDao extends PagingAndSortingRepository<Subclassification, String>, CrudRepository<Subclassification, String> {
    Page<Subclassification> findByStatusOrderByCodeAsc(StatusRecord statusRecord, Pageable pageable);

    List<Subclassification> findByStatus(StatusRecord statusRecord);
}
