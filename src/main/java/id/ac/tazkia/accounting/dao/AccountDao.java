package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountType;
import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.Subclassification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AccountDao extends PagingAndSortingRepository<Account, String>, CrudRepository<Account, String> {

    Page<Account> findByStatusOrderBySubclassificationCodeAscCodeAsc(StatusRecord statusRecord, Pageable pageable);
    Account findByCodeAndStatus(String code, StatusRecord statusRecord);

    List<Account> findByStatusOrderByCode(StatusRecord statusRecord);

    List<Account> findByAndStatusAndNameContainingIgnoreCaseOrCodeContainingIgnoreCase(StatusRecord statusRecord,String name, String code);

    @Query(value = "select count(id) from account where id_subclassification = ?1", nativeQuery = true)
    Integer getCountAccount(String subclassification);

    List<Account> findByStatusAndSubclassificationOrderByCodeAsc(StatusRecord statusRecord, Subclassification subclassification);

    List<Account> findByStatusAndAccountTypeOrderByCodeAsc(StatusRecord statusRecord, AccountType accountType);

}
