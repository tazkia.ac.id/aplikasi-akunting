package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.BillType;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BillTypeDao extends PagingAndSortingRepository<BillType, String>, CrudRepository<BillType, String> {
    List<BillType> findByStatus(StatusRecord statusRecord);
}
