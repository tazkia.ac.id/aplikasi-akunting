package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountBalance;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AccountBalanceDao extends PagingAndSortingRepository<AccountBalance, String>, CrudRepository<AccountBalance, String> {
    Page<AccountBalance> findByStatusOrderByAccountCodeAsc(StatusRecord status, Pageable pageable);

    Page<AccountBalance> findByYearAndStatus(Integer year, StatusRecord status, Pageable pageable);

    Page<AccountBalance> findByYearAndInstitutAndStatusOrderByAccountCodeAsc(Integer year, Institut institut, StatusRecord status, Pageable pageable);

    AccountBalance findByAccountAndInstitutAndYearAndStatus(Account account ,Institut institut, Integer year, StatusRecord statusRecord);
}
