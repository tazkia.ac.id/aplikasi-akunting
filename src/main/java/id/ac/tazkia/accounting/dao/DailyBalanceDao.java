package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.DailyBalance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface DailyBalanceDao extends PagingAndSortingRepository<DailyBalance, String>, CrudRepository<DailyBalance, String> {
    DailyBalance findByAccountAndDate(Account account, LocalDate localDate);

    List<DailyBalance> findByDateOrderByStartAmountDesc(LocalDate localDate);

    DailyBalance findFirstByDateAfterOrderByDateAsc(LocalDate dateYear);
}
