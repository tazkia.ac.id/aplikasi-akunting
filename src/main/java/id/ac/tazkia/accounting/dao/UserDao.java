package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.config.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {
    User findByUsername(String email);
}
