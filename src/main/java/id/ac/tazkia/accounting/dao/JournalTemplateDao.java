package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JournalTemplateDao extends PagingAndSortingRepository<JournalTemplate, String>, CrudRepository<JournalTemplate, String> {
    Page<JournalTemplate> findByStatusOrderByCodeAsc(StatusRecord statusRecord, Pageable pageable);

    List<JournalTemplate> findByStatusAndJournalCategoryTemplateOrderByCodeAsc(StatusRecord statusRecord, JournalCategoryTemplate journalCategoryTemplate);

    JournalTemplate findByCodeAndStatus(String code, StatusRecord statusRecord);

    List<JournalTemplate> findByStatus(StatusRecord statusRecord);

    List<JournalTemplate> findByStatusAndTypeApp(StatusRecord statusRecord, TypeApp typeApp);

    Page<JournalTemplate> findByTypeAppAndStatus(TypeApp typeApp, StatusRecord statusRecord, Pageable pageable);

    Page<JournalTemplate> findByTypeAppAndStatusAndNameContainingIgnoreCaseOrCodeContainingIgnoreCaseOrderByCodeAsc(TypeApp typeApp, StatusRecord statusRecord, String name, String code, Pageable pageable);

    Page<JournalTemplate> findByStatusAndNameContainingIgnoreCaseOrCodeContainingIgnoreCaseAndStatusOrderByCodeAsc(StatusRecord statusRecord, String name, String code, StatusRecord statusRecord1,Pageable pageable);

}
