package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.ReportSetting;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ReportSettingDao extends CrudRepository<ReportSetting, String>, PagingAndSortingRepository<ReportSetting, String> {
    List<ReportSetting> findByStatus(StatusRecord statusRecord);
}
