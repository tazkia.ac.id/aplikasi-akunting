package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.dto.JournalDetailByAccountDto;
import id.ac.tazkia.accounting.dto.JuournalDetailDto;
import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.Journal;
import id.ac.tazkia.accounting.entity.JournalDetail;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.cglib.core.Local;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public interface JournalDetailDao extends PagingAndSortingRepository<JournalDetail, String>, CrudRepository<JournalDetail, String> {
    List<JournalDetail> findByStatusOrderBySequenceAsc(StatusRecord status);
    List<JournalDetail> findByStatusAndJournalOrderBySequence(StatusRecord statusRecord, Journal journal);

    @Query(value = "select sum(amount) as total_amount from journal_detail where id_journal = ?1 and balance_type = 'DEBET'", nativeQuery = true)
    BigDecimal getTotalAmountDebet(String idJournal);

    @Query(value = "select sum(amount) as total_amount from journal_detail where id_journal = ?1 and balance_type = 'CREDIT'", nativeQuery = true)
    BigDecimal getTotalAmountCredit(String idJournal);

    Page<JournalDetail> findByJournalTransactionDateOrderByJournalTransactionTime(Integer month, Pageable pageable);

    List<JournalDetail> findByJournalTransactionDateBetweenOrderByJournalTransactionTime(
            LocalDate startDate,
            LocalDate endDate
    );

    @Query(value = "select transaction_date as date, description, c.name,if(balance_type = 'DEBET', a.amount,'') as debet, \n" +
            "if(balance_type = 'CREDIT',a.amount,'') as credit,sequence from journal_detail as a\n" +
            "inner join journal as b on a.id_journal = b.id\n" +
            "inner join account as c on a.id_account = c.id\n" +
            "where status = 'ACTIVE' and b.transaction_date >= ?1 and b.transaction_date <= ?2\n" +
            "group by a.id order by transaction_time,sequence", nativeQuery = true)
    List<JuournalDetailDto> listJournaDetail(LocalDate startDate, LocalDate endDate);

    @Query(value = "select transaction_date from journal where transaction_date >= ?1 and transaction_date <= ?2 group by transaction_date \n" +
            "order by transaction_date", countQuery = "select count(transaction_date) as nomor from (select transaction_date from journal where transaction_date >= ?1 and transaction_date <= ?2 group by transaction_date)",nativeQuery = true)
    Page<LocalDate> cariJurnal(LocalDate date1, LocalDate date2, Pageable page);

    @Query(value = "SELECT a.id, c.id AS id_account, b.description AS description, b.transaction_date AS date, c.name AS name, c.code AS code, a.amount AS amount, a.balance_type AS balance\n" +
            "FROM journal_detail AS a\n" +
            "INNER JOIN journal AS b ON a.id_journal = b.id\n" +
            "INNER JOIN account AS c ON a.id_account = c.id\n" +
            "WHERE b.transaction_date >= ?1 AND b.transaction_date <= ?2 AND c.id = ?3 \n" +
            "AND c.status = 'ACTIVE' and a.status = 'ACTIVE' and b.status = 'ACTIVE'\n" +
            "ORDER BY b.transaction_date, a.id_journal, b.transaction_time, a.sequence ASC", nativeQuery = true)
    List<JournalDetailByAccountDto> listJournalDetailByAccount(LocalDate startDate, LocalDate endDate, String account);

    @Query(value = "select sum(a.amount) from journal_detail as a \n" +
            "\tinner join journal as b on a.id_journal = b.id\n" +
            "    inner join account as c on a.id_account = c.id\n" +
            "    where b.transaction_date >= ?1 and b.transaction_date <= ?2 \n" +
            "    and c.id = ?3", nativeQuery = true)
    BigDecimal getCurrentBalance(LocalDate startDate, LocalDate endDate, String account);

    @Query(value = "select sum(a.amount) from journal_detail as a \n" +
            "\tinner join journal as b on a.id_journal = b.id\n" +
            "    where b.transaction_date >= ?1 and b.transaction_date <= ?2\n" +
            "    and a.id_account = ?3 and a.balance_type = 'DEBET' and a.status = 'ACTIVE' and b.status = 'ACTIVE'", nativeQuery = true)
    BigDecimal getAmountByAccountTypeDebet(LocalDate startDate, LocalDate endDate, String idAccount);

    @Query(value = "select sum(a.amount) from journal_detail as a \n" +
            "\tinner join journal as b on a.id_journal = b.id\n" +
            "    where b.transaction_date >= ?1 and b.transaction_date <= ?2\n" +
            "    and a.id_account = ?3 and a.balance_type = 'CREDIT' and a.status = 'ACTIVE' and b.status = 'ACTIVE'", nativeQuery = true)
    BigDecimal getAmountByAccountTypeCredit(LocalDate startDate, LocalDate endDate, String idAccount);

    List<JournalDetail> findByAccount(Account account);
}
