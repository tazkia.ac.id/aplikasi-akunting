package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.AccountInstitut;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AccountInstitutDao extends PagingAndSortingRepository<AccountInstitut, String>, CrudRepository<AccountInstitut, String> {
    List<AccountInstitut> findByInstitutAndStatusOrderByAccountCodeAsc(Institut institut, StatusRecord statusRecord);

    AccountInstitut findByAccountAndInstitutAndStatus(Account account, Institut institut, StatusRecord statusRecord);

    List<AccountInstitut> findByAccountAndStatus(Account account, StatusRecord statusRecord);
}
