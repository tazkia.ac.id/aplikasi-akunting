package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.Yayasan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface YayasanDao extends CrudRepository<Yayasan, String> {
    List<Yayasan> findByStatus(StatusRecord statusRecord);
}
