package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.dto.ValidationTemplateDto;
import id.ac.tazkia.accounting.entity.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JournalTemplateDetailDao extends PagingAndSortingRepository<JournalTemplateDetail, String>, CrudRepository<JournalTemplateDetail, String> {
    @Query(value = "SELECT count(id) FROM journal_template_detail where id_journal_template =?1 and status = 'ACTIVE'", nativeQuery = true)
    Integer getCountByJournalTemplate(String journalTemplate);

    @Query(value = "SELECT\n" +
            "    a.id,\n" +
            "    a.code,\n" +
            "    COUNT(DISTINCT b.id_account) AS jumlah_detail,\n" +
            "    COUNT(DISTINCT CASE WHEN b.balance_type = 'DEBET' THEN b.id_account END) AS debet,\n" +
            "    COUNT(DISTINCT CASE WHEN b.balance_type = 'CREDIT' THEN b.id_account END) AS credit\n" +
            "FROM\n" +
            "    journal_template AS a\n" +
            "INNER JOIN\n" +
            "    journal_template_detail AS b ON a.id = b.id_journal_template\n" +
            "WHERE\n" +
            "\tb.status = 'ACTIVE'\n" +
            "GROUP BY\n" +
            "    a.id, a.code", nativeQuery = true)
    List<Object[]> getValidationJurnalTemplate();

    @Query(value = "SELECT\n" +
            "    a.id,\n" +
            "    a.code,\n" +
            "    COUNT(DISTINCT b.id_account) AS detail,\n" +
            "    COUNT(DISTINCT CASE WHEN b.balance_type = 'DEBET' THEN b.id_account END) AS debet,\n" +
            "    COUNT(DISTINCT CASE WHEN b.balance_type = 'CREDIT' THEN b.id_account END) AS credit\n" +
            "FROM\n" +
            "    journal_template AS a\n" +
            "INNER JOIN\n" +
            "    journal_template_detail AS b ON a.id = b.id_journal_template\n" +
            "WHERE\n" +
            "\tb.status = 'ACTIVE' and a.id = ?1 \n" +
            "GROUP BY\n" +
            "    a.id, a.code", nativeQuery = true)
    ValidationTemplateDto getValidationJournalTemplateById(String journalTemplate);

    List<JournalTemplateDetail> findByStatusOrderBySequenceAsc(StatusRecord statusRecord);

    List<JournalTemplateDetail> findByStatusAndJournalTemplateOrderBySequenceAsc(StatusRecord statusRecord, JournalTemplate journalTemplate);

    JournalTemplateDetail findByJournalTemplateAndAccountAndStatus(JournalTemplate journalTemplate, Account account, StatusRecord statusRecord);

    List<JournalTemplateDetail> findByAccount(Account account);

    List<JournalTemplateDetail> findByJournalTemplateAndStatusOrderBySequenceAsc(JournalTemplate journalTemplate, StatusRecord statusRecord);


    List<JournalTemplateDetail> findByJournalTemplateAndStatus(JournalTemplate journalTemplate, StatusRecord statusRecord);
    List<JournalTemplateDetail> findByJournalTemplate(JournalTemplate journalTemplate);
    List<JournalTemplateDetail> findByJournalTemplateOrderBySequenceAsc(JournalTemplate journalTemplate);
}
