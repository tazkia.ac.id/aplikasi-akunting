package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Month;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MonthDao extends PagingAndSortingRepository<Month, String> {
    List<Month> findByStatusOrderByNumberAsc(StatusRecord statusRecord);
}
