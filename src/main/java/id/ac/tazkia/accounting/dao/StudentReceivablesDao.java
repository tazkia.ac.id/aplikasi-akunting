package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.StatusRecord;
import id.ac.tazkia.accounting.entity.StudentReceivables;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StudentReceivablesDao extends PagingAndSortingRepository<StudentReceivables, String>,
        CrudRepository<StudentReceivables, String> {

    List<StudentReceivables> findByNimAndStatus(String nim, StatusRecord statusRecord);

    Page<StudentReceivables> findByStatusOrderByCreatedDesc(StatusRecord statusRecord, Pageable pageable);

}
