package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.JournalCategoryTemplate;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JournalCategoryTemplateDao extends PagingAndSortingRepository<JournalCategoryTemplate, String>, CrudRepository<JournalCategoryTemplate, String> {
    Page<JournalCategoryTemplate> findByStatus(StatusRecord statusRecord, Pageable pageable);
    List<JournalCategoryTemplate> findByStatusOrderByCategoryAsc(StatusRecord statusRecord);
}
