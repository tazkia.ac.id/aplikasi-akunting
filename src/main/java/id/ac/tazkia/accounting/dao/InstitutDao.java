package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InstitutDao extends CrudRepository<Institut, String> {
    List<Institut> findByStatus(StatusRecord statusRecord);
}
