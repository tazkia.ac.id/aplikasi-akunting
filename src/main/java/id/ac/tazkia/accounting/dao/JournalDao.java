package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.dto.*;
import id.ac.tazkia.accounting.entity.Institut;
import id.ac.tazkia.accounting.entity.Journal;
import id.ac.tazkia.accounting.entity.JournalType;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface JournalDao extends PagingAndSortingRepository<Journal, String>, CrudRepository<Journal, String> {
    Page<Journal> findByStatusOrderByTransactionDateDesc(StatusRecord statusRecord, Pageable pageable);
    Page<Journal> findByStatusOrderByTransactionDateAsc(StatusRecord statusRecord, Pageable pageable);
    Page<Journal> findByStatusOrderByTransactionDateDescTransactionTimeDescIdAsc(StatusRecord statusRecord, Pageable pageable);

    Page<Journal> findByStatusAndTransactionDateBetweenOrderByTransactionDateDesc
            (StatusRecord statusRecord, LocalDate startDate, LocalDate endDate, Pageable pageable);
    Page<Journal> findByStatusAndTransactionDateBetweenOrderByTransactionDateAsc
            (StatusRecord statusRecord, LocalDate startDate, LocalDate endDate, Pageable pageable);

    List<Journal> findByTransactionDateBetweenAndStatusOrderByTransactionDate(LocalDate startDate, LocalDate endDate, StatusRecord statusRecord);

    Page<Journal> findByInstitutAndStatusAndTransactionDateBetweenOrderByTransactionDateDesc
            (Institut institut, StatusRecord statusRecord,  LocalDate startDate, LocalDate endDate, Pageable page);
    Page<Journal> findByInstitutAndStatusAndTransactionDateBetweenOrderByTransactionDateAsc
            (Institut institut, StatusRecord statusRecord,  LocalDate startDate, LocalDate endDate, Pageable page);

    Page<Journal> findByStatusAndInstitutOrderByTransactionDateDesc(
            StatusRecord statusRecord, Institut institut, Pageable page
    );

    Page<Journal> findByStatusAndTagsContainingIgnoreCaseOrderByTransactionDateDesc(StatusRecord statusRecord, String app, Pageable page);
    Page<Journal> findByStatusAndTagsContainingIgnoreCaseOrderByTransactionDateAsc(StatusRecord statusRecord, String app, Pageable page);

    Page<Journal> findByStatusAndTagsContainingIgnoreCaseAndTransactionDateBetweenOrderByTransactionDateDesc(StatusRecord statusRecord, String app,
                                                                                                             LocalDate startDate, LocalDate endDate, Pageable page);

    Page<Journal> findByStatusAndDescriptionContainingIgnoreCaseOrderByTransactionDateDesc(StatusRecord statusRecord, String search, Pageable page);
    Page<Journal> findByStatusAndDescriptionContainingIgnoreCaseOrderByTransactionDateAsc(StatusRecord statusRecord, String search, Pageable page);

    Page<Journal> findByStatusAndTransactionDateBetweenAndDescriptionContainingIgnoreCaseOrderByTransactionDateDesc(
            StatusRecord statusRecord, LocalDate startDate, LocalDate endDate, String search, Pageable page
    );
    Page<Journal> findByStatusAndTransactionDateBetweenAndDescriptionContainingIgnoreCaseOrderByTransactionDateAsc(
            StatusRecord statusRecord, LocalDate startDate, LocalDate endDate, String search, Pageable page
    );

    Page<Journal> findByStatusAndTransactionDateBetweenAndTagsContainingIgnoreCaseOrderByTransactionDateDesc(
            StatusRecord statusRecord, LocalDate startDate, LocalDate endDate, String tags, Pageable page
    );
    Page<Journal> findByStatusAndTransactionDateBetweenAndTagsContainingIgnoreCaseOrderByTransactionDateAsc(
            StatusRecord statusRecord, LocalDate startDate, LocalDate endDate, String tags, Pageable page
    );

    Page<Journal> findByStatusAndInstitutAndDescriptionContainingIgnoreCaseOrderByTransactionDateDesc(
            StatusRecord statusRecord, Institut institut, String search, Pageable page
    );
    Page<Journal> findByStatusAndInstitutAndDescriptionContainingIgnoreCaseOrderByTransactionDateAsc(
            StatusRecord statusRecord, Institut institut, String search, Pageable page
    );

    List<Journal> findByStatusOrderByTransactionDateDesc(StatusRecord statusRecord);


    @Query(value = "select code,name, nominal_balance, account_type,sa_debet,sa_kredit,mutasi_debet,mutasi_kredit,ns_debet,ns_kredit,penyesuaian_debet,penyesuaian_kredit,sp_debet,sp_kredit,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_debet,0) as lr_debet,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_kredit,0) as lr_kredit,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_debet,0) as neraca_debet,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_kredit,0) as neraca_kredit from\n" +
            "(select id,code,name,nominal_balance,account_type, sa_debet,sa_kredit, mutasi_debet,mutasi_kredit,\n" +
            "if(saldo >= 0,if(nominal_balance = 'DEBET',saldo,0),if(nominal_balance = 'CREDIT',saldo * -1,0)) as ns_debet,\n" +
            "if(saldo >= 0,if(nominal_balance = 'CREDIT',saldo,0),if(nominal_balance = 'DEBET',saldo * -1,0)) as ns_kredit,\n" +
            "penyesuaian_debet,penyesuaian_kredit,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'DEBET',saldo_penyesuaian,0),if(nominal_balance = 'CREDIT',saldo_penyesuaian * -1,0)) as sp_debet,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'CREDIT',saldo_penyesuaian,0),if(nominal_balance = 'DEBET',saldo_penyesuaian * -1,0)) as sp_kredit\n" +
            "from\n" +
            "(select id, code, name, nominal_balance,account_type,sa_debet,sa_kredit,mutasi_debet,mutasi_kredit,penyesuaian_debet,penyesuaian_kredit,        \n" +
            "if(nominal_balance = 'DEBET',\n" +
            "\tif(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit))) as saldo,\n" +
            "if(nominal_balance = 'DEBET',\n" +
            "\tif(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet  -penyesuaian_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit))) as saldo_penyesuaian\n" +
            "from\n" +
            "(select id,code,name,nominal_balance,account_type,if(nominal_balance = 'DEBET',if(saldo_awal >= 0,saldo_awal,0),if(saldo_awal < 0,saldo_awal * -1,0))as sa_debet,\n" +
            "if(nominal_balance = 'DEBET',if(saldo_awal < 0,saldo_awal * -1,0),if(saldo_awal >= 0,saldo_awal,0))as sa_kredit,\n" +
            "coalesce(mutasi_debet,0) as mutasi_debet, coalesce(mutasi_kredit,0) as mutasi_kredit,\n" +
            "coalesce(pd,0) as penyesuaian_debet, coalesce(pk,0) penyesuaian_kredit\n" +
            "from\n" +
            "(select a.id, code,a.name,account_type,a.nominal_balance,(coalesce(account_balance,0)-coalesce(c.kredit,0))+coalesce(c.debet,0) as saldo_awal from \n" +
            "(select * from account where status = 'ACTIVE') as a\n" +
            "left join account_balance as b on a.id = b.id_account and b.status = 'ACTIVE'\n" +
            "left join (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as debet,sum(if(balance_type = 'CREDIT',amount,0))as kredit from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id where year(b.transaction_date) = ?1 and b.transaction_date < ?2 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' group by a.id_account) as c on a.id = c.id_account) as a\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as mutasi_debet,sum(if(balance_type = 'CREDIT',amount,0))as mutasi_kredit from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id where b.transaction_date  >= ?1 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'NORMAL' group by a.id_account ) as d on a.id = d.id_account\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as pd,sum(if(balance_type = 'CREDIT',amount,0))as pk from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id where b.transaction_date  >= ?2 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'PENYESUAIAN' group by a.id_account) as e on a.id = e.id_account) as a) as a) as a\n" +
            "order by code", nativeQuery = true)
    List<Object[]> getReportSetting(Integer year, LocalDate startDate, LocalDate endDate);


    @Query(value = "select code as code, name as name, nominal_balance as nominalBalance, account_type as accountType,sa_debet as saldoAwalDebet,sa_kredit as saldoAwalKredit,\n" +
            "mutasi_debet as mutasiDebet,mutasi_kredit as mutasiKredit,ns_debet as neracaSaldoDebet,ns_kredit as neracaSaldoKredit,\n" +
            "penyesuaian_debet as penyesuaianDebet,penyesuaian_kredit as penyesuaianKredit,sp_debet as setelahPenyesuaianDebet,\n" +
            "sp_kredit as setelahPenyesuaianKredir,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_debet,0) as labaRugidebet,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_kredit,0) as labaRugikredit,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_debet,0) as neracaDebet,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_kredit,0) as neracaKredit from\n" +
            "(select id,code,name,nominal_balance,account_type, sa_debet,sa_kredit, mutasi_debet,mutasi_kredit,\n" +
            "if(saldo >= 0,if(nominal_balance = 'DEBET',saldo,0),if(nominal_balance = 'CREDIT',saldo * -1,0)) as ns_debet,\n" +
            "if(saldo >= 0,if(nominal_balance = 'CREDIT',saldo,0),if(nominal_balance = 'DEBET',saldo * -1,0)) as ns_kredit,\n" +
            "penyesuaian_debet,penyesuaian_kredit,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'DEBET',saldo_penyesuaian,0),if(nominal_balance = 'CREDIT',saldo_penyesuaian * -1,0)) as sp_debet,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'CREDIT',saldo_penyesuaian,0),if(nominal_balance = 'DEBET',saldo_penyesuaian * -1,0)) as sp_kredit\n" +
            "from\n" +
            "(select id, code, name, nominal_balance,account_type,sa_debet,sa_kredit,mutasi_debet,mutasi_kredit,penyesuaian_debet,penyesuaian_kredit,        \n" +
            "if(nominal_balance = 'DEBET',\n" +
            "\tif(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit))) as saldo,\n" +
            "if(nominal_balance = 'DEBET',\n" +
            "\tif(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit))) as saldo_penyesuaian\n" +
            "from\n" +
            "(select id,code,name,nominal_balance,account_type,if(nominal_balance = 'DEBET',if(saldo_awal >= 0,saldo_awal,0),if(saldo_awal < 0,saldo_awal * -1,0))as sa_debet,\n" +
            "if(nominal_balance = 'DEBET',if(saldo_awal < 0,saldo_awal * -1,0),if(saldo_awal >= 0,saldo_awal,0))as sa_kredit,\n" +
            "coalesce(mutasi_debet,0) as mutasi_debet, coalesce(mutasi_kredit,0) as mutasi_kredit,\n" +
            "coalesce(pd,0) as penyesuaian_debet, coalesce(pk,0) penyesuaian_kredit\n" +
            "from\n" +
            "(select a.id, code,a.name,account_type,a.nominal_balance,(coalesce(account_balance,0)-coalesce(c.kredit,0))+coalesce(c.debet,0) as saldo_awal from \n" +
            "account as a\n" +
            "left join (select id_account, sum(account_balance)as account_balance from account_balance where status = 'ACTIVE' and year = ?1 and id_institut in (?4) group by id_account) as b on a.id = b.id_account \n" +
            "left join (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as debet,sum(if(balance_type = 'CREDIT',amount,0))as kredit from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id and b.id_institut in (?4) and b.transaction_date < ?2 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' group by a.id_account) as c on a.id = c.id_account) as a\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as mutasi_debet,sum(if(balance_type = 'CREDIT',amount,0))as mutasi_kredit from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id and b.id_institut in (?4) and b.transaction_date  >= ?2 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'NORMAL' group by a.id_account ) as d on a.id = d.id_account\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as pd,sum(if(balance_type = 'CREDIT',amount,0))as pk from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id and b.id_institut in (?4) and b.transaction_date  >= ?2 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'PENYESUAIAN' group by a.id_account) as e on a.id = e.id_account) as a) as a) as a\n" +
            "order by code", nativeQuery = true)
    List<ReportInterfaceTerbaru> getNewSqlReport(Integer year, LocalDate startDate, LocalDate endDate, String[] institut);

    @Query(value = "select sum(saldoAwalDebet)as saldoAwalDebet,sum(saldoAwalKredit)as saldoAwalKredit,\n" +
            "sum(mutasiDebet)as mutasiDebet,sum(mutasiKredit)as mutasiKredit, sum(neracaSaldoDebet) as neracaSaldoDebet, sum(neracaSaldoKredit) as neracaSaldoKredit,\n" +
            "sum(penyesuaianDebet) as penyesuaianDebet, sum(penyesuaianKredit) as penyesuaianKredit, sum(setelahPenyesuaianDebet) as setelahPenyesuaianDebet,\n" +
            "sum(setelahPenyesuaianKredir) as setelahPenyesuaianKredir, sum(labaRugidebet) as labaRugidebet, sum(labaRugikredit) as labaRugikredit, sum(neracaDebet) as neracaDebet,\n" +
            "sum(neracaKredit) as neracaKredit  from\n" +
            "(select code as code, name as name, nominal_balance as nominalBalance, account_type as accountType,sa_debet as saldoAwalDebet,sa_kredit as saldoAwalKredit,\n" +
            "mutasi_debet as mutasiDebet,mutasi_kredit as mutasiKredit,ns_debet as neracaSaldoDebet,ns_kredit as neracaSaldoKredit,\n" +
            "penyesuaian_debet as penyesuaianDebet,penyesuaian_kredit as penyesuaianKredit,sp_debet as setelahPenyesuaianDebet,\n" +
            "sp_kredit as setelahPenyesuaianKredir,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_debet,0) as labaRugidebet,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_kredit,0) as labaRugikredit,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_debet,0) as neracaDebet,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_kredit,0) as neracaKredit from\n" +
            "(select id,code,name,nominal_balance,account_type, sa_debet,sa_kredit, mutasi_debet,mutasi_kredit,\n" +
            "if(saldo >= 0,if(nominal_balance = 'DEBET',saldo,0),if(nominal_balance = 'CREDIT',saldo * -1,0)) as ns_debet,\n" +
            "if(saldo >= 0,if(nominal_balance = 'CREDIT',saldo,0),if(nominal_balance = 'DEBET',saldo * -1,0)) as ns_kredit,\n" +
            "penyesuaian_debet,penyesuaian_kredit,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'DEBET',saldo_penyesuaian,0),if(nominal_balance = 'CREDIT',saldo_penyesuaian * -1,0)) as sp_debet,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'CREDIT',saldo_penyesuaian,0),if(nominal_balance = 'DEBET',saldo_penyesuaian * -1,0)) as sp_kredit\n" +
            "from\n" +
            "(select id, code, name, nominal_balance,account_type,sa_debet,sa_kredit,mutasi_debet,mutasi_kredit,penyesuaian_debet,penyesuaian_kredit,        \n" +
            "if(nominal_balance = 'DEBET',\n" +
            "if(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit))) as saldo,\n" +
            "if(nominal_balance = 'DEBET',\n" +
            "if(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit))) as saldo_penyesuaian\n" +
            "from\n" +
            "(select id,code,name,nominal_balance,account_type,if(nominal_balance = 'DEBET',if(saldo_awal >= 0,saldo_awal,0),if(saldo_awal < 0,saldo_awal * -1,0))as sa_debet,\n" +
            "if(nominal_balance = 'DEBET',if(saldo_awal < 0,saldo_awal * -1,0),if(saldo_awal >= 0,saldo_awal,0))as sa_kredit,\n" +
            "coalesce(mutasi_debet,0) as mutasi_debet, coalesce(mutasi_kredit,0) as mutasi_kredit,\n" +
            "coalesce(pd,0) as penyesuaian_debet, coalesce(pk,0) penyesuaian_kredit\n" +
            "from\n" +
            "(select a.id, code,a.name,account_type,a.nominal_balance,(coalesce(account_balance,0)-coalesce(c.kredit,0))+coalesce(c.debet,0) as saldo_awal from \n" +
            "account as a\n" +
            "left join (select id_account, sum(account_balance)as account_balance from account_balance where status = 'ACTIVE' and year = ?1 and id_institut in (?4) group by id_account) as b on a.id = b.id_account \n" +
            "left join (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as debet,sum(if(balance_type = 'CREDIT',amount,0))as kredit from journal_detail as a inner join\n" +
            "journal as b on a.id_journal = b.id and b.id_institut in (?4) and b.transaction_date < ?2 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' group by a.id_account) as c on a.id = c.id_account) as a\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as mutasi_debet,sum(if(balance_type = 'CREDIT',amount,0))as mutasi_kredit from journal_detail as a inner join\n" +
            "journal as b on a.id_journal = b.id and b.id_institut in (?4) and b.transaction_date  >= ?2 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'NORMAL' group by a.id_account ) as d on a.id = d.id_account\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as pd,sum(if(balance_type = 'CREDIT',amount,0))as pk from journal_detail as a inner join\n" +
            "journal as b on a.id_journal = b.id and b.id_institut in (?4) and b.transaction_date  >= ?2 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'PENYESUAIAN' group by a.id_account) as e on a.id = e.id_account) as a) as a) as a\n" +
            "order by code)as a", nativeQuery = true)
    ReportInterfaceTotalDto getTotalReport(Integer year, LocalDate startDate, LocalDate endDate, String[] institut);


    @Query(value = "select code,name, nominal_balance, account_type,sa_debet,sa_kredit,mutasi_debet,mutasi_kredit,ns_debet,ns_kredit,penyesuaian_debet,penyesuaian_kredit,sp_debet,sp_kredit,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_debet,0) as lr_debet,\n" +
            "if(account_type in ('EXPENSE','REVENUE'),sp_kredit,0) as lr_kredit,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_debet,0) as neraca_debet,\n" +
            "if(account_type in ('AKTIVA','PASIVA','EQUITY'),sp_kredit,0) as neraca_kredit from\n" +
            "(select id,code,name,nominal_balance,account_type, sa_debet,sa_kredit, mutasi_debet,mutasi_kredit,\n" +
            "if(saldo >= 0,if(nominal_balance = 'DEBET',saldo,0),if(nominal_balance = 'CREDIT',saldo * -1,0)) as ns_debet,\n" +
            "if(saldo >= 0,if(nominal_balance = 'CREDIT',saldo,0),if(nominal_balance = 'DEBET',saldo * -1,0)) as ns_kredit,\n" +
            "penyesuaian_debet,penyesuaian_kredit,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'DEBET',saldo_penyesuaian,0),if(nominal_balance = 'CREDIT',saldo_penyesuaian * -1,0)) as sp_debet,\n" +
            "if(saldo_penyesuaian >= 0,if(nominal_balance = 'CREDIT',saldo_penyesuaian,0),if(nominal_balance = 'DEBET',saldo_penyesuaian * -1,0)) as sp_kredit\n" +
            "from\n" +
            "(select id, code, name, nominal_balance,account_type,sa_debet,sa_kredit,mutasi_debet,mutasi_kredit,penyesuaian_debet,penyesuaian_kredit,        \n" +
            "if(nominal_balance = 'DEBET',\n" +
            "\tif(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit))) as saldo,\n" +
            "if(nominal_balance = 'DEBET',\n" +
            "\tif(sa_debet = sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit,sa_debet+mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,(sa_kredit * -1)-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit)),\n" +
            "    if(sa_kredit = sa_debet,sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit,\n" +
            "    if(sa_debet > sa_kredit, (sa_debet * -1)+ mutasi_debet-mutasi_kredit+penyesuaian_debet-penyesuaian_kredit,\n" +
            "sa_kredit-mutasi_debet+mutasi_kredit-penyesuaian_debet+penyesuaian_kredit))) as saldo_penyesuaian\n" +
            "from\n" +
            "(select id,code,name,nominal_balance,account_type,if(nominal_balance = 'DEBET',if(saldo_awal >= 0,saldo_awal,0),if(saldo_awal < 0,saldo_awal * -1,0))as sa_debet,\n" +
            "if(nominal_balance = 'DEBET',if(saldo_awal < 0,saldo_awal * -1,0),if(saldo_awal >= 0,saldo_awal,0))as sa_kredit,\n" +
            "coalesce(mutasi_debet,0) as mutasi_debet, coalesce(mutasi_kredit,0) as mutasi_kredit,\n" +
            "coalesce(pd,0) as penyesuaian_debet, coalesce(pk,0) penyesuaian_kredit\n" +
            "from\n" +
            "(select a.id, code,a.name,account_type,a.nominal_balance,(coalesce(account_balance,0)-coalesce(c.kredit,0))+coalesce(c.debet,0) as saldo_awal from \n" +
            "(select * from account where status = 'ACTIVE') as a\n" +
            "left join account_balance as b on a.id = b.id_account and b.status = 'ACTIVE'\n" +
            "left join (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as debet,sum(if(balance_type = 'CREDIT',amount,0))as kredit from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id where year(b.transaction_date) = ?1 and b.transaction_date < ?2 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' group by a.id_account) as c on a.id = c.id_account) as a\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as mutasi_debet,sum(if(balance_type = 'CREDIT',amount,0))as mutasi_kredit from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id where b.transaction_date  >= ?2 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'NORMAL' group by a.id_account ) as d on a.id = d.id_account\n" +
            "            left join\n" +
            "            (select a.id_account,sum(if(balance_type = 'DEBET',amount,0))as pd,sum(if(balance_type = 'CREDIT',amount,0))as pk from journal_detail as a inner join\n" +
            "\t\t\tjournal as b on a.id_journal = b.id where b.transaction_date  >= ?2 and b.transaction_date <= ?3 and a.status = 'ACTIVE'\n" +
            "            and b.status = 'ACTIVE' and b.journal_type = 'PENYESUAIAN' group by a.id_account) as e on a.id = e.id_account) as a) as a) as a\n" +
            "order by code", nativeQuery = true)
    List<Object[]> getReportSql(Integer year, LocalDate startDate, LocalDate endDate);


    @Query(value = "SELECT a.id as idAccount, code as code, a.name as name, account_type as accountType, a.nominal_balance as nominalBalance, \n" +
            "                    (COALESCE(account_balance, 0) - COALESCE(c.kredit, 0)) + COALESCE(c.debet, 0) AS saldoAwal\n" +
            "                FROM account a\n" +
            "                LEFT JOIN account_balance b ON a.id = b.id_account AND b.status = 'ACTIVE'\n" +
            "                LEFT JOIN (\n" +
            "                    SELECT a.id_account, \n" +
            "                        SUM(IF(balance_type = 'DEBET', amount, 0)) AS debet, \n" +
            "                        SUM(IF(balance_type = 'CREDIT', amount, 0)) AS kredit\n" +
            "                    FROM journal_detail a \n" +
            "                    INNER JOIN journal b ON a.id_journal = b.id\n" +
            "                    WHERE YEAR(b.transaction_date) = ?1 \n" +
            "                    AND b.transaction_date < ?2 \n" +
            "                    AND a.status = 'ACTIVE' \n" +
            "                    AND b.status = 'ACTIVE'\n" +
            "                    GROUP BY a.id_account\n" +
            "                ) c ON a.id = c.id_account\n" +
            "                WHERE a.status = 'ACTIVE'", nativeQuery = true)
    List<SaldoAwalDto> getSaldoAwal(Integer year, LocalDate startDate);


    @Query(value = "SELECT a.id_account as idAccount, SUM(IF(balance_type = 'DEBET', amount, 0)) AS mutasiDebet, SUM(IF(balance_type = 'CREDIT', amount, 0)) AS mutasiKredit\n" +
            "                FROM journal_detail a \n" +
            "                INNER JOIN journal b ON a.id_journal = b.id\n" +
            "                WHERE b.transaction_date >= ?1 \n" +
            "                AND b.transaction_date <= ?2 \n" +
            "                AND a.status = 'ACTIVE' \n" +
            "                AND b.status = 'ACTIVE' \n" +
            "                AND b.journal_type = ?3\n" +
            "                GROUP BY a.id_account", nativeQuery = true)
    List<MutasiDto> getMutasiDanPenyesuaian(LocalDate startDate, LocalDate endDate, JournalType journalType);

    @Query(value = "WITH transaksi AS (\n" +
            "    SELECT \n" +
            "        b.id_account,\n" +
            "        a.transaction_date,\n" +
            "        a.transaction_time,\n" +
            "        STR_TO_DATE(CONCAT(a.transaction_date, ' ', TIME(a.transaction_time)), '%Y-%m-%d %H:%i:%s') AS transaction_datetime,\n" +
            "        SUM(IF(b.balance_type = 'DEBET', b.amount, 0)) AS debet,\n" +
            "        SUM(IF(b.balance_type = 'CREDIT', b.amount, 0)) AS credit,\n" +
            "        GROUP_CONCAT(a.description ORDER BY a.transaction_date SEPARATOR ' - ') AS description\n" +
            "    FROM journal AS a\n" +
            "    INNER JOIN journal_detail AS b ON a.id = b.id_journal\n" +
            "    WHERE a.status = 'ACTIVE'\n" +
            "    AND YEAR(a.transaction_date) = ?1\n" +
            "    GROUP BY b.id_account, STR_TO_DATE(CONCAT(a.transaction_date, ' ', TIME(a.transaction_time)), '%Y-%m-%d %H:%i:%s')\n" +
            "),\n" +
            "kalendar AS (\n" +
            "    SELECT DISTINCT \n" +
            "        DATE_FORMAT(DATE_ADD('2024-01-01', INTERVAL seq MONTH), '%Y-%m-01') AS month_start\n" +
            "    FROM (\n" +
            "        SELECT 0 AS seq UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL \n" +
            "        SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL \n" +
            "        SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL \n" +
            "        SELECT 9 UNION ALL SELECT 10 UNION ALL SELECT 11\n" +
            "    ) AS months\n" +
            ")\n" +
            "SELECT \n" +
            "    kalendar.month_start,\n" +
            "    a.code, \n" +
            "    a.name, \n" +
            "    COALESCE(b.saldo, 0) AS saldoAwal, \n" +
            "    COALESCE(t.description, '-') AS desk, \n" +
            "    COALESCE(MONTHNAME(t.transaction_date), '-') AS bulan, \n" +
            "    COALESCE(DAY(t.transaction_date), '-') AS tanggal, \n" +
            "    COALESCE(t.debet, 0) AS debet, \n" +
            "    COALESCE(t.credit, 0) AS credit,\n" +
            "    (COALESCE(b.saldo, 0) \n" +
            "        + COALESCE(SUM(t.debet) OVER (PARTITION BY t.id_account ORDER BY t.transaction_datetime ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 0)\n" +
            "        - COALESCE(SUM(t.credit) OVER (PARTITION BY t.id_account ORDER BY t.transaction_datetime ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 0)\n" +
            "    ) AS saldoAkhir\n" +
            "FROM account AS a\n" +
            "CROSS JOIN kalendar\n" +
            "LEFT JOIN (\n" +
            "    SELECT id_account, SUM(account_balance) AS saldo\n" +
            "    FROM account_balance\n" +
            "    WHERE year = ?1\n" +
            "    GROUP BY id_account\n" +
            ") AS b ON a.id = b.id_account\n" +
            "LEFT JOIN transaksi AS t ON a.id = t.id_account AND DATE_FORMAT(t.transaction_date, '%Y-%m-01') = kalendar.month_start\n" +
            "ORDER BY kalendar.month_start, a.code, t.transaction_datetime", nativeQuery = true)
    List<WoorkSheetAllDto> getWoorksheetAll(String tahun);
}
