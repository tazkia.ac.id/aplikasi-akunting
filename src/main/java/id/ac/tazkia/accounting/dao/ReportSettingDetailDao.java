package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Account;
import id.ac.tazkia.accounting.entity.ReportSetting;
import id.ac.tazkia.accounting.entity.ReportSettingDetail;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ReportSettingDetailDao extends CrudRepository<ReportSettingDetail, String>, PagingAndSortingRepository<ReportSettingDetail, String> {
    List<ReportSettingDetail> findByReportSettingAndStatusOrderByAccountCodeAsc(ReportSetting reportSetting, StatusRecord statusRecord);

    int countByReportSettingAndStatus(ReportSetting reportSetting, StatusRecord statusRecord);

    ReportSettingDetail findByAccountAndReportSettingAndStatus(Account account, ReportSetting reportSetting, StatusRecord statusRecord);
}
