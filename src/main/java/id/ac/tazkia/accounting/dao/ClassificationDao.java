package id.ac.tazkia.accounting.dao;

import id.ac.tazkia.accounting.entity.Classification;
import id.ac.tazkia.accounting.entity.StatusRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ClassificationDao extends PagingAndSortingRepository<Classification, String>, CrudRepository<Classification, String> {
    List<Classification> findByStatus(StatusRecord statusRecord);
}
