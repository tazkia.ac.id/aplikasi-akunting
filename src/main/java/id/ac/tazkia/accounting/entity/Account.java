package id.ac.tazkia.accounting.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data @Entity
public class Account {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty
    private String code;

    @NotEmpty @Size(min = 3, max = 200)
    private String name;

    @NotNull @Min(0)
    private BigDecimal amount = BigDecimal.valueOf(0.0);

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    @ManyToOne
    @JoinColumn(name = "id_subclassification")
    private Subclassification subclassification;

    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Enumerated(EnumType.STRING)
    private BalanceType nominalBalance;

    private LocalDateTime modified;

    private String modifiedBy;

    private LocalDateTime deleted;

    private String deletedBy;


}
