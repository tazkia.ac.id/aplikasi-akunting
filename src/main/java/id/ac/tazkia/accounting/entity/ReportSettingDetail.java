package id.ac.tazkia.accounting.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE report_setting_detail SET status = 'DELETED' WHERE id=?")
public class ReportSettingDetail extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "id_report_setting")
    private ReportSetting reportSetting;

    @ManyToOne
    @JoinColumn(name = "id_account")
    private Account account;

    private Integer sequence;


}
