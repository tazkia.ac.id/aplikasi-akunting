package id.ac.tazkia.accounting.entity;

public enum TypeApp {
    FINANCE, HRD, SPMB, SMILE, GA, ACCOUNTING
}
