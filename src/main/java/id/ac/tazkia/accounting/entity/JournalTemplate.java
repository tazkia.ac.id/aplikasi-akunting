package id.ac.tazkia.accounting.entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity
public class JournalTemplate {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    @ManyToOne
    @JoinColumn(name = "id_journal_category_template")
    private JournalCategoryTemplate journalCategoryTemplate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    @Enumerated(EnumType.STRING)
    private TypeApp typeApp;


}
