package id.ac.tazkia.accounting.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Data @Entity
public class JournalTemplateDetail {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_journal_template")
    private JournalTemplate journalTemplate;

    @ManyToOne
    @JoinColumn(name = "id_account")
    private Account account;

    @NotNull
    private Integer sequence;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    @Enumerated(EnumType.STRING)
    private BalanceType balanceType;


}
