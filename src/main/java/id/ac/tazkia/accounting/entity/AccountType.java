package id.ac.tazkia.accounting.entity;

public enum AccountType {
    AKTIVA, PASIVA, EQUITY, REVENUE, EXPENSE
}
