package id.ac.tazkia.accounting.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
public class Subclassification {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_classification")
    private Classification classification;

    @NotNull
    private String code;

    @NotNull
    private String name;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;
}
