package id.ac.tazkia.accounting.entity;


import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data @Entity
public class AccountBalance {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_account")
    private Account account;

    @ManyToOne
        @JoinColumn(name = "id_institut")
    private Institut institut;

    private BigDecimal accountBalance;

    private Integer year;

    private LocalDate startDate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    private LocalDateTime created;

    private String createdBy;

    private LocalDateTime deleted;

    private String deletedBy;

    private LocalDateTime modified;

    private String modifiedBy;


}
