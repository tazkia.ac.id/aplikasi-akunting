package id.ac.tazkia.accounting.entity;

import java.math.BigDecimal;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data @Entity
public class JournalDetail {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_journal", nullable = false)
    private Journal journal;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_account", nullable = false)
    private Account account;

    @NotNull
    @Enumerated(EnumType.STRING)
    private BalanceType balanceType;

    @NotNull @Min(0)
    private BigDecimal amount;

    @NotNull
    private Integer sequence;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;
}
