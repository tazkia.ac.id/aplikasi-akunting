package id.ac.tazkia.accounting.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import id.ac.tazkia.accounting.entity.config.User;
import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data @Entity
public class Journal {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private LocalDateTime transactionTime;

    @NotNull
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate transactionDate;

    @NotEmpty
    private String description;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    @ManyToOne
    @JoinColumn(name = "id_institut")
    private Institut institut;

    @Enumerated(EnumType.STRING)
    private JournalType journalType;


    private String tags;

}
