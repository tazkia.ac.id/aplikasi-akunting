package id.ac.tazkia.accounting.entity;

public enum BalanceType {
    DEBET, CREDIT
}
