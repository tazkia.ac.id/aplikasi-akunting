package id.ac.tazkia.accounting.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;

@Data
@Entity
@SQLDelete(sql = "UPDATE yayasan SET status = 'DELETED' WHERE id=?")
public class Yayasan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaYayasan;

    private String deskripsi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;
}
