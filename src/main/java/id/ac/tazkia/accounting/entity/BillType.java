package id.ac.tazkia.accounting.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
public class BillType {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String name;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;
}
