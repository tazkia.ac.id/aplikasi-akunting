package id.ac.tazkia.accounting.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE report_setting SET status = 'DELETED' WHERE id=?")
public class ReportSetting extends BaseEntity{
    @Column(name = "name", length = 45)
    private String name;

    private String minus;

    private String brackets;

    private String redsign;
}
