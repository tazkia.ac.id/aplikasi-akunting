package id.ac.tazkia.accounting.entity.config;


import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "s_user")
@Data
public class User {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String username;
    private Boolean active;


    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

    private String user;


}
