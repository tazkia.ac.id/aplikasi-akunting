package id.ac.tazkia.accounting.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@MappedSuperclass
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    @Column(name = "created", updatable = false)
    @CreatedDate
    private LocalDateTime created;

    @Column(name = "created_by", length = 45, updatable = false)
    @CreatedBy
    private String createdBy;

    @Column(name = "deleted")
    @LastModifiedDate
    private LocalDateTime deleted;

    @Column(name = "deleted_by", length = 45)
    @LastModifiedBy
    private String deletedBy;

    @Column(name = "modified")
    @LastModifiedDate
    private LocalDateTime modified;

    @Column(name = "modified_by", length = 45)
    @LastModifiedBy
    private String modifiedBy;

    @PrePersist
    public void prePersist() {
        // Biarkan kolom 'modified' dan 'modifiedBy' kosong pada insert pertama
        this.modified = null;
        this.modifiedBy = null;

        this.deleted = null;
        this.deletedBy = null;
    }

    @PreUpdate
    public void preUpdate() {
        // Isi kolom 'modified' dan 'modifiedBy' pada update
        this.modified = LocalDateTime.now();  // Atau gunakan auditing @LastModifiedDate
        this.modifiedBy = "currentUser"; // Atau gunakan auditing @LastModifiedBy
    }

}
