package id.ac.tazkia.accounting.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Year {
    @Id
    private Integer id;

    private Integer year;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;
}
