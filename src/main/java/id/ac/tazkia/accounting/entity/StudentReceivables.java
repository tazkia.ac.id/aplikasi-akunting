package id.ac.tazkia.accounting.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
public class StudentReceivables {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String nim;

    private String name;

    private String levelStudent;

    private String studyProgram;

    private String semester;

    private String program;

    @ManyToOne
    @JoinColumn(name = "bill_type")
    private BillType billType;


    private BigDecimal amountOfDebt;

    private BigDecimal remainingDebt;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    private String user;

    private LocalDateTime created;


}
