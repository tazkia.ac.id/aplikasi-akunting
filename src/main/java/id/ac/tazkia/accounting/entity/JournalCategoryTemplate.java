package id.ac.tazkia.accounting.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data @Entity
public class JournalCategoryTemplate {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String category;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;
}
